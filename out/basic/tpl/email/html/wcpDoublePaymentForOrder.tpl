<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
  <title>[{$subject}]</title>
  <meta http-equiv="Content-Type" content="text/html; charset=[{$charset}]">
  </head>
  <body bgcolor="#FFFFFF" link="#355222" alink="#355222" vlink="#355222" style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 10px;">
    <br>
   [{ oxcontent ident="oxorderwcppaidemail" }]

    <h3 style="font-weight: bold; margin: 20px 0 7px; padding: 0; line-height: 35px; font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase; border-bottom: 4px solid #ddd;">
        THERE ARE TWO PAYMENTS FOR AN ORDER
    </h3>

    <h3 style="font-weight: bold; margin: 20px 0 7px; padding: 0; line-height: 35px; font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase; border-bottom: 4px solid #ddd;">
        Oxid Ordernumber: [{ $orderNumber }]
    </h3>

    <table border="0" cellspacing="0" cellpadding="0" width="100%">
      <tr valign="top">
        <td style="padding: 5px; border-bottom: 1px solid #ddd;" align="right">
            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                orderNumbers:
            </p>
        </td>
        <td style="padding: 5px; border-bottom: 1px solid #ddd;">
            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                [{ $wcpOrderNumber1 }]
            </p>
            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                [{ $wcpOrderNumber2 }]
            </p>
        </td>
      </tr>
    </table>

    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
        [{ oxmultilang ident="EMAIL_WCP_PAID_HTML_YUORTEAM1" }] [{ $shop->oxshops__oxname->value }] [{ oxmultilang ident="EMAIL_WCP_PAID_HTML_YUORTEAM2" }]
    </p>

    [{if $oViewConf->showTs("ORDERCONFEMAIL") && $oViewConf->getTsId() }]
        <h3 style="font-weight: bold; margin: 20px 0 7px; padding: 0; line-height: 35px; font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase; border-bottom: 4px solid #ddd;">
            [{ oxmultilang ident="EMAIL_WCP_PAID_HTML_TS_RATINGS_RATEUS" }]
        </h3>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
            <a href="[{ $oViewConf->getTsRatingUrl() }]" target="_blank" title="[{ oxmultilang ident="TS_RATINGS_URL_TITLE" }]">
                <img src="https://www.trustedshops.com/bewertung/widget/img/bewerten_de.gif" border="0" alt="[{ oxmultilang ident="TS_RATINGS_BUTTON_ALT" }]" align="middle">
            </a>
        </p>
    [{/if}]
  </body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
  <title>[{$subject}]</title>
  <meta http-equiv="Content-Type" content="text/html; charset=[{$charset}]">
  </head>
  <body bgcolor="#FFFFFF" link="#355222" alink="#355222" vlink="#355222" style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 10px;">
    <br>
    <h3 style="font-weight: bold; margin: 20px 0 7px; padding: 0; line-height: 35px; font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase; border-bottom: 4px solid #ddd;">
        [{ oxmultilang ident="EMAIL_WCP_PAID_PAID" }]
    </h3>

    <h3 style="font-weight: bold; margin: 20px 0 7px; padding: 0; line-height: 35px; font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase; border-bottom: 4px solid #ddd;">
        [{ oxmultilang ident="EMAIL_WCP_PAID_ORDERNUMBER" }] [{ $order->oxorder__oxordernr->value }]
    </h3>

    <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td style="padding: 5px; border-bottom: 4px solid #ddd;" width="70" align="right">
          <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
              <b>[{ oxmultilang ident="EMAIL_WCP_PAID_QUANTITY" }]</b>
          </p>
      </td>
      <td style="padding: 5px; border-bottom: 4px solid #ddd;">
          <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
              <b>[{ oxmultilang ident="EMAIL_WCP_PAID_PRODUCT" }]</b>
          </p>
      </td>
      <td style="padding: 5px; border-bottom: 4px solid #ddd;" width="150" align="right">
          <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
              [{ oxmultilang ident="EMAIL_WCP_PAID_PRODUCTRATING" }]
          </p>
      </td>
    </tr>
    [{foreach from=$order->getOrderArticles() item=oOrderArticle}]
      <tr valign="top">
        <td style="padding: 5px; border-bottom: 1px solid #ddd;" align="right">
            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                [{ $oOrderArticle->oxorderarticles__oxamount->value }]
            </p>
        </td>
        <td style="padding: 5px; border-bottom: 1px solid #ddd;">
            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                [{ $oOrderArticle->oxorderarticles__oxtitle->value }] [{ $oOrderArticle->oxorderarticles__oxselvariant->value }]
                <br>[{ oxmultilang ident="EMAIL_WCP_PAID_ARTNUMBER" }] [{ $oOrderArticle->oxorderarticles__oxartnum->value }]
            </p>
        </td>
        <td style="padding: 5px; border-bottom: 1px solid #ddd;" align="right">
            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                <a href="[{ $oViewConf->getBaseDir() }]index.php?shp=[{$shop->oxshops__oxid->value}]&amp;anid=[{ $oOrderArticle->oxorderarticles__oxartid->value }]&amp;cl=review&amp;reviewuserhash=[{$reviewuserhash}]" style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 10px;" target="_blank">[{ oxmultilang ident="EMAIL_WCP_PAID_REVIEW" }]</a>
            </p>
        </td>
      </tr>
    [{/foreach}]
    </table>

    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
        [{ oxmultilang ident="EMAIL_WCP_PAID_YUORTEAM1" }] [{ $shop->oxshops__oxname->value }] [{ oxmultilang ident="EMAIL_WCP_PAID_YUORTEAM2" }]
    </p>

    [{if $oViewConf->showTs("ORDERCONFEMAIL") && $oViewConf->getTsId() }]
        <h3 style="font-weight: bold; margin: 20px 0 7px; padding: 0; line-height: 35px; font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase; border-bottom: 4px solid #ddd;">
            [{ oxmultilang ident="EMAIL_WCP_PAID_HTML_TS_RATINGS_RATEUS" }]
        </h3>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
            <a href="[{ $oViewConf->getTsRatingUrl() }]" target="_blank" title="[{ oxmultilang ident="TS_RATINGS_URL_TITLE" }]">
                <img src="https://www.trustedshops.com/bewertung/widget/img/bewerten_de.gif" border="0" alt="[{ oxmultilang ident="TS_RATINGS_BUTTON_ALT" }]" align="middle">
            </a>
        </p>
    [{/if}]
  </body>
</html>
[{ oxcontent ident="oxorderwcppaidplainemail" }]

[{ oxmultilang ident="EMAIL_WCP_PAID_PAID" }]

[{ oxmultilang ident="EMAIL_WCP_PAID_ORDERNUMBER" }] [{ $order->oxorder__oxordernr->value }]

[{foreach from=$order->getOrderArticles() item=oOrderArticle}]
[{ $oOrderArticle->oxorderarticles__oxamount->value }] [{ $oOrderArticle->oxorderarticles__oxtitle->getRawValue() }] [{ $oOrderArticle->oxorderarticles__oxselvariant->getRawValue() }]
[{/foreach}]

[{ oxmultilang ident="EMAIL_WCP_PAID_YUORTEAM1" }] [{ $shop->oxshops__oxname->getRawValue() }] [{ oxmultilang ident="EMAIL_WCP_PAID_YUORTEAM2" }]

[{if $oViewConf->showTs("ORDERCONFEMAIL") && $oViewConf->getTsId() }]
[{ oxmultilang ident="EMAIL_WCP_PAID_HTML_TS_RATINGS_RATEUS" }]
[{ $oViewConf->getTsRatingUrl() }]
[{/if}]

[{ oxcontent ident="oxemailfooterplain" }]
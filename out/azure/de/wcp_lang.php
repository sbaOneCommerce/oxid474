<?php
/**
    Shop System Plugins - Terms of use

    This terms of use regulates warranty and liability between Wirecard
    Central Eastern Europe (subsequently referred to as WDCEE) and it's
    contractual partners (subsequently referred to as customer or customers)
    which are related to the use of plugins provided by WDCEE.

    The Plugin is provided by WDCEE free of charge for it's customers and
    must be used for the purpose of WDCEE's payment platform integration
    only. It explicitly is not part of the general contract between WDCEE
    and it's customer. The plugin has successfully been tested under
    specific circumstances which are defined as the shopsystem's standard
    configuration (vendor's delivery state). The Customer is responsible for
    testing the plugin's functionality before putting it into production
    enviroment.
    The customer uses the plugin at own risk. WDCEE does not guarantee it's
    full functionality neither does WDCEE assume liability for any
    disadvantage related to the use of this plugin. By installing the plugin
    into the shopsystem the customer agrees to the terms of use. Please do
    not use this plugin if you do not agree to the terms of use!
*/

$sLangName  = "Deutsch";
$iLangNr    = 1;
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
'charset'                                         => 'ISO-8859-15',
'wcp_payment_page_redirect'                      => 'Bitte warten sie kurz bis sie zu unserem Bezahldienstleister weitergeleitet werden.',
'wcp_return_redirect'                            => 'Bitte warten. Ihre Bezahlung wird verarbeitet...',
'wcp_payment_pending'                            => 'Auf eine Best&auml;tigung der Zahlung wird noch gewartet.',
'wcp_payment_canceled'                           => 'Sie haben den Bezahlvorgang abgebrochen.',
'wcp_payment_failure'                            => 'W&auml;hrend des Bezahlvorgangs ist ein Fehler aufgetreten',
'EMAIL_WCP_PAID_SUBJECT'                    => 'Zahlung erfolgreich eingegangen.',
'EMAIL_WCP_PAID_PAID'                       => 'Die Zahlung wurde erfolgreich durchgef&uuml;hrt',
'EMAIL_WCP_PAID_ORDERNUMBER'                => 'Ihre Bestellnr.:',
'EMAIL_WCP_PAID_QUANTITY'                   => 'Anzahl',
'EMAIL_WCP_PAID_PRODUCT'                    => 'Artikel',
'EMAIL_WCP_PAID_PRODUCTRATING'              => 'Artikel bewerten',
'EMAIL_WCP_PAID_ARTNUMBER'                  => 'Art.Nr.:',
'EMAIL_WCP_PAID_REVIEW'                     => 'bewerten',
'EMAIL_WCP_PAID_YUORTEAM1'                  => 'Ihr',
'EMAIL_WCP_PAID_YUORTEAM2'                  => 'Team',
);

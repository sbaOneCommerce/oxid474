[{ assign var="shop"      value=$oEmailView->getShop() }]
[{ assign var="oViewConf" value=$oEmailView->getViewConfig() }]

[{block name="email_plain_wcpPaid_sendemail"}]
[{ oxcontent ident="oxorderwcppaidplainemail" }]
[{/block}]

[{block name="email_plain_wcpPaid_infoheader"}]
[{ oxmultilang ident="EMAIL_WCP_PAID_PAID" }]
[{/block}]

[{block name="email_plain_wcpPaid_oxordernr"}]
[{ oxmultilang ident="EMAIL_WCP_PAID_ORDERNUMBER" }] [{ $order->oxorder__oxordernr->value }]
[{/block}]

[{block name="email_plain_wcpPaid_orderarticles"}]
[{foreach from=$order->getOrderArticles() item=oOrderArticle}]
[{ $oOrderArticle->oxorderarticles__oxamount->value }] [{ $oOrderArticle->oxorderarticles__oxtitle->getRawValue() }] [{ $oOrderArticle->oxorderarticles__oxselvariant->getRawValue() }]
[{/foreach}]
[{/block}]

[{block name="email_plain_wcpPaid_infofooter"}]
[{ oxmultilang ident="EMAIL_WCP_PAID_YUORTEAM1" }] [{ $shop->oxshops__oxname->getRawValue() }] [{ oxmultilang ident="EMAIL_WCP_PAID_YUORTEAM2" }]
[{/block}]

[{block name="email_plain_wcpPaid_ts"}]
[{if $oViewConf->showTs("ORDERCONFEMAIL") && $oViewConf->getTsId() }]
[{ oxmultilang ident="EMAIL_WCP_PAID_HTML_TS_RATINGS_RATEUS" }]
[{ $oViewConf->getTsRatingUrl() }]
[{/if}]
[{/block}]

[{ oxcontent ident="oxemailfooterplain" }]
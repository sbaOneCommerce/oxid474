<?php
/**
    Shop System Plugins - Terms of use

    This terms of use regulates warranty and liability between Wirecard
    Central Eastern Europe (subsequently referred to as WDCEE) and it's
    contractual partners (subsequently referred to as customer or customers)
    which are related to the use of plugins provided by WDCEE.

    The Plugin is provided by WDCEE free of charge for it's customers and
    must be used for the purpose of WDCEE's payment platform integration
    only. It explicitly is not part of the general contract between WDCEE
    and it's customer. The plugin has successfully been tested under
    specific circumstances which are defined as the shopsystem's standard
    configuration (vendor's delivery state). The Customer is responsible for
    testing the plugin's functionality before putting it into production
    enviroment.
    The customer uses the plugin at own risk. WDCEE does not guarantee it's
    full functionality neither does WDCEE assume liability for any
    disadvantage related to the use of this plugin. By installing the plugin
    into the shopsystem the customer agrees to the terms of use. Please do
    not use this plugin if you do not agree to the terms of use!
*/

$sLangName  = "English";
$iLangNr    = 1;
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
//Wirecard CEE Text START
'charset'                                       => 'ISO-8859-15',
'wcp_config_title'                             => 'Wirecard CEE Configuration',
'wcp_parameter_settings_title'                 => 'Parameter Settings',
'wcp_settings'                                 => 'Wirecard Checkout Page Settings',
'wcp_save_message'                             => 'Settings have been saved successfully.',
'wcp_do_not_move'                              => 'do not move',
'wcp_customer_id_desc'                         => 'Your customer number. (D2#####)',
'wcp_shop_id_desc'                             => 'Shop site identifier required when more than one licence is used for a single customer number.',
'wcp_secret_desc'                              => 'Preshared key for signing the transfered parameters.',
'wcp_image_url_desc'                           => 'Link to your logo embedded on the Wirecard Checkout Page pages.',
'wcp_service_url_desc'                         => 'URL connecting to a page providing service information.',
'wcp_display_text_desc'                        => 'Text displayed to the consumer along with the order data.',
'wcp_customer_statement_desc'                  => 'Text which is to appear customer\'s statement.',
'wcp_max_retries_desc'                         => 'Maximum number of payment attempts.',
'wcp_auto_deposit_desc'                        => 'Activates automatic settling of a payment.',
'wcp_background_color_desc'                    => 'Background color used by Wirecard Checkout Page in hex without # (e.g. 0beef0)',
'wcp_plugin_settings_title'                    => 'Plugin Settings',
'wcp_use_iframe_title'                         => 'USE IFRAME',
'wcp_use_iframe_desc'                          => 'Display Paymentpage in an IFrame (Yes) or redirect to the Paymentpage (No)',
'wcp_use_iframe_note'                          => 'Note: Your design must not be XHTML Strict for this feature. XHTML Strict does not support IFrames.',
'wcp_send_additional_data_title'               => 'Send consumer-informations',
'wcp_send_additional_data_desc'                => 'Send consumer address-informations for address-verification.',
'wcp_log_confirmations_title'                  => 'LOG CONFIRMATIONS',
'wcp_log_confirmations_desc'                   => 'Debug logging for Configmation Requests (log/wirecardCheckoutPageConfirm.log)',
'wcp_paid_mail_to_owner_title'                 => 'Deliver Payment-success mail to Shopowner.',
'wcp_paid_mail_to_owner_desc'                  => 'If you activate this action you choose to send an payment-success mail to the Shopowners Mail address.',
'wcp_checkout_folder_title'                    => 'CHECKOUT FOLDER',
'wcp_checkout_folder_desc'                     => 'Folder new orders will be moved before payment process finished.',
'wcp_success_folder_title'                     => 'SUCCESS FOLDER',
'wcp_success_folder_desc'                      => 'Folder orders will be moved if payment was successful.',
'wcp_pending_folder_title'                     => 'PENDING FOLDER',
'wcp_pending_folder_desc'                      => 'Folder orders will be moved if payment is pending.',
'wcp_cancel_folder_title'                      => 'CANCEL FOLDER',
'wcp_cancel_folder_desc'                       => 'Folder orders will be moved if the customer canceled payment.',
'wcp_failure_folder_title'                     => 'FAILURE FOLDER',
'wcp_failure_folder_desc'                      => 'Folder orders will be moved if an error occured during the payment process.',
'wcp_terms_of_use_title'                       => 'Terms of use',
'wcp_terms_of_use_desc'                        =>
'<p>This terms of use regulates warranty and liability between Wirecard<br />
Central Eastern Europe (subsequently referred to as WDCEE) and it\'s<br />
contractual partners (subsequently referred to as customer or customers)<br />
which are related to the use of plugins provided by WDCEE.
</p>
<p>The Plugin is provided by WDCEE free of charge for it\'s customers and<br />
must be used for the purpose of WDCEE\'s payment platform integration<br />
only. It explicitly is not part of the general contract between WDCEE<br />
and it\'s customer. The plugin has successfully been tested under<br />
specific circumstances which are defined as the shopsystem\'s standard<br />
configuration (vendor\'s delivery state). The Customer is responsible for<br />
testing the plugin\'s functionality before putting it into production<br />
enviroment.
</p>
<p>The customer uses the plugin at own risk. WDCEE does not guarantee it\'s<br />
full functionality neither does WDCEE assume liability for any<br />
disadvantage related to the use of this plugin. By installing the plugin<br />
into the shopsystem the customer agrees to the terms of use. Please do<br />
not use this plugin if you do not agree to the terms of use!
</p>'
);

[{include file="headitem.tpl" }]
[{* TODO: get rid of the tables. prio: -2 *}]
<form name="transfer" id="transfer" action="[{ $oViewConf->getSelfLink() }]" method="post">
    [{include file="wcp_formparams.tpl" cl="wcp_shopsettings" lstrt=$lstrt actedit=$actedit oxid=$oxid fnc="" language=$actlang editlanguage=$actlang delshopid="" updatenav=""}]
</form>

<table cellspacing="0" cellpadding="0" border="0" width="99%" height="100%">
    <tr>
        <td width="100%" align="center" valign="top" bgcolor="#E7EAED" style="border : 1px #000000; border-style : none none solid none;">
            <table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
                <tr>
                    <td></td>
                    <td colspan="2"><h2>[{ oxmultilang ident="wcp_config_title" }]</h2></td>
                </tr>
                <tr>
                    <td width="15"></td>
                    <td valign="top" class="edittext">
                        <form name="myedit" id="myedit" action="[{ $shop->selflink }]" method="post">
                            [{include file="wcp_formparams.tpl" cl="wcp_shopsettings" lstrt=$lstrt actedit=$actedit oxid=$oxid fnc="save" language=$actlang editlanguage=$actlang delshopid="" updatenav=""}]
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td colspan="2" valign="top" class="edittext" style="border-bottom: 0px;"><h3>[{ oxmultilang ident="wcp_plugin_settings_title" }]</h3></td>
                                </tr>
                                [{ if $wcp_saved}]
                                    <tr>
                                        <td colspan="2" class="edittext" style="color: #4F8A10; background-color: #DFF2BF; border: 1px solid; font-weight: bold; padding: 4px;">[{ oxmultilang ident="wcp_save_message" }]</td>
                                    <tr>
                                    <tr>
                                        <td colspan="2" valign="top" class="edittext"><hr></td>
                                    </tr>
                                [{/if}]
                                <!-- iFrame -->
                                <tr>
                                    <td valign="top" class="edittext">[{ oxmultilang ident="wcp_use_iframe_title" }]</td>
                                    <td valign="top" class="edittext">
                                        <select name=confstrs[WCP_USE_IFRAME]>
                                          [{if $confstrs.WCP_USE_IFRAME == yes}] 
                                          <option value="yes" selected="selected">[{ oxmultilang ident="GENERAL_YES" }]</option>
                                          <option value="no">[{ oxmultilang ident="GENERAL_NO" }]</option>
                                          [{else}]
                                          <option value="yes">[{ oxmultilang ident="GENERAL_YES" }]</option>
                                          <option value="no" selected="selected">[{ oxmultilang ident="GENERAL_NO" }]</option>
                                          [{/if}]
                                        </select>
                                        <p>
                                            [{ oxmultilang ident="wcp_use_iframe_desc" }]
                                        </p>
                                        <p>
                                            <i>
                                                [{ oxmultilang ident="wcp_use_iframe_note" }]
                                            </i>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- send additional data -->
                                <tr>
                                    <td valign="top" class="edittext">[{ oxmultilang ident="wcp_send_additional_data_title" }]</td>
                                    <td valign="top" class="edittext">
                                        <select name=confstrs[WCP_SEND_ADDITIONAL_DATA]>
                                          [{if $confstrs.WCP_SEND_ADDITIONAL_DATA == yes}] 
                                          <option value="yes" selected="selected">[{ oxmultilang ident="GENERAL_YES" }]</option>
                                          <option value="no">[{ oxmultilang ident="GENERAL_NO" }]</option>
                                          [{else}]
                                          <option value="yes">[{ oxmultilang ident="GENERAL_YES" }]</option>
                                          <option value="no" selected="selected">[{ oxmultilang ident="GENERAL_NO" }]</option>
                                          [{/if}]
                                        </select>
                                        <p>
                                            [{ oxmultilang ident="wcp_send_additional_data_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- log confirmations -->
                                <tr>
                                    <td valign="top" class="edittext">[{ oxmultilang ident="wcp_log_confirmations_title" }]</td>
                                    <td valign="top" class="edittext">
                                        <select name=confstrs[WCP_LOG_CONFIRMATION]>
                                          [{if $confstrs.WCP_LOG_CONFIRMATION == yes}] 
                                          <option value="yes" selected="selected">[{ oxmultilang ident="GENERAL_YES" }]</option>
                                          <option value="no">[{ oxmultilang ident="GENERAL_NO" }]</option>
                                          [{else}]
                                          <option value="yes">[{ oxmultilang ident="GENERAL_YES" }]</option>
                                          <option value="no" selected="selected">[{ oxmultilang ident="GENERAL_NO" }]</option>
                                          [{/if}]
                                        </select>
                                        <p>
                                            [{ oxmultilang ident="wcp_log_confirmations_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- paid mail -->
                                <tr>
                                    <td valign="top" class="edittext">[{ oxmultilang ident="wcp_paid_mail_to_owner_title" }]</td>
                                    <td valign="top" class="edittext">
                                        <select name=confstrs[WCP_PAID_MAIL_TO_OWNER]>
                                          [{if $confstrs.WCP_PAID_MAIL_TO_OWNER == yes}] 
                                          <option value="yes" selected="selected">[{ oxmultilang ident="GENERAL_YES" }]</option>
                                          <option value="no">[{ oxmultilang ident="GENERAL_NO" }]</option>
                                          [{else}]
                                          <option value="yes">[{ oxmultilang ident="GENERAL_YES" }]</option>
                                          <option value="no" selected="selected">[{ oxmultilang ident="GENERAL_NO" }]</option>
                                          [{/if}]
                                        </select>
                                        <p>
                                            [{ oxmultilang ident="wcp_paid_mail_to_owner_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- checkout folder -->
                                <tr>
                                    <td valign="top" class="edittext">[{ oxmultilang ident="wcp_checkout_folder_title" }]</td>
                                    <td valign="top" class="edittext">
                                        <select name=confstrs[WCP_CHECKOUT_FOLDER]>
                                            <option value="">[{ oxmultilang ident=wcp_do_not_move noerror=true }]</option>
                                            [{foreach from=$afolder key=field item=color}]
                                            <option value="[{ $field }]" [{ if $confstrs.WCP_CHECKOUT_FOLDER == $field }]selected="selected"[{/if}] style="color: [{ $color }];">[{ oxmultilang ident=$field noerror=true }]</option>
                                            [{/foreach}]
                                        </select>
                                        <p>
                                            [{ oxmultilang ident="wcp_checkout_folder_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- success folder -->
                                <tr>
                                    <td valign="top" class="edittext">[{ oxmultilang ident="wcp_success_folder_title" }]</td>
                                    <td valign="top" class="edittext">
                                        <select name=confstrs[WCP_SUCCESS_FOLDER]>
                                            <option value="">[{ oxmultilang ident=wcp_do_not_move noerror=true }]</option>
                                            [{foreach from=$afolder key=field item=color}]
                                            <option value="[{ $field }]" [{ if $confstrs.WCP_SUCCESS_FOLDER == $field }]selected="selected"[{/if}] style="color: [{ $color }];">[{ oxmultilang ident=$field noerror=true }]</option>
                                            [{/foreach}]
                                        </select>
                                        <p>
                                            [{ oxmultilang ident="wcp_success_folder_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- pending folder -->
                                <tr>
                                    <td valign="top" class="edittext">[{ oxmultilang ident="wcp_pending_folder_title" }]</td>
                                    <td valign="top" class="edittext">
                                        <select name=confstrs[WCP_PENDING_FOLDER]>
                                            <option value="">[{ oxmultilang ident=wcp_do_not_move noerror=true }]</option>
                                            [{foreach from=$afolder key=field item=color}]
                                            <option value="[{ $field }]" [{ if $confstrs.WCP_PENDING_FOLDER == $field }]selected="selected"[{/if}] style="color: [{ $color }];">[{ oxmultilang ident=$field noerror=true }]</option>
                                            [{/foreach}]
                                        </select>
                                        <p>
                                            [{ oxmultilang ident="wcp_pending_folder_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- cancel folder -->
                                <tr>
                                    <td valign="top" class="edittext">[{ oxmultilang ident="wcp_cancel_folder_title" }]</td>
                                    <td valign="top" class="edittext">
                                        <select name=confstrs[WCP_CANCEL_FOLDER]>
                                            <option value="">[{ oxmultilang ident=wcp_do_not_move noerror=true }]</option>
                                            [{foreach from=$afolder key=field item=color}]
                                            <option value="[{ $field }]" [{ if $confstrs.WCP_CANCEL_FOLDER == $field }]selected="selected"[{/if}] style="color: [{ $color }];">[{ oxmultilang ident=$field noerror=true }]</option>
                                            [{/foreach}]
                                        </select>
                                        <p>
                                            [{ oxmultilang ident="wcp_cancel_folder_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- failure folder -->
                                <tr>
                                    <td valign="top" class="edittext">[{ oxmultilang ident="wcp_failure_folder_title" }]</td>
                                    <td valign="top" class="edittext">
                                        <select name=confstrs[WCP_FAILURE_FOLDER]>
                                            <option value="">[{ oxmultilang ident=wcp_do_not_move noerror=true }]</option>
                                            [{foreach from=$afolder key=field item=color}]
                                            <option value="[{ $field }]" [{ if $confstrs.WCP_FAILURE_FOLDER == $field }]selected="selected"[{/if}] style="color: [{ $color }];">[{ oxmultilang ident=$field noerror=true }]</option>
                                            [{/foreach}]
                                        </select>
                                        <p>
                                            [{ oxmultilang ident="wcp_failure_folder_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- save -->
                                <tr>
                                    <td class="edittext"></td>
                                    <td class="edittext"><br>
                                        <input type="submit" class="confinput" name="save" value="[{ oxmultilang ident="GENERAL_SAVE" }]" onClick="Javascript:document.myedit.fnc.value='save'">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                    <td valign="top" class="edittext" align="left"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

[{include file="bottomnaviitem.tpl"}]

[{include file="bottomitem.tpl"}]
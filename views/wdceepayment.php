<?php
/**
    Shop System Plugins - Terms of use

    This terms of use regulates warranty and liability between Wirecard
    Central Eastern Europe (subsequently referred to as WDCEE) and it's
    contractual partners (subsequently referred to as customer or customers)
    which are related to the use of plugins provided by WDCEE.

    The Plugin is provided by WDCEE free of charge for it's customers and
    must be used for the purpose of WDCEE's payment platform integration
    only. It explicitly is not part of the general contract between WDCEE
    and it's customer. The plugin has successfully been tested under
    specific circumstances which are defined as the shopsystem's standard
    configuration (vendor's delivery state). The Customer is responsible for
    testing the plugin's functionality before putting it into production
    enviroment.
    The customer uses the plugin at own risk. WDCEE does not guarantee it's
    full functionality neither does WDCEE assume liability for any
    disadvantage related to the use of this plugin. By installing the plugin
    into the shopsystem the customer agrees to the terms of use. Please do
    not use this plugin if you do not agree to the terms of use!
*/

/**
 * viewscript to render payment-redirect
 * if order-view would have been used there would be some problems with
 * low-stock items
 */
class wdceepayment extends oxUBase {
    protected $_oOrder = null;
    
    protected $_oBasket = null;
    
    protected static $_PAYMENT_URL = 'https://checkout.wirecard.com/page/init.php';
    
    protected static $_PLUGIN_VERSION = '2.4.0';
    
    protected static $_VALID_PAYMENT_TYPES = Array(
            'oxWCPSelect'               => 'SELECT',
            'oxWCPCCard'                => 'CCARD',
            'oxWCPCCardMoto'            => 'CCARD-MOTO',
            'oxWCPMaestro'              => 'MAESTRO',
            'oxWCPPaybox'               => 'PBX',
            'oxWCPPaysafecard'          => 'PSC',
            'oxWCPEPS'                  => 'EPS',
            'oxWCPDirectDebit'          => 'ELV',
            'oxWCPQuick'                => 'QUICK',
            'oxWCPIdeal'                => 'IDL',
            'oxWCPGiropay'              => 'GIROPAY',
            'oxWCPPaypal'               => 'PAYPAL',
            'oxWCPSofortueberweisung'   => 'SOFORTUEBERWEISUNG',
            'oxWCPClick2Pay'            => 'C2P',
            'oxWCPInvoice'              => 'INVOICE',
            'oxWCPInstallment'          => 'INSTALLMENT',
            'oxWCPBanContactMisterCash' => 'BANCONTACT_MISTERCASH',
            'oxWCPPrzelewy24'           => 'PRZELEWY24',
            'oxWCPMonetaRu'             => 'MONETA',
            'oxWCPPoli'                 => 'POLI',
            'oxWCPEKonto'               => 'EKONTO',
            'oxWCPInstantBank'          => 'INSTANTBANK',
            'oxWCPSkrillDirect'         => 'SKRILLDIRECT',
            'oxWCPSkrillWallet'         => 'SKRILLWALLET',
            'oxWCPMpass'                => 'MPASS'
    );

    protected static $_PAYMENT_CANCELED_ERRORCODE = 7;
    protected static $_PAYMENT_FAILED_ERRORCODE = 8;
    protected static $_LOG_FILE_NAME = 'wirecardCheckoutPageConfirm.log';

    public function init() {
        if(!oxSession::getVar('sess_challenge')) {
            if(isset($_POST['sess_challenge'])) {
                oxSession::setVar('sess_challenge', $_POST['sess_challenge']);
            }
            else {
                oxSession::setVar('sess_challenge', '');
            }
        }

        $oOrder = $this->_getOrder();
        // not a valid order or wcp payment. redirect to payment selection.
        if(!$oOrder || !self::isValidWCPPayment($oOrder->oxorder__oxpaymenttype->value)) {
            self::paymentRedirect();
        }

        parent::init();
    }

    /**
     * Writes a log-line to confirm-logfile.
     * 
     * @param $text log
     *            text
     */
    protected function _wcpConfirmLogging($text) {
        $date = date("Y-m-d H:i:s");
        if($this->getConfig()->getConfigParam('WCP_LOG_CONFIRMATION') == 'yes') {
            oxUtils::getInstance()->writeToLog($date . ': ' . $text . "\n", self::$_LOG_FILE_NAME);
        }
    }

    protected function _getOrder() {
        if($this->_oOrder === null) {
            $oOrder = oxNew('oxorder');
            $oOrder->load(oxSession::getVar('sess_challenge'));
            $this->_oOrder = $oOrder;
        }
        return $this->_oOrder;
    }

    protected function _getBasket() {
        if($this->_oBasket === null) {
            $this->_oBasket = false;
            if($oBasket = $this->getSession()->getBasket()) {
                $this->_oBasket = $oBasket;
            }
        }
        return $this->_oBasket;
    }

    public function checkoutIFrame() {
        $oConfig = $this->getConfig();
        $this->addGlobalParams();
        $this->_aViewData['oxcmp_basket'] = $this->_getBasket();
        $this->_aViewData['wcpIFrameUrl'] = html_entity_decode($oConfig->getShopCurrentUrl()) . 'cl=wdceepayment&fnc=checkoutForm';
        $this->_sThisTemplate = 'page/checkout/wcp_checkout_iframe.tpl';
    }

    /**
     * generating autosubmiting checkout form.
     */
    public function checkoutForm() {
        $oOrder = $this->_getOrder();
        $oConfig = $this->getConfig();
        $oOrder = $this->_setOrderFolder($oOrder, $oConfig->getConfigParam('WCP_CHECKOUT_FOLDER'));
        $oOrder->save();
        $request = $this->_createWCPRequestArray($oOrder);
        $this->_aViewData['wcpRequest'] = $request;
        $this->_aViewData['wcpPaymentUrl'] = self::$_PAYMENT_URL;
        $this->_sThisTemplate = 'page/checkout/wcp_checkout_page.tpl';
    }

    /**
     * creates an array used for WCP Checkout.
     * 
     * @param $oOrder -
     *            the oxOrder Object
     * @return String[] - Array with all Requestparameters
     */
    protected function _createWCPRequestArray($oOrder) {
        $oConfig = $this->getConfig();
        $returnUrl = $oConfig->getShopCurrentUrl();
        $sUrl = $oConfig->getShopCurrentUrl() . 'action=confirm';
        $oStr = getStr();
        $sUrl = $oStr->preg_replace('/(\?|&(amp;)?)force_sid=[a-z0-9\._]+&?(amp;)?/i', '\1', $sUrl);
        $confirmUrl = $sUrl;
        $shopName = 'Oxid ' . $oConfig->getEdition();
        $shopVersion = $oConfig->getVersion() . ' ' . $oConfig->getRevision();
        $pluginName = 'WirecardCEE_WCP';
        $pluginVersion = self::$_PLUGIN_VERSION;
        $versionString = base64_encode($shopName . '; ' . $shopVersion . '; ; ' . $pluginName . '; ' . $pluginVersion);
        
        $request = Array();
        
        // WCP RequestParameters
        $request['customerId'] = $oConfig->getConfigParam('WCP_CUSTOMER_ID');
        $request['shopId'] = $oConfig->getConfigParam('WCP_SHOP_ID');
        $request['amount'] = $oOrder->getTotalOrderSum();
        $request['currency'] = $oConfig->getActShopCurrencyObject()->name;
        $request['paymenttype'] = self::$_VALID_PAYMENT_TYPES[$oOrder->oxorder__oxpaymenttype->value];
        $request['language'] = oxLang::getInstance()->getLanguageAbbr();
        $request['orderDescription'] = $oOrder->oxorder__oxordernr->value;
        $request['displayText'] = $oConfig->getConfigParam('WCP_DISPLAY_TEXT');
        $request['successUrl'] = $returnUrl;
        $request['cancelUrl'] = $returnUrl;
        $request['failureUrl'] = $returnUrl;
        $request['pendingUrl'] = $returnUrl;
        $request['serviceUrl'] = $oConfig->getConfigParam('WCP_SERVICE_URL');
        $request['confirmUrl'] = $confirmUrl;
        $request['cl'] = 'wdceepayment';
        $request['fnc'] = 'returnPage';
        $request['imageUrl'] = $oConfig->getConfigParam('WCP_IMAGE_URL');
        $request['customerStatement'] = $oOrder->oxorder__oxordernr->value;
        $request['orderReference'] = $oOrder->oxorder__oxordernr->value;
        $request['autoDeposit'] = $oConfig->getConfigParam('WCP_AUTO_DEPOSIT');
        $request['pluginVersion'] = $versionString;
        $request['backgroundColor'] = $oConfig->getConfigParam('WCP_BACKGROUND_COLOR');
        
        if($oConfig->getConfigParam('WCP_MAX_RETRIES') >= 0) {
            $request['maxRetries'] = $oConfig->getConfigParam('WCP_MAX_RETRIES');
        }
        
        if($oConfig->getConfigParam('WCP_SEND_ADDITIONAL_DATA') == 'yes') {
            $consumerBillingFirstname = $oOrder->oxorder__oxbillfname->value;
            $consumerBillingLastname = $oOrder->oxorder__oxbilllname->value;
            $consumerBillingAddress1 = $oOrder->oxorder__oxbillstreet->value;
            $consumerBillingAddress2 = $oOrder->oxorder__oxbillstreetnr->value;
            $consumerBillingState = $oOrder->oxorder__oxbillstateid->value;
            $consumerBillingZipCode = $oOrder->oxorder__oxbillzip->value;
            $consumerBillingCity = $oOrder->oxorder__oxbillcity->value;
            $consumerBillingCountryId = $oOrder->oxorder__oxbillcountryid->value;
            $oDB = oxDb::GetDB();
            $consumerBillingCountry = $oDB->getOne("select oxisoalpha2 from oxcountry where oxid = '$consumerBillingCountryId'");
            $consumerBillingPhone = $oOrder->oxorder__oxbillfon->value;
            $consumerBillingFax = $oOrder->oxorder__oxbillfax->value;
            
            $consumerShippingData = $oOrder->getDelAddressInfo();
            if($consumerShippingData) {
                $consumerShippingFirstname = $consumerShippingData->oxaddress__oxfname->value;
                $consumerShippingLastname = $consumerShippingData->oxaddress__oxlname->value;
                $consumerShippingAddress1 = $consumerShippingData->oxaddress__oxstreet->value;
                $consumerShippingAddress2 = $consumerShippingData->oxaddress__oxstreetnr->value;
                $consumerShippingState = $consumerShippingData->oxaddress__oxstateid->value;
                $consumerShippingZipCode = $consumerShippingData->oxaddress__oxzip->value;
                $consumerShippingCity = $consumerShippingData->oxaddress__oxcity->value;
                $consumerShippingCountryId = $consumerShippingData->oxaddress__oxcountryid->value;
                $consumerShippingCountry = $oDB->getOne("select oxisoalpha2 from oxcountry where oxid = '$consumerShippingCountryId'");
                $consumerShippingPhone = $consumerShippingData->oxaddress__oxfon->value;
                $consumerShippingFax = $consumerShippingData->oxaddress__oxfax->value;
            }
            else {
                $consumerShippingFirstname = $consumerBillingFirstname;
                $consumerShippingLastname = $consumerBillingLastname;
                $consumerShippingAddress1 = $consumerBillingAddress1;
                $consumerShippingAddress2 = $consumerBillingAddress2;
                $consumerShippingState = $consumerBillingState;
                $consumerShippingZipCode = $consumerBillingZipCode;
                $consumerShippingCity = $consumerBillingCity;
                $consumerShippingCountry = $consumerBillingCountry;
                $consumerShippingPhone = $consumerBillingPhone;
                $consumerShippingFax = $consumerBillingFax;
            }
            $consumerEmail = $oOrder->oxorder__oxbillemail->value;
            $oUser = $oOrder->getOrderUser();
            
            // processing birth date which came from output as array
            $consumerBirthDate = is_array($oUser->oxuser__oxbirthdate->value) ? $oUser->convertBirthday($oUser->oxuser__oxbirthdate->value) : $oUser->oxuser__oxbirthdate->value;

            $request['consumerBillingFirstname'] = $consumerBillingFirstname;
            $request['consumerBillingLastname'] = $consumerBillingLastname;
            $request['consumerBillingAddress1'] = $consumerBillingAddress1;
            $request['consumerBillingAddress2'] = $consumerBillingAddress2;
            $request['consumerBillingCity'] = $consumerBillingCity;
            $request['consumerBillingCountry'] = $consumerBillingCountry;
            $request['consumerBillingState'] = $consumerBillingState;
            $request['consumerBillingZipCode'] = $consumerBillingZipCode;
            $request['consumerBillingPhone'] = $consumerBillingPhone;
            $request['consumerBillingFax'] = $consumerBillingFax;
            $request['consumerShippingFirstname'] = $consumerShippingFirstname;
            $request['consumerShippingLastname'] = $consumerShippingLastname;
            $request['consumerShippingAddress1'] = $consumerShippingAddress1;
            $request['consumerShippingAddress2'] = $consumerShippingAddress2;
            $request['consumerShippingCity'] = $consumerShippingCity;
            $request['consumerShippingCountry'] = $consumerShippingCountry;
            $request['consumerShippingState'] = $consumerShippingState;
            $request['consumerShippingZipCode'] = $consumerShippingZipCode;
            $request['consumerShippingPhone'] = $consumerShippingPhone;
            $request['consumerShippingFax'] = $consumerShippingFax;
            $request['consumerEmail'] = $consumerEmail;
            
            if($consumerBirthDate != '0000-00-00') {
                $request['consumerBirthDate'] = $consumerBirthDate;
            }
        }
        else 
            if(in_array(self::$_VALID_PAYMENT_TYPES[$oOrder->oxorder__oxpaymenttype->value], Array('INVOICE', 'INSTALLMENT'))) {
                $consumerBillingFirstname = $oOrder->oxorder__oxbillfname->value;
                $consumerBillingLastname = $oOrder->oxorder__oxbilllname->value;
                $consumerBillingAddress1 = $oOrder->oxorder__oxbillstreet->value;
                $consumerBillingZipCode = $oOrder->oxorder__oxbillzip->value;
                $consumerBillingCity = $oOrder->oxorder__oxbillcity->value;
                $consumerBillingCountryId = $oOrder->oxorder__oxbillcountryid->value;
                $oDB = oxDb::GetDB();
                $consumerBillingCountry = $oDB->getOne("select oxisoalpha2 from oxcountry where oxid = '$consumerBillingCountryId'");
                $consumerEmail = $oOrder->oxorder__oxbillemail->value;
                $oUser = $oOrder->getOrderUser();
                
                // processing birth date which came from output as array
                $consumerBirthDate = is_array($oUser->oxuser__oxbirthdate->value) ? $oUser->convertBirthday($oUser->oxuser__oxbirthdate->value) : $oUser->oxuser__oxbirthdate->value;
                
                $request['consumerBillingFirstname'] = $consumerBillingFirstname;
                $request['consumerBillingLastname'] = $consumerBillingLastname;
                $request['consumerBillingAddress1'] = $consumerBillingAddress1;
                $request['consumerBillingCity'] = $consumerBillingCity;
                $request['consumerBillingCountry'] = $consumerBillingCountry;
                $request['consumerBillingZipCode'] = $consumerBillingZipCode;
                $request['consumerEmail'] = $consumerEmail;
                if($consumerBirthDate != '0000-00-00') {
                    $request['consumerBirthDate'] = $consumerBirthDate;
                }
            }
        
        if($this->getConfig()->getConfigParam('WCP_USE_IFRAME') == 'yes') {
            $request['checkoutType'] = 'IFRAME';
            $request['windowName'] = 'wcpIFrame';
        }
        else {
            $request['checkoutType'] = 'PAGE';
        }

        $request['sess_challenge'] = oxSession::getVar('sess_challenge');

        $requestFingerprintOrder = 'secret';
        $requestFingerprintSeed = $oConfig->getConfigParam('WCP_SECRET');

        foreach($request as $key => $value) {
            $value = trim($value);
            if(!empty($value)) {
                $requestFingerprintSeed .= htmlspecialchars_decode($value, ENT_QUOTES);
                $requestFingerprintOrder .= ',' . $key;
            }
            else {
                unset($request[$key]);
            }
        }

        $requestFingerprintOrder .= ',requestFingerprintOrder';
        $requestFingerprintSeed .= $requestFingerprintOrder;
        $requestFingerprint = md5($requestFingerprintSeed);
        $request['requestFingerprintOrder'] = $requestFingerprintOrder;
        $request['requestFingerprint'] = $requestFingerprint;
        
        return $request;
    }

    public function returnPage() {
        if(isset($_POST['paymentState'])) {
            oxSession::setVar('wcpPaymentState', htmlentities($_POST['paymentState']));
        }
        
        if(isset($_GET['action']) && $_GET['action'] == 'confirm') {
            $this->_confirmProcess();
        }
        else {
            if(isset($_POST['checkoutType']) && $_POST['checkoutType'] == 'IFRAME') {
                $this->_returnIFramePage();
            }
            else {
                $this->_returnProcess();
            }
        }
    }

    protected function _returnIFramePage() {
        $oConfig = $this->getConfig();
        $this->_aViewData['wcpReturnUrl'] = html_entity_decode($oConfig->getShopCurrentUrl()) . 'cl=wdceepayment&fnc=returnPage';
        $this->_sThisTemplate = 'page/checkout/wcp_return_iframe.tpl';
    }

    protected function _returnProcess() {
        $oOrder = $this->_getOrder();
        if($oOrder->oxorder__oxtransstatus->value == 'PAID') {
            // send mail
            $oBasket = $this->_getBasket();
            $oUser = $this->getUser();
            
            $mail = method_exists($oOrder, 'sendWCPOrderByEmail') ? $oOrder->sendWCPOrderByEmail($oUser, $oBasket) : null;

            $sNextStep = 'cl=thankyou';
            if($mail === oxOrder::ORDER_STATE_MAILINGERROR) {
                $sNextStep .= '&mailerror=1';
            }
            
            oxUtils::getInstance()->redirect(oxConfig::getInstance()->getShopCurrentUrl() . $sNextStep);
        }
        else 
            if($oOrder->oxorder__oxtransstatus->value == 'PENDING') {
                $sNextStep = 'cl=thankyou&pending=1';
                oxUtils::getInstance()->redirect(oxConfig::getInstance()->getShopCurrentUrl() . $sNextStep);
            }
            else {
                $oSession = $this->getSession();
                $paymentState = is_null($oSession->getVar('wcpPaymentState')) ? 'FAILURE' : $oSession->getVar('wcpPaymentState');

                $oSession->setBasket(unserialize($oSession->getVar('wcpBasket')));
                // force oxid to use a new Order for next try.
                $oSession->deleteVar('sess_challenge');
                // clean up wcpPaymentState
                $oSession->deleteVar('wcpPaymentState');
                // redirect to payment page with correct error.
                self::paymentRedirect($paymentState);
            }
    }

    public static function isValidWCPPayment($sPaymentType) {
        return (bool) array_key_exists($sPaymentType, self::$_VALID_PAYMENT_TYPES);
    }

    /**
     * check if magic quotes gpc or magic quotes runtime are enabled
     * 
     * @return bool
     */
    protected function _magicQuotesUsed() {
        return (bool) (get_magic_quotes_gpc() || get_magic_quotes_runtime());
    }

    protected function _prepareValueForFingerprint($value) {
        return (bool) $this->_magicQuotesUsed() ? stripslashes($value) : $value;
    }

    protected function _getResponseFingerprintSeed($oOrder) {
        $responseFingerprintKeys = explode(',', $_POST['responseFingerprintOrder']);
        $responseFingerprintSeed = '';
        foreach($responseFingerprintKeys as $key) {
            if(strtolower($key) == 'secret') {
                $responseFingerprintSeed .= $this->getConfig()->getConfigParam('WCP_SECRET');
            }
            else 
                if(isset($_POST[$key])) {
                    $responseFingerprintSeed .= $this->_prepareValueForFingerprint($_POST[$key]);
                }
                else {
                    if($this->_isPaid($oOrder)) {
                        $this->_wcpConfirmLogging('Order has allready been paid.');
                        $this->_wcpConfirmLogging('2nd confirmation attempt failed at fingerPrintSeed creation');
                        die($this->_wcpConfirmResponse());
                    }
                    else {
                        $oOrder->cancelOrder();
                        // $blOrderSaved = true;
                        $failureFolder = $this->getConfig()->getConfigParam('WCP_FAILURE_FOLDER');
                        $oOrder = $this->_setOrderFolder($oOrder, $failureFolder);
                        if(!$oOrder->save()) {
                            die($this->_wcpConfirmResponse('Mandatory fields not used. Order update failed.'));
                        }
                        else {
                            die($this->_wcpConfirmResponse('Mandatory fields not used.'));
                        }
                    }
                }
        }
        return $responseFingerprintSeed;
    }

    /**
     * order confirmation.
     * check fingerprint. do some logging.
     * set orderState, cancel order, move to folder.
     */
    protected function _confirmProcess() {
        $oOrder = $this->_getOrder();
        $confirmResponseMessage = null;
        if(isset($_POST['paymentState'])) {
            if($_POST['paymentState'] == 'SUCCESS') {
                $this->_wcpConfirmLogging('Paymentstate: Success - Trying to generate ResponseFingerprintSeed');
                if(strcasecmp(md5($this->_getResponseFingerprintSeed($oOrder)), $_POST['responseFingerprintKey'])) {
                    if(!$this->_isPaid($oOrder)) {
                        $this->_wcpConfirmLogging('Fingerprints match. Setting orderStatus to PAID');
                        
                        // send mail if old status was pending
                        if($oOrder->oxorder__oxtransstatus->value == 'PENDING') {
                            $oBasket = $this->_getBasket();
                            $oUser = $this->getUser();
                            if(method_exists($oOrder, 'sendWCPOrderByEmail')) {
                                $oOrder->sendWCPOrderByEmail($oUser, $oBasket);
                            }
                        }
                        
                        $oOrder->oxorder__oxtransstatus = new oxField('PAID');
                        $oOrder->oxorder__oxpaid = new oxField(date('Y-m-d H:i:s'));
                        $orderNumber = mysql_real_escape_string($_POST['orderNumber']);
                        $gatewayReferenceNumber = mysql_real_escape_string($_POST['gatewayReferenceNumber']);
                        $gatewayContractNumber = mysql_real_escape_string($_POST['gatewayContractNumber']);
                        $oOrder->oxorder__oxtransid = new oxField($orderNumber);
                        $oOrder->oxorder__oxpayid = new oxField($gatewayReferenceNumber);
                        $oOrder->oxorder__oxxid = new oxField($gatewayContractNumber);
                        $successFolder = $this->getConfig()->getConfigParam('WCP_SUCCESS_FOLDER');
                        $oOrder = $this->_setOrderFolder($oOrder, $successFolder);
                        if(!$oOrder->save()) {
                            $confirmResponseMessage = 'Orderstatus update failed.';
                        }
                        $this->_wcpConfirmLogging('Everything seems to be ok.');
                        die($this->_wcpConfirmResponse($confirmResponseMessage));
                    }
                    else {
                        $oxEmail = oxnew('oxemail');
                        $oxEmail->sendWCPDoublePaymentMail($oOrder->oxorder__oxordernr->value, $oOrder->oxorder_oxtransid->rawValue, mysql_real_escape_string($_POST['orderNumber']));
                        die($this->_wcpConfirmResponse());
                    }
                }
                else {
                    $oOrder->cancelOrder();
                    $oOrder->oxorder__oxtransstatus = new oxField('FAILED');
                    $failureFolder = $this->getConfig()->getConfigParam('WCP_FAILURE_FOLDER');
                    $oOrder = $this->_setOrderFolder($oOrder, $failureFolder);
                    
                    $confirmResponseMessage = $oOrder->save() ? 'Fingerprint is Invalid.' : 'Fingerprint is Invalid. Orderstatus update failed.';
                    die($confirmResponseMessage);
                }
            }
            else 
                if($_POST['paymentState'] == 'PENDING') {
                    $this->_wcpConfirmLogging('Paymentstate: Pending - Trying to generate ResponseFingerprintSeed');
                    if(strcasecmp(md5($this->_getResponseFingerprintSeed($oOrder)), $_POST['responseFingerprintKey'])) {
                        if(!$this->_isPaid($oOrder)) {
                            $this->_wcpConfirmLogging('Fingerprints match. Setting orderStatus to PENDING');
                            
                            $oOrder->oxorder__oxtransstatus = new oxField('PENDING');
                            $pendingFolder = $this->getConfig()->getConfigParam('WCP_PENDING_FOLDER');
                            $oOrder = $this->_setOrderFolder($oOrder, $pendingFolder);
                            if(!$oOrder->save()) {
                                $confirmResponseMessage = 'Orderstatus update failed.';
                            }
                            $this->_wcpConfirmLogging('Everything seems to be ok.');
                            die($this->_wcpConfirmResponse($confirmResponseMessage));
                        }
                        else {
                            $oxEmail = oxnew('oxemail');
                            $oxEmail->sendWCPDoublePaymentMail($oOrder->oxorder__oxordernr->value, $oOrder->oxorder_oxtransid->rawValue, mysql_real_escape_string($_POST['orderNumber']));
                            die($this->_wcpConfirmResponse());
                        }
                    }
                    else {
                        $oOrder->cancelOrder();
                        $oOrder->oxorder__oxtransstatus = new oxField('FAILED');
                        $failureFolder = $this->getConfig()->getConfigParam('WCP_FAILURE_FOLDER');
                        $oOrder = $this->_setOrderFolder($oOrder, $failureFolder);
                        
                        $confirmResponseMessage = $oOrder->save() ? 'Fingerprint is Invalid.' : 'Fingerprint is Invalid. Orderstatus update failed.';
                        die($confirmResponseMessage);
                    }
                }
                else 
                    if(!$this->_isPaid($oOrder)) {
                        if($_POST['paymentState'] == 'CANCEL') {
                            $oOrder->cancelOrder();
                            $oOrder->oxorder__oxtransstatus = new oxField('CANCELED');
                            $cancelFolder = $this->getConfig()->getConfigParam('WCP_CANCEL_FOLDER');
                            $oOrder = $this->_setOrderFolder($oOrder, $cancelFolder);
                            
                            if(!$oOrder->save()) {
                                $confirmResponseMessage = 'Orderstatus update failed.';
                            }
                            
                            die($this->_wcpConfirmResponse($confirmResponseMessage));
                        }
                        else 
                            if($_POST['paymentState'] == 'FAILURE') {
                                $oOrder->cancelOrder();
                                $oOrder->oxorder__oxtransstatus = new oxField('FAILED');
                                $failureFolder = $this->getConfig()->getConfigParam('WCP_FAILURE_FOLDER');
                                $oOrder = $this->_setOrderFolder($oOrder, $failureFolder);
                                
                                if(!$oOrder->save()) {
                                    $confirmResponseMessage = 'Orderstatus update failed.';
                                }
                                
                                die($this->_wcpConfirmResponse($confirmResponseMessage));
                            }
                            else {
                                // paymentState unknown - not paid yet. handle as failure.
                                $this->_wcpConfirmLogging('Invalid paymentState. Set failurestate for order');
                                $oOrder->cancelOrder();
                                $oOrder->oxorder__oxtransstatus = new oxField('FAILED');
                                $failureFolder = $this->getConfig()->getConfigParam('WCP_FAILURE_FOLDER');
                                $oOrder = $this->_setOrderFolder($oOrder, $failureFolder);
                                
                                if(!$oOrder->save()) {
                                    $confirmResponseMessage = 'Orderstatus update failed.';
                                }
                                
                                die($this->_wcpConfirmResponse($confirmResponseMessage));
                            }
                    }
                    else {
                        // order has already been saved. no matter what comes
                        // next we have a valid payment
                        $this->_wcpConfirmLogging('order has already been paid.');
                        die($this->_wcpConfirmResponse());
                    }
        }
        else {
            die($this->_wcpConfirmResponse('Invalid call of confirm action. no paymentstate given'));
        }
    }

    protected function _wcpConfirmResponse($message = null) {
        if(!is_null($message)) {
            $this->_wcpConfirmLogging($message);
            $value = 'result="NOK" message="' . $message . '" ';
        }
        else {
            $this->_wcpConfirmLogging('Success confirmation will be delivered');
            $value = 'result="OK"';
        }
        return '<WCP-CONFIRMATION-RESPONSE ' . $value . ' />';
    }

    protected function _setOrderFolder($oOrder, $folder) {
        if($folder) {
            $oOrder->oxorder__oxfolder = new oxField($folder, oxField::T_RAW);
            $this->_wcpConfirmLogging('moved order to ' . $folder . ' folder');
        }
        return $oOrder;
    }

    /**
     * check if order is payed
     *
     * @var $oOrder
     * @return boolean
     */
    protected function _isPaid($oOrder) {
        if($oOrder->oxorder__oxtransstatus->value == 'PAID') {
            $this->_wcpConfirmLogging('Order has already been paid.');
            return true;
        }
        
        return false;
    }

    /**
     * Redirect to index.php?cl=payment
     * 
     * @param $state (optional)            
     */
    public static function paymentRedirect($state = null) {
        if(!is_null($state)) {
            $errorCode = ($state == 'CANCEL') ? self::$_PAYMENT_CANCELED_ERRORCODE : self::$_PAYMENT_FAILED_ERRORCODE;
            oxUtils::getInstance()->redirect(oxConfig::getInstance()->getShopCurrentUrl() . 'cl=payment&payerror=' . $errorCode);
        }
        else {
            oxUtils::getInstance()->redirect(oxConfig::getInstance()->getShopCurrentUrl() . 'cl=payment');
        }
    }
}
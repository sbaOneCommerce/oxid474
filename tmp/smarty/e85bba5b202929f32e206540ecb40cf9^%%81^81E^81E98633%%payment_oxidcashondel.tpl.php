<?php /* Smarty version 2.6.26, created on 2014-01-09 11:16:17
         compiled from page/checkout/inc/payment_oxidcashondel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxmultilang', 'page/checkout/inc/payment_oxidcashondel.tpl', 9, false),)), $this); ?>
<dl>
    <dt>
        <input id="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
" type="radio" name="paymentid" value="<?php echo $this->_tpl_vars['sPaymentID']; ?>
" <?php if ($this->_tpl_vars['oView']->getCheckedPaymentId() == $this->_tpl_vars['paymentmethod']->oxpayments__oxid->value): ?>checked<?php endif; ?>>
        <label for="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
"><b><?php echo $this->_tpl_vars['paymentmethod']->oxpayments__oxdesc->value; ?>
</b></label>
    </dt>
    <dd class="<?php if ($this->_tpl_vars['oView']->getCheckedPaymentId() == $this->_tpl_vars['paymentmethod']->oxpayments__oxid->value): ?>activePayment<?php endif; ?>">
        <?php if ($this->_tpl_vars['paymentmethod']->getPrice()): ?>
            <?php if ($this->_tpl_vars['oxcmp_basket']->getPayCostNet()): ?>
                <?php echo $this->_tpl_vars['paymentmethod']->getFNettoPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
 <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_BASKETCONTENTS_PLUSTAX1'), $this);?>
 <?php echo $this->_tpl_vars['paymentmethod']->getFPriceVat(); ?>

            <?php else: ?>
                <?php echo $this->_tpl_vars['paymentmethod']->getFBruttoPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

            <?php endif; ?>
        <?php endif; ?>
        <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_PLUSCODCHARGE2'), $this);?>


        
            <?php if ($this->_tpl_vars['paymentmethod']->oxpayments__oxlongdesc->value): ?>
                <div class="desc">
                    <?php echo $this->_tpl_vars['paymentmethod']->oxpayments__oxlongdesc->getRawValue(); ?>

                </div>
            <?php endif; ?>
        
    </dd>
</dl>
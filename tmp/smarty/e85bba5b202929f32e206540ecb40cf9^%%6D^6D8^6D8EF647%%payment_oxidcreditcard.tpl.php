<?php /* Smarty version 2.6.26, created on 2014-01-09 11:16:17
         compiled from page/checkout/inc/payment_oxidcreditcard.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxmultilang', 'page/checkout/inc/payment_oxidcreditcard.tpl', 10, false),)), $this); ?>
<?php $this->assign('dynvalue', $this->_tpl_vars['oView']->getDynValue()); ?>
<dl>
    <dt>
        <input id="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
" type="radio" name="paymentid" value="<?php echo $this->_tpl_vars['sPaymentID']; ?>
" <?php if ($this->_tpl_vars['oView']->getCheckedPaymentId() == $this->_tpl_vars['paymentmethod']->oxpayments__oxid->value): ?>checked<?php endif; ?>>
        <label for="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
"><b><?php echo $this->_tpl_vars['paymentmethod']->oxpayments__oxdesc->value; ?>
</b></label>
    </dt>
    <dd class="<?php if ($this->_tpl_vars['oView']->getCheckedPaymentId() == $this->_tpl_vars['paymentmethod']->oxpayments__oxid->value): ?>activePayment<?php endif; ?>">
        <ul class="form">
            <li>
                <label><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_CREDITCARD'), $this);?>
</label>
                <select name="dynvalue[kktype]">
                    <option value="mcd" <?php if (( $this->_tpl_vars['dynvalue']['kktype'] == 'mcd' || ! $this->_tpl_vars['dynvalue']['kktype'] )): ?>selected<?php endif; ?>><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_MASTERCARD'), $this);?>
</option>
                    <option value="vis" <?php if ($this->_tpl_vars['dynvalue']['kktype'] == 'vis'): ?>selected<?php endif; ?>><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_VISA'), $this);?>
</option>
                    <!--
                    <option value="amx" <?php if ($this->_tpl_vars['dynvalue']['kktype'] == 'amx'): ?>selected<?php endif; ?>>American Express</option>
                    <option value="dsc" <?php if ($this->_tpl_vars['dynvalue']['kktype'] == 'dsc'): ?>selected<?php endif; ?>>Discover</option>
                    <option value="dnc" <?php if ($this->_tpl_vars['dynvalue']['kktype'] == 'dnc'): ?>selected<?php endif; ?>>Diners Club</option>
                    <option value="jcb" <?php if ($this->_tpl_vars['dynvalue']['kktype'] == 'jcb'): ?>selected<?php endif; ?>>JCB</option>
                    <option value="swi" <?php if ($this->_tpl_vars['dynvalue']['kktype'] == 'swi'): ?>selected<?php endif; ?>>Switch</option>
                    <option value="dlt" <?php if ($this->_tpl_vars['dynvalue']['kktype'] == 'dlt'): ?>selected<?php endif; ?>>Delta</option>
                    <option value="enr" <?php if ($this->_tpl_vars['dynvalue']['kktype'] == 'enr'): ?>selected<?php endif; ?>>EnRoute</option>
                    -->
                </select>
            </li>
            <li>
                <label><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_NUMBER'), $this);?>
</label>
                <input type="text" class="js-oxValidate js-oxValidate_notEmpty" size="20" maxlength="64" name="dynvalue[kknumber]" value="<?php echo $this->_tpl_vars['dynvalue']['kknumber']; ?>
">
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty"><?php echo smarty_function_oxmultilang(array('ident' => 'EXCEPTION_INPUT_NOTALLFIELDS'), $this);?>
</span>
                </p>
            </li>
            <li>
                <label><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_ACCOUNTHOLDER'), $this);?>
</label>
                <input type="text" size="20" class="js-oxValidate js-oxValidate_notEmpty" maxlength="64" name="dynvalue[kkname]" value="<?php if ($this->_tpl_vars['dynvalue']['kkname']): ?><?php echo $this->_tpl_vars['dynvalue']['kkname']; ?>
<?php else: ?><?php echo $this->_tpl_vars['oxcmp_user']->oxuser__oxfname->value; ?>
 <?php echo $this->_tpl_vars['oxcmp_user']->oxuser__oxlname->value; ?>
<?php endif; ?>">
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty"><?php echo smarty_function_oxmultilang(array('ident' => 'EXCEPTION_INPUT_NOTALLFIELDS'), $this);?>
</span>
                </p>
                <br>
                <div class="note"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_DIFFERENTBILLINGADDRESS'), $this);?>
</div>
            </li>
            <li>
                <label><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_VALIDUNTIL'), $this);?>
</label>
                <select name="dynvalue[kkmonth]">
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '01'): ?>selected<?php endif; ?>>01</option>
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '02'): ?>selected<?php endif; ?>>02</option>
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '03'): ?>selected<?php endif; ?>>03</option>
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '04'): ?>selected<?php endif; ?>>04</option>
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '05'): ?>selected<?php endif; ?>>05</option>
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '06'): ?>selected<?php endif; ?>>06</option>
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '07'): ?>selected<?php endif; ?>>07</option>
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '08'): ?>selected<?php endif; ?>>08</option>
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '09'): ?>selected<?php endif; ?>>09</option>
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '10'): ?>selected<?php endif; ?>>10</option>
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '11'): ?>selected<?php endif; ?>>11</option>
                  <option <?php if ($this->_tpl_vars['dynvalue']['kkmonth'] == '12'): ?>selected<?php endif; ?>>12</option>
                </select>

                &nbsp;/&nbsp;

                <select name="dynvalue[kkyear]">
                <?php $_from = $this->_tpl_vars['oView']->getCreditYears(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['year']):
?>
                    <option <?php if ($this->_tpl_vars['dynvalue']['kkyear'] == $this->_tpl_vars['year']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['year']; ?>
</option>
                <?php endforeach; endif; unset($_from); ?>
                </select>
            </li>
            <li>
                <label><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_SECURITYCODE'), $this);?>
</label>
                <input type="text" class="js-oxValidate js-oxValidate_notEmpty" size="20" maxlength="64" name="dynvalue[kkpruef]" value="<?php echo $this->_tpl_vars['dynvalue']['kkpruef']; ?>
">
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty"><?php echo smarty_function_oxmultilang(array('ident' => 'EXCEPTION_INPUT_NOTALLFIELDS'), $this);?>
</span>
                </p>
                <br>
                <div class="note"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_SECURITYCODEDESCRIPTION'), $this);?>
</div>
            </li>
        </ul>

        
            <?php if ($this->_tpl_vars['paymentmethod']->oxpayments__oxlongdesc->value): ?>
                <div class="desc">
                    <?php echo $this->_tpl_vars['paymentmethod']->oxpayments__oxlongdesc->getRawValue(); ?>

                </div>
            <?php endif; ?>
        
    </dd>
</dl>
<?php /* Smarty version 2.6.26, created on 2014-01-09 11:16:04
         compiled from page/checkout/inc/options.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxscript', 'page/checkout/inc/options.tpl', 2, false),array('function', 'oxmultilang', 'page/checkout/inc/options.tpl', 8, false),)), $this); ?>

    <?php echo smarty_function_oxscript(array('include' => "js/widgets/oxequalizer.js",'priority' => 10), $this);?>

    <?php echo smarty_function_oxscript(array('add' => "$(function(){oxEqualizer.equalHeight($( '.checkoutOptions .option' ));});"), $this);?>

    <div class="checkoutOptions clear">
        
            <?php if ($this->_tpl_vars['oView']->getShowNoRegOption()): ?>
            <div class="lineBox option" id="optionNoRegistration">
                <h3><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_USER_OPTION_NOREGISTRATION'), $this);?>
</h3>
                
                    <p><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_USER_OPTION_NOREGISTRATION_DESCRIPTION'), $this);?>
</p>
                    <?php if ($this->_tpl_vars['oView']->isDownloadableProductWarning()): ?>
                        <p class="errorMsg"><?php echo smarty_function_oxmultilang(array('ident' => 'MESSAGE_DOWNLOADABLE_PRODUCT'), $this);?>
</p>
                    <?php endif; ?>
                
                <form action="<?php echo $this->_tpl_vars['oViewConf']->getSslSelfLink(); ?>
" method="post">
                    <p>
                        <?php echo $this->_tpl_vars['oViewConf']->getHiddenSid(); ?>

                        <?php echo $this->_tpl_vars['oViewConf']->getNavFormParams(); ?>

                        <input type="hidden" name="cl" value="user">
                        <input type="hidden" name="fnc" value="">
                        <input type="hidden" name="option" value="1">
                        <button class="submitButton nextStep" type="submit"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_USER_OPTION_NEXT'), $this);?>
</button>
                    </p>
                </form>
            </div>
            <?php endif; ?>
        

        
            <div class="lineBox option" id="optionRegistration">
                <h3><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_USER_OPTION_REGISTRATION'), $this);?>
</h3>
                
                    <p><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_USER_OPTION_REGISTRATION_DESCRIPTION'), $this);?>
</p>
                
                <form action="<?php echo $this->_tpl_vars['oViewConf']->getSslSelfLink(); ?>
" method="post">
                    <p>
                        <?php echo $this->_tpl_vars['oViewConf']->getHiddenSid(); ?>

                        <?php echo $this->_tpl_vars['oViewConf']->getNavFormParams(); ?>

                        <input type="hidden" name="cl" value="user">
                        <input type="hidden" name="fnc" value="">
                        <input type="hidden" name="option" value="3">
                        <button class="submitButton nextStep" type="submit"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_USER_OPTION_NEXT'), $this);?>
</button>
                    </p>
                </form>
            </div>
        

        
            <div class="lineBox option" id="optionLogin">
                <h3><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_USER_OPTION_LOGIN'), $this);?>
</h3>
                
                    <p><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_USER_OPTION_LOGIN_DESCRIPTION'), $this);?>
</p>
                
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "form/login.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
        
    </div>
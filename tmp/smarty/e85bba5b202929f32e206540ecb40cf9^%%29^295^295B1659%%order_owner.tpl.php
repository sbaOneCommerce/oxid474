<?php /* Smarty version 2.6.26, created on 2014-01-09 11:16:27
         compiled from /home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/plain/order_owner.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxcontent', '/home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/plain/order_owner.tpl', 11, false),array('function', 'oxmultilang', '/home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/plain/order_owner.tpl', 17, false),array('modifier', 'strip_tags', '/home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/plain/order_owner.tpl', 32, false),array('modifier', 'replace', '/home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/plain/order_owner.tpl', 82, false),array('modifier', 'oxmultilangsal', '/home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/plain/order_owner.tpl', 206, false),)), $this); ?>
<?php $this->assign('shop', $this->_tpl_vars['oEmailView']->getShop()); ?>
<?php $this->assign('oViewConf', $this->_tpl_vars['oEmailView']->getViewConfig()); ?>
<?php $this->assign('currency', $this->_tpl_vars['oEmailView']->getCurrency()); ?>
<?php $this->assign('user', $this->_tpl_vars['oEmailView']->getUser()); ?>
<?php $this->assign('basket', $this->_tpl_vars['order']->getBasket()); ?>
<?php $this->assign('oDelSet', $this->_tpl_vars['order']->getDelSet()); ?>
<?php $this->assign('payment', $this->_tpl_vars['order']->getPayment()); ?>


<?php if ($this->_tpl_vars['payment']->oxuserpayments__oxpaymentsid->value == 'oxempty'): ?>
<?php echo smarty_function_oxcontent(array('ident' => 'oxadminordernpplainemail'), $this);?>

<?php else: ?>
<?php echo smarty_function_oxcontent(array('ident' => 'oxadminorderplainemail'), $this);?>

<?php endif; ?>


<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_ORDERNOMBER'), $this);?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>



<?php if ($this->_tpl_vars['oViewConf']->getShowVouchers()): ?>
<?php $_from = $this->_tpl_vars['order']->getVoucherList(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['voucher']):
?>
<?php $this->assign('voucherseries', $this->_tpl_vars['voucher']->getSerie()); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_USEDCOUPONS'), $this);?>
 <?php echo $this->_tpl_vars['voucher']->oxvouchers__oxvouchernr->value; ?>
 - <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_DICOUNT'), $this);?>
 <?php echo $this->_tpl_vars['voucherseries']->oxvoucherseries__oxdiscount->value; ?>
 <?php if ($this->_tpl_vars['voucherseries']->oxvoucherseries__oxdiscounttype->value == 'absolute'): ?><?php echo $this->_tpl_vars['currency']->sign; ?>
<?php else: ?>%<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>


<?php $this->assign('basketitemlist', $this->_tpl_vars['basket']->getBasketArticles()); ?>
<?php $_from = $this->_tpl_vars['basket']->getContents(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['basketindex'] => $this->_tpl_vars['basketitem']):
?>

<?php $this->assign('basketproduct', $this->_tpl_vars['basketitemlist'][$this->_tpl_vars['basketindex']]); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['basketproduct']->oxarticles__oxtitle->getRawValue())) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
<?php if ($this->_tpl_vars['basketproduct']->oxarticles__oxvarselect->value): ?>, <?php echo $this->_tpl_vars['basketproduct']->oxarticles__oxvarselect->value; ?>
<?php endif; ?>
<?php if ($this->_tpl_vars['basketitem']->getChosenSelList()): ?><?php $_from = $this->_tpl_vars['basketitem']->getChosenSelList(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['oList']):
?><?php echo $this->_tpl_vars['oList']->name; ?>
 <?php echo $this->_tpl_vars['oList']->value; ?>
<?php endforeach; endif; unset($_from); ?><?php endif; ?>
<?php if ($this->_tpl_vars['basketitem']->getPersParams()): ?><?php $_from = $this->_tpl_vars['basketitem']->getPersParams(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sVar'] => $this->_tpl_vars['aParam']):
?><?php echo $this->_tpl_vars['sVar']; ?>
 : <?php echo $this->_tpl_vars['aParam']; ?>
<?php endforeach; endif; unset($_from); ?><?php endif; ?>
<?php if ($this->_tpl_vars['oViewConf']->getShowGiftWrapping()): ?>
<?php $this->assign('oWrapping', $this->_tpl_vars['basketitem']->getWrapping()); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_WRAPPING'), $this);?>
 <?php if (! $this->_tpl_vars['basketitem']->getWrappingId()): ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_NONE'), $this);?>
<?php else: ?><?php echo $this->_tpl_vars['oWrapping']->oxwrapping__oxname->getRawValue(); ?>
<?php endif; ?>
<?php endif; ?>
<?php if ($this->_tpl_vars['basketproduct']->oxarticles__oxorderinfo->value): ?><?php echo $this->_tpl_vars['basketproduct']->oxarticles__oxorderinfo->getRawValue(); ?>
<?php endif; ?>

<?php $this->assign('dRegUnitPrice', $this->_tpl_vars['basketitem']->getRegularUnitPrice()); ?>
<?php $this->assign('dUnitPrice', $this->_tpl_vars['basketitem']->getUnitPrice()); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_UNITPRICE'), $this);?>
 <?php echo $this->_tpl_vars['basketitem']->getFUnitPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>
 <?php if (! $this->_tpl_vars['basketitem']->isBundle()): ?> <?php if ($this->_tpl_vars['dRegUnitPrice']->getPrice() > $this->_tpl_vars['dUnitPrice']->getPrice()): ?> (<?php echo $this->_tpl_vars['basketitem']->getFRegularUnitPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
) <?php endif; ?><?php endif; ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_QUANTITY'), $this);?>
 <?php echo $this->_tpl_vars['basketitem']->getAmount(); ?>

<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_VAT'), $this);?>
 <?php echo $this->_tpl_vars['basketitem']->getVatPercent(); ?>
%
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTAL'), $this);?>
 <?php echo $this->_tpl_vars['basketitem']->getFTotalPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>


<?php endforeach; endif; unset($_from); ?>
------------------------------------------------------------------
<?php if (! $this->_tpl_vars['basket']->getDiscounts()): ?>

<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALNET'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getProductsNetPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>



<?php $_from = $this->_tpl_vars['basket']->getProductVats(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['VATitem']):
?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PLUSTAX1'), $this);?>
 <?php echo $this->_tpl_vars['key']; ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PLUSTAX2'), $this);?>
 <?php echo $this->_tpl_vars['VATitem']; ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>

<?php endforeach; endif; unset($_from); ?>


<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALGROSS'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getFProductsPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>


<?php endif; ?>

<?php if ($this->_tpl_vars['basket']->getDiscounts()): ?>
<?php if ($this->_tpl_vars['order']->isNettoMode()): ?>

<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALNET'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getProductsNetPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>


<?php else: ?>

<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALGROSS'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getFProductsPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>


<?php endif; ?>

  <?php $_from = $this->_tpl_vars['basket']->getDiscounts(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['oDiscount']):
?>
  <?php if ($this->_tpl_vars['oDiscount']->dDiscount < 0): ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_CHARGE'), $this);?>
<?php else: ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_DICOUNT'), $this);?>
<?php endif; ?> <?php echo $this->_tpl_vars['oDiscount']->sDiscount; ?>
: <?php if ($this->_tpl_vars['oDiscount']->dDiscount < 0): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['oDiscount']->fDiscount)) ? $this->_run_mod_handler('replace', true, $_tmp, "-", "") : smarty_modifier_replace($_tmp, "-", "")); ?>
<?php else: ?>-<?php echo $this->_tpl_vars['oDiscount']->fDiscount; ?>
<?php endif; ?> <?php echo $this->_tpl_vars['currency']->name; ?>

  <?php endforeach; endif; unset($_from); ?>

<?php if (! $this->_tpl_vars['order']->isNettoMode()): ?>

    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALNET'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getProductsNetPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>


<?php endif; ?>

    <?php $_from = $this->_tpl_vars['basket']->getProductVats(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['VATitem']):
?>
    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PLUSTAX1'), $this);?>
 <?php echo $this->_tpl_vars['key']; ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PLUSTAX2'), $this);?>
 <?php echo $this->_tpl_vars['VATitem']; ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>

  <?php endforeach; endif; unset($_from); ?>

<?php endif; ?>
<?php if ($this->_tpl_vars['order']->isNettoMode()): ?>

<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALGROSS'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getFProductsPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>


<?php endif; ?>

<?php if ($this->_tpl_vars['oViewConf']->getShowVouchers() && $this->_tpl_vars['basket']->getVoucherDiscValue()): ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_COUPON'), $this);?>
 <?php if ($this->_tpl_vars['basket']->getFVoucherDiscountValue() > 0): ?>-<?php endif; ?><?php echo ((is_array($_tmp=$this->_tpl_vars['basket']->getFVoucherDiscountValue())) ? $this->_run_mod_handler('replace', true, $_tmp, "-", "") : smarty_modifier_replace($_tmp, "-", "")); ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>

<?php endif; ?>


<?php if ($this->_tpl_vars['basket']->getDelCostNet()): ?>
    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_SHIPPINGNET'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getDelCostNet(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

    <?php if ($this->_tpl_vars['basket']->getDelCostVat()): ?> <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_PLUS_PROPORTIONAL_VAT'), $this);?>
 <?php else: ?> <?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT1'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getDelCostVatPercent(); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT2'), $this);?>
<?php endif; ?> <?php echo $this->_tpl_vars['basket']->getDelCostVat(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

<?php elseif ($this->_tpl_vars['basket']->getFDeliveryCosts()): ?>
    <?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_COST'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getFDeliveryCosts(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

<?php endif; ?>


<?php if ($this->_tpl_vars['basket']->getPayCostNet()): ?>
    <?php if ($this->_tpl_vars['basket']->getPaymentCosts() >= 0): ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PAYMENTCHARGEDISCOUNT1'), $this);?>
<?php else: ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PAYMENTCHARGEDISCOUNT2'), $this);?>
<?php endif; ?> <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PAYMENTCHARGEDISCOUNT3'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getPayCostNet(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

    <?php if ($this->_tpl_vars['basket']->getPayCostVat()): ?>
        <?php if ($this->_tpl_vars['basket']->isProportionalCalculationOn()): ?> <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_PLUS_PROPORTIONAL_VAT'), $this);?>
<?php else: ?> <?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT1'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getPayCostVatPercent(); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT2'), $this);?>
<?php endif; ?> <?php echo $this->_tpl_vars['basket']->getPayCostVat(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

    <?php endif; ?>
<?php elseif ($this->_tpl_vars['basket']->getFPaymentCosts()): ?>
    <?php echo smarty_function_oxmultilang(array('ident' => 'SURCHARGE'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getFPaymentCosts(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

<?php endif; ?>



<?php if ($this->_tpl_vars['basket']->getTsProtectionCosts()): ?>
    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TSPROTECTIONCHARGETAX1'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getTsProtectionVatPercent(); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TSPROTECTIONCHARGETAX2'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getTsProtectionVat(); ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>

    <?php if ($this->_tpl_vars['basket']->getTsProtectionVat()): ?>
        <?php if ($this->_tpl_vars['basket']->isProportionalCalculationOn()): ?><?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_PLUS_PROPORTIONAL_VAT'), $this);?>
<?php else: ?> <?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT1'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getTsProtectionVatPercent(); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT2'), $this);?>
<?php endif; ?>
    <?php endif; ?>
<?php endif; ?>



<?php if ($this->_tpl_vars['oViewConf']->getShowGiftWrapping()): ?>
    <?php if ($this->_tpl_vars['basket']->getWrappCostNet()): ?>
        <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_WRAPPING_COSTS_NET'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getWrappCostNet(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

        <?php if ($this->_tpl_vars['basket']->getWrappCostVat()): ?>
            <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_PLUS_VAT'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getWrappCostVat(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

        <?php endif; ?>
    <?php elseif ($this->_tpl_vars['basket']->getFWrappingCosts()): ?>
        <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_WRAPPING_COSTS'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getFWrappingCosts(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

    <?php endif; ?>
<?php endif; ?>



<?php if ($this->_tpl_vars['oViewConf']->getShowGiftWrapping()): ?>
    <?php if ($this->_tpl_vars['basket']->getGiftCardCostNet()): ?>
        <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_GIFTCARD_COSTS_NET'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getGiftCardCostNet(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

        <?php if ($this->_tpl_vars['basket']->getGiftCardCostVat()): ?>
            <?php if ($this->_tpl_vars['basket']->isProportionalCalculationOn()): ?><?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_PLUS_PROPORTIONAL_VAT'), $this);?>
<?php else: ?> <?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT1'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getGiftCardCostVatPercent(); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT2'), $this);?>
<?php endif; ?> <?php echo $this->_tpl_vars['basket']->getGiftCardCostVat(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

        <?php endif; ?>
    <?php elseif ($this->_tpl_vars['basket']->getFGiftCardCosts()): ?>
        <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_GIFTCARD_COSTS'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getFGiftCardCosts(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

    <?php endif; ?>
<?php endif; ?>



<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_GRANDTOTAL'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getFPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->name; ?>

<?php if ($this->_tpl_vars['basket']->oCard): ?>
    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_OWNER_HTML_ATENTIONGREETINGCARD'), $this);?>

    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_YOURMESSAGE'), $this);?>

    <?php echo $this->_tpl_vars['basket']->getCardMessage(); ?>

<?php endif; ?>



<?php if ($this->_tpl_vars['order']->oxorder__oxremark->value): ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_OWNER_HTML_MESSAGE'), $this);?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxremark->getRawValue(); ?>

<?php endif; ?>



<?php if ($this->_tpl_vars['payment']->oxuserpayments__oxpaymentsid->value != 'oxempty'): ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_OWNER_HTML_PAYMENTINFO'), $this);?>

<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PAYMENTMETHOD'), $this);?>
<?php echo $this->_tpl_vars['payment']->oxpayments__oxdesc->getRawValue(); ?>
 <?php if ($this->_tpl_vars['basket']->getPaymentCosts()): ?>(<?php echo $this->_tpl_vars['basket']->getFPaymentCosts(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
)<?php endif; ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_OWNER_HTML_PAYMENTINFOOFF'), $this);?>

<?php endif; ?>



<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_EMAILADDRESS'), $this);?>
 <?php echo $this->_tpl_vars['user']->oxuser__oxusername->value; ?>




<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_BILLINGADDRESS'), $this);?>

<?php echo $this->_tpl_vars['order']->oxorder__oxbillcompany->getRawValue(); ?>

<?php echo ((is_array($_tmp=$this->_tpl_vars['order']->oxorder__oxbillsal->value)) ? $this->_run_mod_handler('oxmultilangsal', true, $_tmp) : smarty_modifier_oxmultilangsal($_tmp)); ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbillfname->getRawValue(); ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbilllname->getRawValue(); ?>

<?php if ($this->_tpl_vars['order']->oxorder__oxbilladdinfo->value): ?><?php echo $this->_tpl_vars['order']->oxorder__oxbilladdinfo->getRawValue(); ?>
<?php endif; ?>
<?php echo $this->_tpl_vars['order']->oxorder__oxbillstreet->getRawValue(); ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbillstreetnr->value; ?>

<?php echo $this->_tpl_vars['order']->oxorder__oxbillstateid->value; ?>

<?php echo $this->_tpl_vars['order']->oxorder__oxbillzip->value; ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbillcity->getRawValue(); ?>

<?php echo $this->_tpl_vars['order']->oxorder__oxbillcountry->getRawValue(); ?>

<?php if ($this->_tpl_vars['order']->oxorder__oxbillustid->value): ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_VATIDNOMBER'), $this);?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbillustid->value; ?>
<?php endif; ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PHONE'), $this);?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbillfon->value; ?>


<?php if ($this->_tpl_vars['order']->oxorder__oxdellname->value): ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_SHIPPINGADDRESS'), $this);?>

<?php echo $this->_tpl_vars['order']->oxorder__oxdelcompany->getRawValue(); ?>

<?php echo ((is_array($_tmp=$this->_tpl_vars['order']->oxorder__oxdelsal->value)) ? $this->_run_mod_handler('oxmultilangsal', true, $_tmp) : smarty_modifier_oxmultilangsal($_tmp)); ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxdelfname->getRawValue(); ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxdellname->getRawValue(); ?>

<?php if ($this->_tpl_vars['order']->oxorder__oxdeladdinfo->value): ?><?php echo $this->_tpl_vars['order']->oxorder__oxdeladdinfo->getRawValue(); ?>
<?php endif; ?>
<?php echo $this->_tpl_vars['order']->oxorder__oxdelstreet->getRawValue(); ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxdelstreetnr->value; ?>

<?php echo $this->_tpl_vars['order']->oxorder__oxdelstateid->getRawValue(); ?>

<?php echo $this->_tpl_vars['order']->oxorder__oxdelzip->value; ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxdelcity->getRawValue(); ?>

<?php echo $this->_tpl_vars['order']->oxorder__oxdelcountry->getRawValue(); ?>

<?php endif; ?>



<?php if ($this->_tpl_vars['payment']->oxuserpayments__oxpaymentsid->value != 'oxempty'): ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_SHIPPINGCARRIER'), $this);?>
 <?php echo $this->_tpl_vars['order']->oDelSet->oxdeliveryset__oxtitle->getRawValue(); ?>

<?php endif; ?>


<?php echo smarty_function_oxcontent(array('ident' => 'oxemailfooterplain'), $this);?>

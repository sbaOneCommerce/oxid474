<?php /* Smarty version 2.6.26, created on 2014-01-09 11:16:17
         compiled from page/checkout/inc/payment_other.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxmultilang', 'page/checkout/inc/payment_other.tpl', 7, false),)), $this); ?>
<dl>
    <dt>
        <input id="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
" type="radio" name="paymentid" value="<?php echo $this->_tpl_vars['sPaymentID']; ?>
" <?php if ($this->_tpl_vars['oView']->getCheckedPaymentId() == $this->_tpl_vars['paymentmethod']->oxpayments__oxid->value): ?>checked<?php endif; ?>>
        <label for="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
"><b><?php echo $this->_tpl_vars['paymentmethod']->oxpayments__oxdesc->value; ?>

        <?php if ($this->_tpl_vars['paymentmethod']->getPrice()): ?>
            <?php if ($this->_tpl_vars['oxcmp_basket']->getPayCostNet()): ?>
                (<?php echo $this->_tpl_vars['paymentmethod']->getFNettoPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
 <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_BASKETCONTENTS_PLUSTAX1'), $this);?>
 <?php echo $this->_tpl_vars['paymentmethod']->getFPriceVat(); ?>
 )
            <?php else: ?>
                (<?php echo $this->_tpl_vars['paymentmethod']->getFBruttoPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
)
            <?php endif; ?>
        <?php endif; ?>

        </b></label>
    </dt>
    <dd class="<?php if ($this->_tpl_vars['oView']->getCheckedPaymentId() == $this->_tpl_vars['paymentmethod']->oxpayments__oxid->value): ?>activePayment<?php endif; ?>">
        <ul>
        <?php $_from = $this->_tpl_vars['paymentmethod']->getDynValues(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['PaymentDynValues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['PaymentDynValues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['value']):
        $this->_foreach['PaymentDynValues']['iteration']++;
?>
            <li>
                <label><?php echo $this->_tpl_vars['value']->name; ?>
</label>
                <input id="<?php echo $this->_tpl_vars['sPaymentID']; ?>
_<?php echo $this->_foreach['PaymentDynValues']['iteration']; ?>
" type="text" class="textbox" size="20" maxlength="64" name="dynvalue[<?php echo $this->_tpl_vars['value']->name; ?>
]" value="<?php echo $this->_tpl_vars['value']->value; ?>
">
            </li>
        <?php endforeach; endif; unset($_from); ?>
        </ul>

        
            <?php if ($this->_tpl_vars['paymentmethod']->oxpayments__oxlongdesc->value): ?>
                <div class="desc">
                    <?php echo $this->_tpl_vars['paymentmethod']->oxpayments__oxlongdesc->getRawValue(); ?>

                </div>
            <?php endif; ?>
        
    </dd>
</dl>
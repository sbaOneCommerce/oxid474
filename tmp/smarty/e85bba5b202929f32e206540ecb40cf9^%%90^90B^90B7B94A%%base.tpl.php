<?php /* Smarty version 2.6.26, created on 2014-01-13 15:55:13
         compiled from layout/base.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'layout/base.tpl', 11, false),array('function', 'oxstyle', 'layout/base.tpl', 53, false),array('function', 'math', 'layout/base.tpl', 197, false),array('function', 'oxscript', 'layout/base.tpl', 289, false),array('function', 'oxid_include_dynamic', 'layout/base.tpl', 304, false),)), $this); ?>
<?php ob_start(); ?>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->_tpl_vars['oView']->getCharSet(); ?>
">

    <?php $this->assign('_sMetaTitlePrefix', $this->_tpl_vars['oView']->getTitlePrefix()); ?>
    <?php $this->assign('_sMetaTitleSuffix', $this->_tpl_vars['oView']->getTitleSuffix()); ?>
    <?php $this->assign('_sMetaTitlePageSuffix', $this->_tpl_vars['oView']->getTitlePageSuffix()); ?>
    <?php $this->assign('_sMetaTitle', $this->_tpl_vars['oView']->getTitle()); ?>

    <title><?php echo $this->_tpl_vars['_sMetaTitlePrefix']; ?>
<?php if ($this->_tpl_vars['_sMetaTitlePrefix'] && $this->_tpl_vars['_sMetaTitle']): ?> | <?php endif; ?><?php echo ((is_array($_tmp=$this->_tpl_vars['_sMetaTitle'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
<?php if ($this->_tpl_vars['_sMetaTitleSuffix'] && ( $this->_tpl_vars['_sMetaTitlePrefix'] || $this->_tpl_vars['_sMetaTitle'] )): ?> | <?php endif; ?><?php echo $this->_tpl_vars['_sMetaTitleSuffix']; ?>
 <?php if ($this->_tpl_vars['_sMetaTitlePageSuffix']): ?> | <?php echo $this->_tpl_vars['_sMetaTitlePageSuffix']; ?>
 <?php endif; ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

    <?php if ($this->_tpl_vars['oView']->noIndex() == 1): ?>
        <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
    <?php elseif ($this->_tpl_vars['oView']->noIndex() == 2): ?>
        <meta name="ROBOTS" content="NOINDEX, FOLLOW">
    <?php endif; ?>
    <?php if ($this->_tpl_vars['oView']->getMetaDescription()): ?>
        <meta name="description" content="<?php echo $this->_tpl_vars['oView']->getMetaDescription(); ?>
">
    <?php endif; ?>
    <?php if ($this->_tpl_vars['oView']->getMetaKeywords()): ?>
        <meta name="keywords" content="<?php echo $this->_tpl_vars['oView']->getMetaKeywords(); ?>
">
    <?php endif; ?>

    <?php if ($this->_tpl_vars['oViewConf']->getFbAppId()): ?>
        <meta property="og:site_name" content="<?php echo $this->_tpl_vars['oViewConf']->getBaseDir(); ?>
">
        <meta property="fb:app_id" content="<?php echo $this->_tpl_vars['oViewConf']->getFbAppId(); ?>
">
        <meta property="og:title" content="<?php echo $this->_tpl_vars['_sMetaTitlePrefix']; ?>
<?php if ($this->_tpl_vars['_sMetaTitlePrefix'] && $this->_tpl_vars['_sMetaTitle']): ?> | <?php endif; ?><?php echo ((is_array($_tmp=$this->_tpl_vars['_sMetaTitle'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
<?php if ($this->_tpl_vars['_sMetaTitleSuffix'] && ( $this->_tpl_vars['_sMetaTitlePrefix'] || $this->_tpl_vars['_sMetaTitle'] )): ?> | <?php endif; ?><?php echo $this->_tpl_vars['_sMetaTitleSuffix']; ?>
 <?php if ($this->_tpl_vars['_sMetaTitlePageSuffix']): ?> | <?php echo $this->_tpl_vars['_sMetaTitlePageSuffix']; ?>
 <?php endif; ?>">
        <?php if ($this->_tpl_vars['oViewConf']->getActiveClassName() == 'details'): ?>
            <meta property="og:type" content="product">
            <meta property="og:image" content="<?php echo $this->_tpl_vars['oView']->getActPicture(); ?>
">
            <meta property="og:url" content="<?php echo $this->_tpl_vars['oView']->getCanonicalUrl(); ?>
">
        <?php else: ?>
            <meta property="og:type" content="website">
            <meta property="og:image" content="<?php echo $this->_tpl_vars['oViewConf']->getImageUrl('basket.png'); ?>
">
            <meta property="og:url" content="<?php echo $this->_tpl_vars['oViewConf']->getCurrentHomeDir(); ?>
">
        <?php endif; ?>
    <?php endif; ?>


    <?php $this->assign('canonical_url', $this->_tpl_vars['oView']->getCanonicalUrl()); ?>
    <?php if ($this->_tpl_vars['canonical_url']): ?>
        <link rel="canonical" href="<?php echo $this->_tpl_vars['canonical_url']; ?>
">
    <?php endif; ?>
    <link rel="shortcut icon" href="<?php echo $this->_tpl_vars['oViewConf']->getImageUrl('favicon.ico'); ?>
">
        
        <?php echo smarty_function_oxstyle(array('include' => "css/reset.css"), $this);?>

        <?php echo smarty_function_oxstyle(array('include' => "css/oxid.css"), $this);?>

        <?php echo smarty_function_oxstyle(array('include' => "css/ie7.css",'if' => 'IE 7'), $this);?>

        <?php echo smarty_function_oxstyle(array('include' => "css/ie8.css",'if' => 'IE 8'), $this);?>

        <?php echo smarty_function_oxstyle(array('include' => "css/libs/jscrollpane.css"), $this);?>

    

    <?php $this->assign('rsslinks', $this->_tpl_vars['oView']->getRssLinks()); ?>
    <?php if ($this->_tpl_vars['rsslinks']): ?>
        <?php $_from = $this->_tpl_vars['rsslinks']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rssentry']):
?>
            <link rel="alternate" type="application/rss+xml" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['rssentry']['title'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" href="<?php echo $this->_tpl_vars['rssentry']['link']; ?>
">
        <?php endforeach; endif; unset($_from); ?>
    <?php endif; ?>

    
        <?php $_from = $this->_tpl_vars['oxidBlock_head']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_block']):
?>
            <?php echo $this->_tpl_vars['_block']; ?>

        <?php endforeach; endif; unset($_from); ?>
    

<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->append('oxidBlock_pageHead', ob_get_contents());ob_end_clean(); ?>
<!DOCTYPE HTML>
<html lang="<?php echo $this->_tpl_vars['oView']->getActiveLangAbbr(); ?>
" <?php if ($this->_tpl_vars['oViewConf']->getShowFbConnect()): ?>xmlns:fb="http://www.facebook.com/2008/fbml"<?php endif; ?>>
<head>
    <?php $_from = $this->_tpl_vars['oxidBlock_pageHead']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_block']):
?>
        <?php echo $this->_tpl_vars['_block']; ?>

    <?php endforeach; endif; unset($_from); ?>
    <?php echo smarty_function_oxstyle(array(), $this);?>

</head>
<body>
    <?php $this->assign('config', $this->_tpl_vars['oViewConf']->getConfig()); ?>
<script>
    <?php echo '
        dataLayer = [{
    '; ?>

                    'googleConversionActive':<?php if ($this->_tpl_vars['config']->getConfigParam('blGoogleConversionActive')): ?>1<?php else: ?>0<?php endif; ?>,
                    'googleCommerceActive':<?php if ($this->_tpl_vars['config']->getConfigParam('blGoogleCommerceActive')): ?>1<?php else: ?>0<?php endif; ?>,
                    'gaActive':<?php if ($this->_tpl_vars['config']->getConfigParam('blGaActive')): ?>1<?php else: ?>0<?php endif; ?>
    <?php echo '
         }];
    '; ?>

</script>

<script>
    dataLayer.push({
        'pageCategory' : '<?php echo $this->_tpl_vars['oViewConf']->getActiveClassName(); ?>
',
        'visitorExistingCustomer' : '<?php if ($this->_tpl_vars['oxcmp_user']): ?>1<?php else: ?>0<?php endif; ?>'
    });
</script>

<?php if ($this->_tpl_vars['oViewConf']->getActiveClassName() == 'register'): ?>
    <script>
        dataLayer.push({
            'registerState' : '<?php echo $this->_tpl_vars['oView']->getRegistrationStatus(); ?>
'
        });
    </script>
<?php endif; ?>

<?php if ($this->_tpl_vars['oViewConf']->getActiveClassName() == 'search'): ?>
    <?php $this->assign('currency', $this->_tpl_vars['oView']->getActCurrency()); ?>
    <script>
        dataLayer.push({
            'searchQuery':'<?php echo $this->_tpl_vars['oView']->getSearchParamForHtml(); ?>
',
            'siteSearchResults': <?php echo $this->_tpl_vars['oView']->getArticleCount(); ?>

        });

        dataLayer.push({
            'searchResults':  [
                <?php $_from = $this->_tpl_vars['oView']->getArticleList(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['search'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['search']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['_product']):
        $this->_foreach['search']['iteration']++;
?>
                    {
                    "sku": "<?php echo $this->_tpl_vars['_product']->oxarticles__oxartnum->value; ?>
",
                    "price": <?php echo ((is_array($_tmp=$this->_tpl_vars['_product']->getFPrice())) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
,
                    "currency": "<?php echo $this->_tpl_vars['currency']->name; ?>
"

                    }<?php if (! ($this->_foreach['search']['iteration'] == $this->_foreach['search']['total'])): ?>,<?php endif; ?>
                <?php endforeach; endif; unset($_from); ?>
            ]
        });
    </script>
<?php endif; ?>


<?php if ($this->_tpl_vars['oViewConf']->getActiveClassName() == 'alist'): ?>
    <?php if (! $this->_tpl_vars['actCategory']): ?>
        <?php $this->assign('actCategory', $this->_tpl_vars['oView']->getActiveCategory()); ?>
    <?php endif; ?>
    <script>
        dataLayer.push({
            'categoryTitle' : '<?php echo $this->_tpl_vars['actCategory']->oxcategories__oxtitle->value; ?>
'
        });
    </script>

<?php endif; ?>

<?php if ($this->_tpl_vars['oViewConf']->getActiveClassName() == 'newsletter'): ?>
    <script>
        dataLayer.push({
           'newsletterState':<?php echo $this->_tpl_vars['oView']->getNewsletterStatus(); ?>

        });
    </script>
<?php endif; ?>

<?php if ($this->_tpl_vars['oxcmp_user']): ?>
    <script>
        dataLayer.push({
            'visitorType':'<?php echo $this->_tpl_vars['oxcmp_user']->getUserValue(); ?>
',
            'visitorLifetimeValue':<?php echo $this->_tpl_vars['oxcmp_user']->getUserLifetimeValue(); ?>

        });
    </script>
<?php endif; ?>

<?php if ($this->_tpl_vars['oViewConf']->getActiveClassName() == 'thankyou'): ?>
    <?php if (! $this->_tpl_vars['order']): ?>
        <?php $this->assign('order', $this->_tpl_vars['oView']->getOrder()); ?>
    <?php endif; ?>

    <?php if ($this->_tpl_vars['order']): ?>



        <?php if ($this->_tpl_vars['config']->getConfigParam('blGoogleConversionActive')): ?>
                <script>
            dataLayer.push({
                'conversionValue':<?php echo $this->_tpl_vars['order']->oxorder__oxtotalordersum->value; ?>
,
                'conversionId':'<?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
',
                'conversionType':'<?php echo $this->_tpl_vars['order']->getConversionType(); ?>
',
                'conversionAttributes':'<?php echo $this->_tpl_vars['order']->getConversionAttributes(); ?>
',
                'conversionDate':'<?php echo $this->_tpl_vars['order']->oxorder__oxorderdate->value; ?>
'
            });
        </script>
        <?php endif; ?>

        <?php if ($this->_tpl_vars['config']->getConfigParam('blGoogleCommerceActive')): ?>
        <script>
            
            dataLayer.push({
                'transactionTotal':<?php echo $this->_tpl_vars['order']->oxorder__oxtotalordersum->value; ?>
,
                'transactionId':'<?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
',
                'transactionCurrency':'<?php echo $this->_tpl_vars['order']->oxorder__oxcurrency->value; ?>
',
                'transactionDate':'<?php echo $this->_tpl_vars['order']->oxorder__oxorderdate->value; ?>
',
                'transactionShipping':<?php echo $this->_tpl_vars['order']->oxorder__oxdeliverycost->value; ?>
,
                'transactionType':'<?php echo $this->_tpl_vars['order']->getConversionType(); ?>
',
                'transactionTax':<?php echo smarty_function_math(array('equation' => "x-y",'x' => $this->_tpl_vars['order']->oxorder__oxtotalordersum->value,'y' => $this->_tpl_vars['order']->oxorder__oxtotalnetsum->value,'format' => "%.2f"), $this);?>
,
                'transactionPaymentType':'<?php echo $this->_tpl_vars['order']->oxorder__oxpaymenttype->value; ?>
',
                'transactionShippingMethod':'<?php echo $this->_tpl_vars['order']->getDeliveryMethod(); ?>
',
                'transactionPromoCode':'<?php echo $this->_tpl_vars['order']->getPromoCode(); ?>
',
                'transactionAffiliation':'<?php echo $this->_tpl_vars['order']->getAffiliation(); ?>
',
                'orderNetValue': <?php echo $this->_tpl_vars['order']->oxorder__oxtotalnetsum->value; ?>
,
                'neworder':1
            });
        </script>
        <script>
            dataLayer.push({
            'transactionProducts': [
            <?php $_from = $this->_tpl_vars['order']->getOrderArticles(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['transProds'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['transProds']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['oOrderArticle']):
        $this->_foreach['transProds']['iteration']++;
?>
                {
                    name: '<?php echo $this->_tpl_vars['oOrderArticle']->oxorderarticles__oxtitle->value; ?>
',
                    sku: '<?php echo $this->_tpl_vars['oOrderArticle']->oxorderarticles__oxartnum->value; ?>
',
                    category: '<?php echo $this->_tpl_vars['oOrderArticle']->getCategoryName(true); ?>
',
                    price: <?php echo $this->_tpl_vars['oOrderArticle']->oxorderarticles__oxprice->value; ?>
,
                    quantity: <?php echo $this->_tpl_vars['oOrderArticle']->oxorderarticles__oxamount->value; ?>

                }<?php if (! ($this->_foreach['transProds']['iteration'] == $this->_foreach['transProds']['total'])): ?>,<?php endif; ?>
            <?php endforeach; endif; unset($_from); ?>
            ]
            });

        </script>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['config']->getConfigParam('blGoogleDynamicRemarketing')): ?>

<?php if ($this->_tpl_vars['oViewConf']->getActiveClassName() == 'basket'): ?>
        <script>
            var google_tag_params = { <?php echo $this->_tpl_vars['oxcmp_shop']->getJsonData('cart',$this->_tpl_vars['oView']->getBasketArticles(),$this->_tpl_vars['oxcmp_basket']->getFPrice()); ?>
 }
        </script>
<?php elseif ($this->_tpl_vars['oViewConf']->getActiveClassName() == 'thankyou'): ?>
    <?php if (! $this->_tpl_vars['order']): ?>
        <?php $this->assign('order', $this->_tpl_vars['oView']->getOrder()); ?>
    <?php endif; ?>

    <?php if ($this->_tpl_vars['order']): ?>
        <script>
            var google_tag_params = { <?php echo $this->_tpl_vars['oxcmp_shop']->getJsonData('purchase',$this->_tpl_vars['order']); ?>
 }
        </script>
    <?php endif; ?>
<?php elseif ($this->_tpl_vars['oViewConf']->getActiveClassName() == 'alist'): ?>
    <?php if (! $this->_tpl_vars['actCategory']): ?>
        <?php $this->assign('actCategory', $this->_tpl_vars['oView']->getActiveCategory()); ?>
    <?php endif; ?>
        <script>
            var google_tag_params = { <?php echo $this->_tpl_vars['oxcmp_shop']->getJsonData('alist',$this->_tpl_vars['oView']->getArticleList()); ?>
 }
        </script>
<?php elseif ($this->_tpl_vars['oViewConf']->getActiveClassName() == 'details'): ?>
        <script>
            var google_tag_params = { <?php echo $this->_tpl_vars['oxcmp_shop']->getJsonData('product',$this->_tpl_vars['oView']->getProduct()); ?>
 }
        </script>
<?php elseif ($this->_tpl_vars['oViewConf']->getActiveClassName() == 'start'): ?>
        <script>
            var google_tag_params = { <?php echo $this->_tpl_vars['oxcmp_shop']->getJsonData('start',null); ?>
 }
        </script>
<?php else: ?>
    <script>
        var google_tag_params = { <?php echo $this->_tpl_vars['oxcmp_shop']->getJsonData('siteview',null); ?>
 }
    </script>
<?php endif; ?>
    <script>
        dataLayer.push({
          'google_tag_params': window.google_tag_params
        });
    </script>
<?php endif; ?>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=<?php echo $this->_tpl_vars['config']->getConfigParam('sGtmId'); ?>
"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','<?php echo $this->_tpl_vars['config']->getConfigParam('sGtmId'); ?>
');</script>
<!-- End Google Tag Manager -->


    
    <?php $_from = $this->_tpl_vars['oxidBlock_pageBody']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_block']):
?>
        <?php echo $this->_tpl_vars['_block']; ?>

    <?php endforeach; endif; unset($_from); ?>
    <?php $_from = $this->_tpl_vars['oxidBlock_pagePopup']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_block']):
?>
        <?php echo $this->_tpl_vars['_block']; ?>

    <?php endforeach; endif; unset($_from); ?>

    
        <?php echo smarty_function_oxscript(array('include' => "js/libs/jquery.min.js",'priority' => 1), $this);?>

        <?php echo smarty_function_oxscript(array('include' => "js/libs/cookie/jquery.cookie.js",'priority' => 1), $this);?>

        <?php echo smarty_function_oxscript(array('include' => "js/libs/jquery-ui.min.js",'priority' => 1), $this);?>

        <?php echo smarty_function_oxscript(array('include' => 'js/libs/superfish/hoverIntent.js'), $this);?>

        <?php echo smarty_function_oxscript(array('include' => 'js/libs/superfish/supersubs.js'), $this);?>

        <?php echo smarty_function_oxscript(array('include' => 'js/libs/superfish/superfish.js'), $this);?>

        <?php echo smarty_function_oxscript(array('include' => 'js/libs/dynamicload.js'), $this);?>

    

    <?php if ($this->_tpl_vars['oViewConf']->isTplBlocksDebugMode()): ?>
        <?php echo smarty_function_oxscript(array('include' => "js/widgets/oxblockdebug.js"), $this);?>

        <?php echo smarty_function_oxscript(array('add' => "$( 'hr.debugBlocksStart' ).oxBlockDebug();"), $this);?>

    <?php endif; ?>

    <?php echo smarty_function_oxscript(array(), $this);?>

    <?php echo smarty_function_oxid_include_dynamic(array('file' => "widget/dynscript.tpl"), $this);?>


    <?php $_from = $this->_tpl_vars['oxidBlock_pageScript']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_block']):
?>
        <?php echo $this->_tpl_vars['_block']; ?>

    <?php endforeach; endif; unset($_from); ?>

    <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="<?php echo $this->_tpl_vars['oViewConf']->getResourceUrl('js/libs/IE9.js'); ?>
"></script>
    <![endif]-->


</body>
</html>
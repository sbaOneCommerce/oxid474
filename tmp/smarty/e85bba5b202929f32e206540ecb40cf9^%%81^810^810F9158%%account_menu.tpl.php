<?php /* Smarty version 2.6.26, created on 2014-01-09 11:16:51
         compiled from page/account/inc/account_menu.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxscript', 'page/account/inc/account_menu.tpl', 1, false),array('function', 'oxgetseourl', 'page/account/inc/account_menu.tpl', 5, false),array('function', 'oxmultilang', 'page/account/inc/account_menu.tpl', 5, false),array('modifier', 'cat', 'page/account/inc/account_menu.tpl', 5, false),)), $this); ?>
<?php echo smarty_function_oxscript(array('include' => "js/widgets/oxequalizer.js",'priority' => 10), $this);?>

<?php echo smarty_function_oxscript(array('add' => "$(function(){oxEqualizer.equalHeight($('.sidebarMenu'), $('#content'));});"), $this);?>

<ul class="tree sidebarMenu corners">
    
        <li <?php if ($this->_tpl_vars['active_link'] == 'password'): ?>class="active"<?php endif; ?>><a href="<?php echo smarty_function_oxgetseourl(array('ident' => ((is_array($_tmp=$this->_tpl_vars['oViewConf']->getSslSelfLink())) ? $this->_run_mod_handler('cat', true, $_tmp, "cl=account_password") : smarty_modifier_cat($_tmp, "cl=account_password"))), $this);?>
" rel="nofollow"><?php echo smarty_function_oxmultilang(array('ident' => 'CHANGE_PASSWORD_2'), $this);?>
</a></li>
        <li <?php if ($this->_tpl_vars['active_link'] == 'newsletter'): ?>class="active"<?php endif; ?>><a href="<?php echo smarty_function_oxgetseourl(array('ident' => ((is_array($_tmp=$this->_tpl_vars['oViewConf']->getSslSelfLink())) ? $this->_run_mod_handler('cat', true, $_tmp, "cl=account_newsletter") : smarty_modifier_cat($_tmp, "cl=account_newsletter"))), $this);?>
" rel="nofollow"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_ACCOUNT_INC_ACCOUNT_MENU_NEWSLETTERSETTINGS'), $this);?>
</a></li>
        <li <?php if ($this->_tpl_vars['active_link'] == 'billship'): ?>class="active"<?php endif; ?>><a href="<?php echo smarty_function_oxgetseourl(array('ident' => ((is_array($_tmp=$this->_tpl_vars['oViewConf']->getSslSelfLink())) ? $this->_run_mod_handler('cat', true, $_tmp, "cl=account_user") : smarty_modifier_cat($_tmp, "cl=account_user"))), $this);?>
" rel="nofollow"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_ACCOUNT_INC_ACCOUNT_MENU_BILLINGSHIPPINGSET'), $this);?>
</a></li>
        <li <?php if ($this->_tpl_vars['active_link'] == 'orderhistory'): ?>class="active"<?php endif; ?>><a href="<?php echo smarty_function_oxgetseourl(array('ident' => ((is_array($_tmp=$this->_tpl_vars['oViewConf']->getSelfLink())) ? $this->_run_mod_handler('cat', true, $_tmp, "cl=account_order") : smarty_modifier_cat($_tmp, "cl=account_order"))), $this);?>
" rel="nofollow"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_ACCOUNT_INC_ACCOUNT_MENU_ORDERHISTORY'), $this);?>
</a></li>
        <?php if ($this->_tpl_vars['oViewConf']->getShowCompareList()): ?>
            <li <?php if ($this->_tpl_vars['active_link'] == 'compare'): ?>class="active"<?php endif; ?>><a href="<?php echo smarty_function_oxgetseourl(array('ident' => ((is_array($_tmp=$this->_tpl_vars['oViewConf']->getSelfLink())) ? $this->_run_mod_handler('cat', true, $_tmp, "cl=compare") : smarty_modifier_cat($_tmp, "cl=compare"))), $this);?>
" rel="nofollow"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_ACCOUNT_INC_ACCOUNT_MENU_COMPARE'), $this);?>
</a></li>
        <?php endif; ?>
        <li <?php if ($this->_tpl_vars['active_link'] == 'noticelist'): ?>class="active"<?php endif; ?>><a href="<?php echo smarty_function_oxgetseourl(array('ident' => ((is_array($_tmp=$this->_tpl_vars['oViewConf']->getSelfLink())) ? $this->_run_mod_handler('cat', true, $_tmp, "cl=account_noticelist") : smarty_modifier_cat($_tmp, "cl=account_noticelist"))), $this);?>
" rel="nofollow"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_ACCOUNT_INC_ACCOUNT_MENU_NOTICELIST'), $this);?>
</a></li>
        <?php if ($this->_tpl_vars['oViewConf']->getShowWishlist()): ?>
            <li <?php if ($this->_tpl_vars['active_link'] == 'wishlist'): ?>class="active"<?php endif; ?>><a href="<?php echo smarty_function_oxgetseourl(array('ident' => ((is_array($_tmp=$this->_tpl_vars['oViewConf']->getSelfLink())) ? $this->_run_mod_handler('cat', true, $_tmp, "cl=account_wishlist") : smarty_modifier_cat($_tmp, "cl=account_wishlist"))), $this);?>
" rel="nofollow"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_ACCOUNT_INC_ACCOUNT_MENU_WISHLIST'), $this);?>
</a></li>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['oViewConf']->getShowListmania()): ?>
            <li <?php if ($this->_tpl_vars['active_link'] == 'recommendationlist'): ?>class="active"<?php endif; ?>><a href="<?php echo smarty_function_oxgetseourl(array('ident' => ((is_array($_tmp=$this->_tpl_vars['oViewConf']->getSelfLink())) ? $this->_run_mod_handler('cat', true, $_tmp, "cl=account_recommlist") : smarty_modifier_cat($_tmp, "cl=account_recommlist"))), $this);?>
" rel="nofollow"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_ACCOUNT_INC_ACCOUNT_MENU_RECOMMENDLIST'), $this);?>
</a></li>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['oView']->isEnabledDownloadableFiles()): ?>
        <li <?php if ($this->_tpl_vars['active_link'] == 'downloads'): ?>class="active"<?php endif; ?>><a href="<?php echo smarty_function_oxgetseourl(array('ident' => ((is_array($_tmp=$this->_tpl_vars['oViewConf']->getSelfLink())) ? $this->_run_mod_handler('cat', true, $_tmp, "cl=account_downloads") : smarty_modifier_cat($_tmp, "cl=account_downloads"))), $this);?>
" rel="nofollow"><?php echo smarty_function_oxmultilang(array('ident' => 'MY_DOWNLOADS'), $this);?>
</a></li>
        <?php endif; ?>
    
</ul>
<?php /* Smarty version 2.6.26, created on 2014-01-09 11:16:17
         compiled from page/checkout/inc/payment_oxiddebitnote.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxmultilang', 'page/checkout/inc/payment_oxiddebitnote.tpl', 10, false),)), $this); ?>
<?php $this->assign('dynvalue', $this->_tpl_vars['oView']->getDynValue()); ?>
<dl>
    <dt>
        <input id="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
" type="radio" name="paymentid" value="<?php echo $this->_tpl_vars['sPaymentID']; ?>
" <?php if ($this->_tpl_vars['oView']->getCheckedPaymentId() == $this->_tpl_vars['paymentmethod']->oxpayments__oxid->value): ?>checked<?php endif; ?>>
        <label for="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
"><b><?php echo $this->_tpl_vars['paymentmethod']->oxpayments__oxdesc->value; ?>
</b></label>
    </dt>
    <dd class="<?php if ($this->_tpl_vars['oView']->getCheckedPaymentId() == $this->_tpl_vars['paymentmethod']->oxpayments__oxid->value): ?>activePayment<?php endif; ?>">
        <ul class="form">
            <li>
                <label><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_BANK'), $this);?>
</label>
                <input id="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
_1" class="js-oxValidate js-oxValidate_notEmpty" type="text" size="20" maxlength="64" name="dynvalue[lsbankname]" value="<?php echo $this->_tpl_vars['dynvalue']['lsbankname']; ?>
">
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty"><?php echo smarty_function_oxmultilang(array('ident' => 'EXCEPTION_INPUT_NOTALLFIELDS'), $this);?>
</span>
                </p>
            </li>
            <li>
                <label><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_ROUTINGNUMBER'), $this);?>
</label>
                <input type="text" class="js-oxValidate js-oxValidate_notEmpty" size="20" maxlength="64" name="dynvalue[lsblz]" value="<?php echo $this->_tpl_vars['dynvalue']['lsblz']; ?>
">
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty"><?php echo smarty_function_oxmultilang(array('ident' => 'EXCEPTION_INPUT_NOTALLFIELDS'), $this);?>
</span>
                </p>
            </li>
            <li>
                <label><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_ACCOUNTNUMBER'), $this);?>
</label>
                <input type="text" class="js-oxValidate js-oxValidate_notEmpty" size="20" maxlength="64" name="dynvalue[lsktonr]" value="<?php echo $this->_tpl_vars['dynvalue']['lsktonr']; ?>
">
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty"><?php echo smarty_function_oxmultilang(array('ident' => 'EXCEPTION_INPUT_NOTALLFIELDS'), $this);?>
</span>
                </p>
            </li>
            <li>
                <label><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_ACCOUNTHOLDER2'), $this);?>
</label>
                <input type="text" class="js-oxValidate js-oxValidate_notEmpty" size="20" maxlength="64" name="dynvalue[lsktoinhaber]" value="<?php if ($this->_tpl_vars['dynvalue']['lsktoinhaber']): ?><?php echo $this->_tpl_vars['dynvalue']['lsktoinhaber']; ?>
<?php else: ?><?php echo $this->_tpl_vars['oxcmp_user']->oxuser__oxfname->value; ?>
 <?php echo $this->_tpl_vars['oxcmp_user']->oxuser__oxlname->value; ?>
<?php endif; ?>">
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty"><?php echo smarty_function_oxmultilang(array('ident' => 'EXCEPTION_INPUT_NOTALLFIELDS'), $this);?>
</span>
                </p>
            </li>
        </ul>

        
            <?php if ($this->_tpl_vars['paymentmethod']->oxpayments__oxlongdesc->value): ?>
                <div class="desc">
                    <?php echo $this->_tpl_vars['paymentmethod']->oxpayments__oxlongdesc->getRawValue(); ?>

                </div>
            <?php endif; ?>
        
    </dd>
</dl>
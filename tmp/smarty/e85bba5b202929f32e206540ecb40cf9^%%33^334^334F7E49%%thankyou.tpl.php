<?php /* Smarty version 2.6.26, created on 2014-01-09 11:16:27
         compiled from page/checkout/thankyou.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxmultilang', 'page/checkout/thankyou.tpl', 13, false),array('function', 'oxgetseourl', 'page/checkout/thankyou.tpl', 29, false),array('modifier', 'cat', 'page/checkout/thankyou.tpl', 32, false),array('insert', 'oxid_tracker', 'page/checkout/thankyou.tpl', 140, false),)), $this); ?>
<?php ob_start(); ?>

    <?php if ($this->_tpl_vars['oView']->showFinalStep()): ?>
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page/checkout/inc/steps.tpl", 'smarty_include_vars' => array('active' => 5)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php endif; ?>

    
        <?php $this->assign('order', $this->_tpl_vars['oView']->getOrder()); ?>
        <?php $this->assign('basket', $this->_tpl_vars['oView']->getBasket()); ?>

        <div id="thankyouPage">
            <h3 class="blockHead"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_TITLE'), $this);?>
</h3>

            
                <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_THANKYOU1'), $this);?>
 <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_THANKYOU2'), $this);?>
 <?php echo $this->_tpl_vars['oxcmp_shop']->oxshops__oxname->value; ?>
. <br>
                <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_REGISTEREDYOUORDERNO1'), $this);?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
 <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_REGISTEREDYOUORDERNO2'), $this);?>
<br>
                <?php if (! $this->_tpl_vars['oView']->getMailError()): ?>
                    <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_YOURECEIVEDORDERCONFIRM'), $this);?>
<br>
                <?php else: ?><br>
                    <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_CONFIRMATIONNOTSUCCEED'), $this);?>
<br>
                <?php endif; ?>
                <br>
                <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_WEWILLINFORMYOU'), $this);?>
<br><br>
            

            
                <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_YOUCANGO'), $this);?>

                <a id="backToShop" rel="nofollow" href="<?php echo smarty_function_oxgetseourl(array('ident' => $this->_tpl_vars['oViewConf']->getHomeLink()), $this);?>
" class="link"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_BACKTOSHOP'), $this);?>
</a>
                <?php if ($this->_tpl_vars['oxcmp_user']->oxuser__oxpassword->value): ?>
                    <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_OR'), $this);?>

                    <a id="orderHistory" rel="nofollow" href="<?php echo smarty_function_oxgetseourl(array('ident' => ((is_array($_tmp=$this->_tpl_vars['oViewConf']->getSelfLink())) ? $this->_run_mod_handler('cat', true, $_tmp, "cl=account_order") : smarty_modifier_cat($_tmp, "cl=account_order"))), $this);?>
" class="link"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_ORDERHISTORY'), $this);?>
</a>.
                <?php endif; ?>
            

            
                <?php if ($this->_tpl_vars['oViewConf']->showTs('THANKYOU') && $this->_tpl_vars['oViewConf']->getTsId()): ?>
                    <?php $this->assign('sTSRatingImg', ((is_array($_tmp=((is_array($_tmp="https://www.trustedshops.com/bewertung/widget/img/bewerten_")) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['oView']->getActiveLangAbbr()) : smarty_modifier_cat($_tmp, $this->_tpl_vars['oView']->getActiveLangAbbr())))) ? $this->_run_mod_handler('cat', true, $_tmp, ".gif") : smarty_modifier_cat($_tmp, ".gif"))); ?>
                    <h3 class="blockHead"><?php echo smarty_function_oxmultilang(array('ident' => 'TS_RATINGS'), $this);?>
</h3>
                    <?php echo smarty_function_oxmultilang(array('ident' => 'TS_RATINGS_RATEUS'), $this);?>

                    <div class="etrustTsRatingButton">
                        <a href="<?php echo $this->_tpl_vars['oViewConf']->getTsRatingUrl(); ?>
" target="_blank" title="<?php echo smarty_function_oxmultilang(array('ident' => 'TS_RATINGS_URL_TITLE'), $this);?>
">
                            <img src="<?php echo $this->_tpl_vars['sTSRatingImg']; ?>
" border="0" alt="<?php echo smarty_function_oxmultilang(array('ident' => 'TS_RATINGS_BUTTON_ALT'), $this);?>
" align="middle">
                        </a>
                    </div>
                <?php endif; ?>
            

            
                <?php if (( $this->_tpl_vars['oView']->getTrustedShopId() ) || $this->_tpl_vars['iswebmiles'] || $this->_tpl_vars['oxcmp_shop']->oxshops__oxadbutlerid->value || $this->_tpl_vars['oxcmp_shop']->oxshops__oxaffilinetid->value || $this->_tpl_vars['oxcmp_shop']->oxshops__oxsuperclicksid->value || $this->_tpl_vars['oxcmp_shop']->oxshops__oxaffiliweltid->value || $this->_tpl_vars['oxcmp_shop']->oxshops__oxaffili24id->value): ?>

                    <h3 class="blockHead"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_PARTNERFROM'), $this);?>
</h3>
                    <?php if ($this->_tpl_vars['oView']->getTrustedShopId()): ?>
                        <div class="etrustlogocol">
                            <a href="https://www.trustedshops.com/shop/certificate.php?shop_id=<?php echo $this->_tpl_vars['oView']->getTrustedShopId(); ?>
" target="_blank">
                                <img src="<?php echo $this->_tpl_vars['oViewConf']->getImageUrl('trustedshops_m.gif'); ?>
" title="<?php echo smarty_function_oxmultilang(array('ident' => 'INC_TRUSTEDSHOPS_ITEM_IMGTITLE'), $this);?>
">
                            </a>
                        </div>
                        <form id="formTsShops" name="formTShops" method="post" action="https://www.trustedshops.com/shop/protection.php" target="_blank">
                          <div>
                              <input type="hidden" name="_charset_">
                              <input name="shop_id" type=hidden value="<?php echo $this->_tpl_vars['oView']->getTrustedShopId(); ?>
">
                              <input name="email" type="hidden" value="<?php echo $this->_tpl_vars['oxcmp_user']->oxuser__oxusername->value; ?>
">
                              <input name="amount" type=hidden value="<?php echo $this->_tpl_vars['order']->getTotalOrderSum(); ?>
">
                              <input name="curr" type=hidden value="<?php echo $this->_tpl_vars['order']->oxorder__oxcurrency->value; ?>
">
                              <input name="payment" type=hidden value="">
                              <input name="KDNR" type="hidden" value="<?php echo $this->_tpl_vars['oxcmp_user']->oxuser__oxcustnr->value; ?>
">
                              <input name="ORDERNR" type="hidden" value="<?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
">
                              <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_TRUSTEDSHOPMESSAGE'), $this);?>
<br><br>
                              <span><input type="submit" id="btnProtect" name="btnProtect" value="<?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_LOGGIN'), $this);?>
"></span>
                          </div>
                        </form>
                        <div class="clear"></div>
                    <?php endif; ?>

                    <!-- Anfang Tracking-Code fuer Partnerprogramme -->

                    <?php if ($this->_tpl_vars['oxcmp_shop']->oxshops__oxadbutlerid->value): ?>
                        <!--Adbutler-->
                        <?php $this->assign('discountnetprice', $this->_tpl_vars['basket']->getDiscountedNettoPrice()); ?>
                        <?php $this->assign('currencycovindex', $this->_tpl_vars['oView']->getCurrencyCovIndex()); ?>
                        <img src="https://www1.belboon.de/adtracking/sale/<?php echo $this->_tpl_vars['oxcmp_shop']->oxshops__oxadbutlerid->value; ?>
.gif/oc=<?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
&sale=<?php echo $this->_tpl_vars['discountnetprice']; ?>
&belboon=<?php echo $this->_tpl_vars['oView']->getBelboonParam(); ?>
" WIDTH="1" HEIGHT="1">
                        <object type="application/x-shockwave-flash" data="http://www1.belboon.de/tracking/flash.swf" width="1" height="1" >
                            <param name="flashvars" value="pgmid=<?php echo $this->_tpl_vars['oxcmp_shop']->oxshops__oxadbutlerid->value; ?>
&etype=sale&tparam=sale&evalue=<?php echo $this->_tpl_vars['discountnetprice']; ?>
&oc=<?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
">
                            <param name="movie" value="http://www1.belboon.de/tracking/flash.swf" />
                        </object>
                        <!--Adbutler ende-->
                    <?php endif; ?>

                    <?php if ($this->_tpl_vars['oxcmp_shop']->oxshops__oxaffilinetid->value): ?>
                        <!--Affilinet-->
                        <?php $this->assign('discountnetprice', $this->_tpl_vars['basket']->getDiscountedNettoPrice()); ?>
                        <?php $this->assign('currencycovindex', $this->_tpl_vars['oView']->getCurrencyCovIndex()); ?>
                        <img src="https://partners.webmasterplan.com/registersale.asp?site=<?php echo $this->_tpl_vars['oxcmp_shop']->oxshops__oxaffilinetid->value; ?>
&amp;order=<?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
&amp;curr=<?php echo $this->_tpl_vars['order']->oxorder__oxcurrency->value; ?>
&amp;price=<?php echo $this->_tpl_vars['discountnetprice']; ?>
" WIDTH="1" HEIGHT="1">
                        <!--Affilinet Ende-->
                    <?php endif; ?>

                    <?php if ($this->_tpl_vars['oxcmp_shop']->oxshops__oxsuperclicksid->value): ?>
                        <!--Superclix-Code-->
                        <?php $this->assign('discountnetprice', $this->_tpl_vars['basket']->getDiscountedNettoPrice()); ?>
                        <?php $this->assign('currencycovindex', $this->_tpl_vars['oView']->getCurrencyCovIndex()); ?>
                        <img src="https://clix.superclix.de/cgi-bin/code.cgi?pp=<?php echo $this->_tpl_vars['oxcmp_shop']->oxshops__oxsuperclicksid->value; ?>
&amp;cashflow=<?php echo $this->_tpl_vars['discountnetprice']; ?>
&amp;tax=1.00&amp;goods=<?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
" width="1" height="1">
                        <!--Superclix Ende-->
                    <?php endif; ?>

                    <?php if ($this->_tpl_vars['oxcmp_shop']->oxshops__oxaffiliweltid->value): ?>
                        <!--Affiliwelt-Code-->
                        <!--img src="https://www.affiliwelt.net/partner/sregistering.php3?ID=<?php echo $this->_tpl_vars['oxcmp_shop']->oxshops__oxaffiliweltid->value; ?>
&track=<?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
&wert=<?php echo $this->_tpl_vars['basket']->getDiscountedNettoPrice(); ?>
&mone=EUR" width="1" height="1" border="0"-->
                        <?php $this->assign('discountnetprice', $this->_tpl_vars['basket']->getDiscountedNettoPrice()); ?>
                        <?php $this->assign('currencycovindex', $this->_tpl_vars['oView']->getCurrencyCovIndex()); ?>
                        <img src="https://www.affiliwelt.net/tracking.php?prid=<?php echo $this->_tpl_vars['oxcmp_shop']->oxshops__oxaffiliweltid->value; ?>
&amp;bestid=<?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
&amp;beschreibung=OXID&preis=<?php echo $this->_tpl_vars['discountnetprice']; ?>
" width="1" height="1">
                        <!--Affiliwelt Ende-->
                    <?php endif; ?>

                    <?php if ($this->_tpl_vars['oxcmp_shop']->oxshops__oxaffili24id->value): ?>
                        <!--Affili24.com-->
                        <?php $this->assign('discountnetprice', $this->_tpl_vars['basket']->getDiscountedNettoPrice()); ?>
                        <?php $this->assign('currencycovindex', $this->_tpl_vars['oView']->getCurrencyCovIndex()); ?>
                        <img src="https://partners.affili24.com/registering.php?ID=<?php echo $this->_tpl_vars['oxcmp_shop']->oxshops__oxaffili24id->value; ?>
&amp;track=<?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
&amp;wert=<?php echo $this->_tpl_vars['discountnetprice']; ?>
" width="1" height="1">
                        <!--Affili24 Ende-->
                    <?php endif; ?>

                    <!-- Ende Tracking-Code fuer Partnerprogramme -->
                <?php endif; ?>
            

            <?php if ($this->_tpl_vars['oView']->showFinalStep()): ?>
                <?php if ($this->_tpl_vars['oView']->getAlsoBoughtTheseProducts()): ?>
                    <br><br>
                    <h1 class="pageHead">
                         <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_THANKYOU_ALSOBOUGHT'), $this);?>

                    </h1>
                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "widget/product/list.tpl", 'smarty_include_vars' => array('type' => $this->_tpl_vars['oView']->getListDisplayType(),'listId' => 'alsoBoughtThankyou','products' => $this->_tpl_vars['oView']->getAlsoBoughtTheseProducts(),'blDisableToCart' => true)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'oxid_tracker', 'title' => $this->_tpl_vars['template_title'])), $this); ?>

<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->append('oxidBlock_content', ob_get_contents());ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "layout/page.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
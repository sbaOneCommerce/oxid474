<?php /* Smarty version 2.6.26, created on 2014-01-09 11:16:27
         compiled from /home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/html/order_owner.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxcontent', '/home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/html/order_owner.tpl', 15, false),array('function', 'oxmultilang', '/home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/html/order_owner.tpl', 20, false),array('modifier', 'strip_tags', '/home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/html/order_owner.tpl', 51, false),array('modifier', 'replace', '/home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/html/order_owner.tpl', 284, false),array('modifier', 'oxescape', '/home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/html/order_owner.tpl', 638, false),array('modifier', 'oxmultilangsal', '/home/demfoqqq/www.demo-oxid-ce.de/demo474/application/views/azure/tpl/email/html/order_owner.tpl', 679, false),)), $this); ?>
<?php $this->assign('shop', $this->_tpl_vars['oEmailView']->getShop()); ?>
<?php $this->assign('oViewConf', $this->_tpl_vars['oEmailView']->getViewConfig()); ?>
<?php $this->assign('oConf', $this->_tpl_vars['oViewConf']->getConfig()); ?>
<?php $this->assign('currency', $this->_tpl_vars['oEmailView']->getCurrency()); ?>
<?php $this->assign('user', $this->_tpl_vars['oEmailView']->getUser()); ?>
<?php $this->assign('basket', $this->_tpl_vars['order']->getBasket()); ?>
<?php $this->assign('oDelSet', $this->_tpl_vars['order']->getDelSet()); ?>
<?php $this->assign('payment', $this->_tpl_vars['order']->getPayment()); ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "email/html/header.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['shop']->oxshops__oxordersubject->value)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

        
            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 10px 0;">
                <?php if ($this->_tpl_vars['payment']->oxuserpayments__oxpaymentsid->value == 'oxempty'): ?>
                    <?php echo smarty_function_oxcontent(array('ident' => 'oxadminordernpemail'), $this);?>

                <?php else: ?>
                    <?php echo smarty_function_oxcontent(array('ident' => 'oxadminorderemail'), $this);?>

                <?php endif; ?>

                <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_ORDERNOMBER'), $this);?>
 <b><?php echo $this->_tpl_vars['order']->oxorder__oxordernr->value; ?>
</b>
            </p>
        

            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td height="15" width="100" style="padding: 5px; border-bottom: 4px solid #ddd;">
                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 0;"><b><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PRODUCT'), $this);?>
</b></p>
                    </td>
                    <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                        &nbsp;
                    </td>
                    <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 0;"><b><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_UNITPRICE'), $this);?>
</b></p>
                    </td>
                    <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 0;"><b><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_QUANTITY'), $this);?>
</b></p>
                    </td>
                    <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 0;"><b><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_VAT'), $this);?>
</b></p>
                    </td>
                    <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 0;"><b><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTAL'), $this);?>
</b></p>
                    </td>
                </tr>
                <?php $this->assign('basketitemlist', $this->_tpl_vars['basket']->getBasketArticles()); ?>
                <?php $_from = $this->_tpl_vars['basket']->getContents(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['basketindex'] => $this->_tpl_vars['basketitem']):
?>
                    
                        <?php $this->assign('basketproduct', $this->_tpl_vars['basketitemlist'][$this->_tpl_vars['basketindex']]); ?>
                        <tr valign="top">
                            <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                                <img src="<?php echo $this->_tpl_vars['basketproduct']->getThumbnailUrl(false); ?>
" border="0" hspace="0" vspace="0" alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['basketitem']->getTitle())) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" align="texttop">
                                <?php if ($this->_tpl_vars['oViewConf']->getShowGiftWrapping()): ?>
                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 10px 0;">
                                    <?php $this->assign('oWrapping', $this->_tpl_vars['basketitem']->getWrapping()); ?>
                                    <b><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_WRAPPING'), $this);?>
&nbsp;</b><?php if (! $this->_tpl_vars['basketitem']->getWrappingId()): ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_NONE'), $this);?>
<?php else: ?><?php echo $this->_tpl_vars['oWrapping']->oxwrapping__oxname->value; ?>
<?php endif; ?>
                                </p>
                                <?php endif; ?>
                            </td>
                            <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 10px 0;">
                                    <b><?php echo $this->_tpl_vars['basketitem']->getTitle(); ?>
</b>
                                    <?php if ($this->_tpl_vars['basketitem']->getChosenSelList()): ?>,
                                    <?php $_from = $this->_tpl_vars['basketitem']->getChosenSelList(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['oList']):
?>
                                    <?php echo $this->_tpl_vars['oList']->name; ?>
 <?php echo $this->_tpl_vars['oList']->value; ?>
&nbsp;
                                    <?php endforeach; endif; unset($_from); ?>
                                    <?php endif; ?>
                                    <?php if ($this->_tpl_vars['basketitem']->getPersParams()): ?>
                                    <?php $_from = $this->_tpl_vars['basketitem']->getPersParams(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sVar'] => $this->_tpl_vars['aParam']):
?>
                                    ,&nbsp;<em><?php echo $this->_tpl_vars['sVar']; ?>
 : <?php echo $this->_tpl_vars['aParam']; ?>
</em>
                                    <?php endforeach; endif; unset($_from); ?>
                                    <?php endif; ?>
                                    <br><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_ARTNOMBER'), $this);?>
 <?php echo $this->_tpl_vars['basketproduct']->oxarticles__oxartnum->value; ?>

                                </p>
                            </td>
                            <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 10px 0;">
                                    <b><?php if ($this->_tpl_vars['basketitem']->getFUnitPrice()): ?><?php echo $this->_tpl_vars['basketitem']->getFUnitPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
<?php endif; ?></b>
                                    <?php if (! $this->_tpl_vars['basketitem']->isBundle()): ?>
                                        <?php $this->assign('dRegUnitPrice', $this->_tpl_vars['basketitem']->getRegularUnitPrice()); ?>
                                        <?php $this->assign('dUnitPrice', $this->_tpl_vars['basketitem']->getUnitPrice()); ?>
                                        <?php if ($this->_tpl_vars['dRegUnitPrice']->getPrice() > $this->_tpl_vars['dUnitPrice']->getPrice()): ?>
                                        <br><s><?php echo $this->_tpl_vars['basketitem']->getFRegularUnitPrice(); ?>
&nbsp;<?php echo $this->_tpl_vars['currency']->sign; ?>
</s>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if ($this->_tpl_vars['basketitem']->aDiscounts): ?><br><br>
                                    <em style="font-size: 7pt;font-weight: normal;"><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_DISCOUNT'), $this);?>

                                        <?php $_from = $this->_tpl_vars['basketitem']->aDiscounts; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['oDiscount']):
?>
                                        <br><?php echo $this->_tpl_vars['oDiscount']->sDiscount; ?>

                                        <?php endforeach; endif; unset($_from); ?>
                                    </em>
                                    <?php endif; ?>
                                    <?php if ($this->_tpl_vars['basketproduct']->oxarticles__oxorderinfo->value): ?>
                                    <?php echo $this->_tpl_vars['basketproduct']->oxarticles__oxorderinfo->value; ?>

                                    <?php endif; ?>
                                </p>
                            </td>
                            <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 10px 0;">
                                    <?php echo $this->_tpl_vars['basketitem']->getAmount(); ?>

                                </p>
                            </td>
                            <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 10px 0;">
                                    <?php echo $this->_tpl_vars['basketitem']->getVatPercent(); ?>
%
                                </p>
                            </td>
                            <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0; padding: 10px 0;">
                                    <b><?php echo $this->_tpl_vars['basketitem']->getFTotalPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
</b>
                                </p>
                            </td>
                        </tr>
                    
                <?php endforeach; endif; unset($_from); ?>
            </table>
            <br>

            
                <?php if ($this->_tpl_vars['basket']->oCard): ?>
                    <table border="0" cellspacing="0" cellpadding="2" width="100%">
                        <tr valign="top">
                            <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                    <b><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_OWNER_HTML_ATENTIONGREETINGCARD'), $this);?>
</b><br>
                                    <img src="<?php echo $this->_tpl_vars['basket']->oCard->getPictureUrl(); ?>
" alt="<?php echo $this->_tpl_vars['basket']->oCard->oxwrapping__oxname->value; ?>
" hspace="0" vspace="0" border="0" align="top">
                                </p>
                            </td>
                            <td style="padding: 5px; border-bottom: 4px solid #ddd;">
                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_YOURMESSAGE'), $this);?>

                                    <br><br>
                                    <?php echo $this->_tpl_vars['basket']->getCardMessage(); ?>

                                </p>
                            </td>
                        </tr>
                    </table>
                    <br>
                <?php endif; ?>
            

            <table border="0" cellspacing="0" cellpadding="2" width="100%">
                <tr>
                    <td width="50%" valign="top">
                        
                            <?php if ($this->_tpl_vars['oViewConf']->getShowVouchers()): ?>
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <?php if ($this->_tpl_vars['basket']->getVoucherDiscValue()): ?>
                                    <tr valign="top">
                                        <td style="padding: 5px 20px 5px 5px; border-bottom: 2px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;  color: #555;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_USEDCOUPONS'), $this);?>

                                            </p>
                                        </td>
                                        <td style="padding: 5px 20px 5px 5px; border-bottom: 2px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;  color: #555;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_REBATE'), $this);?>

                                            </p>
                                        </td>
                                    </tr>
                                    <?php endif; ?>
                                    <?php $_from = $this->_tpl_vars['order']->getVoucherList(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['voucher']):
?>
                                    <?php $this->assign('voucherseries', $this->_tpl_vars['voucher']->getSerie()); ?>
                                    <tr valign="top">
                                        <td style="padding: 5px 20px 5px 5px;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['voucher']->oxvouchers__oxvouchernr->value; ?>

                                            </p>
                                        </td>
                                        <td style="padding: 5px 20px 5px 5px;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['voucherseries']->oxvoucherseries__oxdiscount->value; ?>
 <?php if ($this->_tpl_vars['voucherseries']->oxvoucherseries__oxdiscounttype->value == 'absolute'): ?><?php echo $this->_tpl_vars['currency']->sign; ?>
<?php else: ?>%<?php endif; ?>
                                            </p>
                                        </td>
                                    </tr>
                                    <?php endforeach; endif; unset($_from); ?>
                                </table>
                            <?php endif; ?>
                        
                    </td>
                    <td width="50%" valign="top">
                        <table border="0" cellspacing="0" cellpadding="2" width="300">
                            <?php if (! $this->_tpl_vars['basket']->getDiscounts()): ?>
                                
                                    <!-- netto price -->
                                    <tr valign="top">
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALNET'), $this);?>

                                            </p>
                                        </td>
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right" width="60">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['basket']->getProductsNetPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                
                                
                                    <!-- VATs -->
                                    <?php $_from = $this->_tpl_vars['basket']->getProductVats(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['VATitem']):
?>
                                        <tr valign="top">
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PLUSTAX1'), $this);?>
 <?php echo $this->_tpl_vars['key']; ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PLUSTAX2'), $this);?>

                                                </p>
                                            </td>
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo $this->_tpl_vars['VATitem']; ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                                </p>
                                            </td>
                                        </tr>
                                    <?php endforeach; endif; unset($_from); ?>
                                

                                
                                    <!-- brutto price -->
                                    <tr valign="top">
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALGROSS'), $this);?>

                                            </p>
                                        </td>
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['basket']->getFProductsPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                

                            <?php endif; ?>

                            <!-- applied discounts -->
                            <?php if ($this->_tpl_vars['basket']->getDiscounts()): ?>

                                <?php if ($this->_tpl_vars['order']->isNettoMode()): ?>
                                    
                                    <!-- netto price -->
                                        <tr valign="top">
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALNET'), $this);?>

                                                </p>
                                            </td>
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right" width="60">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo $this->_tpl_vars['basket']->getProductsNetPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                                </p>
                                            </td>
                                        </tr>
                                    
                                <?php else: ?>
                                    
                                        <!-- brutto price -->
                                        <tr valign="top">
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALGROSS'), $this);?>

                                                </p>
                                            </td>
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo $this->_tpl_vars['basket']->getFProductsPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                                </p>
                                            </td>
                                        </tr>
                                    
                                <?php endif; ?>



                                
                                    <!-- discounts -->
                                    <?php $_from = $this->_tpl_vars['basket']->getDiscounts(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['oDiscount']):
?>
                                        <tr valign="top">
                                             <td style="padding: 5px; border-bottom: 1px solid #ddd;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php if ($this->_tpl_vars['oDiscount']->dDiscount < 0): ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_CHARGE'), $this);?>
<?php else: ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_DICOUNT'), $this);?>
<?php endif; ?> <em><?php echo $this->_tpl_vars['oDiscount']->sDiscount; ?>
</em> :
                                                </p>
                                            </td>
                                            <td style="padding: 5px; border-bottom: 1px solid #ddd;" align="right">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php if ($this->_tpl_vars['oDiscount']->dDiscount < 0): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['oDiscount']->fDiscount)) ? $this->_run_mod_handler('replace', true, $_tmp, "-", "") : smarty_modifier_replace($_tmp, "-", "")); ?>
<?php else: ?>-<?php echo $this->_tpl_vars['oDiscount']->fDiscount; ?>
<?php endif; ?> <?php echo $this->_tpl_vars['currency']->sign; ?>

                                                </p>
                                            </td>
                                        </tr>
                                    <?php endforeach; endif; unset($_from); ?>
                                

                                <?php if (! $this->_tpl_vars['order']->isNettoMode()): ?>
                                
                                    <!-- netto price -->
                                    <tr valign="top">
                                        <td style="padding: 5px; border-bottom: 1px solid #ddd;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALNET'), $this);?>

                                            </p>
                                        </td>
                                        <td style="padding: 5px; border-bottom: 1px solid #ddd;" align="right">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['basket']->getProductsNetPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                
                                <?php endif; ?>

                                
                                    <!-- VATs -->
                                    <?php $_from = $this->_tpl_vars['basket']->getProductVats(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['VATitem']):
?>
                                        <tr valign="top">
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PLUSTAX1'), $this);?>
 <?php echo $this->_tpl_vars['key']; ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PLUSTAX2'), $this);?>

                                                </p>
                                            </td>
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo $this->_tpl_vars['VATitem']; ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                                </p>
                                            </td>
                                        </tr>
                                    <?php endforeach; endif; unset($_from); ?>
                                

                                <?php if ($this->_tpl_vars['order']->isNettoMode()): ?>
                                
                                    <!-- brutto price -->
                                    <tr valign="top">
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TOTALGROSS'), $this);?>

                                            </p>
                                        </td>
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['basket']->getFProductsPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                
                                <?php endif; ?>

                            <?php endif; ?>

                            
                                <!-- voucher discounts -->
                                <?php if ($this->_tpl_vars['oViewConf']->getShowVouchers() && $this->_tpl_vars['basket']->getVoucherDiscValue()): ?>
                                    <tr valign="top">
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_COUPON'), $this);?>

                                            </p>
                                        </td>
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php if ($this->_tpl_vars['basket']->getFVoucherDiscountValue() > 0): ?>-<?php endif; ?><?php echo ((is_array($_tmp=$this->_tpl_vars['basket']->getFVoucherDiscountValue())) ? $this->_run_mod_handler('replace', true, $_tmp, "-", "") : smarty_modifier_replace($_tmp, "-", "")); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            

                            
                            <!-- delivery costs -->
                            <?php if ($this->_tpl_vars['basket']->getDelCostNet()): ?>
                                <tr valign="top">
                                    <td style="padding: 5px; border-bottom: 1px solid #ccc;">
                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                            <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_SHIPPINGNET'), $this);?>

                                        </p>
                                    </td>
                                    <td style="padding: 5px; border-bottom: 1px solid #ccc;" align="right">
                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                            <?php echo $this->_tpl_vars['basket']->getDelCostNet(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                        </p>
                                    </td>
                                </tr>
                                <?php if ($this->_tpl_vars['basket']->getDelCostVat()): ?>
                                    <tr valign="top">
                                        <?php if ($this->_tpl_vars['basket']->isProportionalCalculationOn()): ?>
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_PLUS_PROPORTIONAL_VAT'), $this);?>
:
                                                </p>
                                            </td>
                                        <?php else: ?>
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT1'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getDelCostVatPercent(); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT2'), $this);?>

                                                </p>
                                            </td>
                                        <?php endif; ?>
                                        <td style="padding: 5px; border-bottom: 2px solid #ddd;" align="right">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['basket']->getDelCostVat(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php elseif ($this->_tpl_vars['basket']->getFDeliveryCosts()): ?>
                                <tr valign="top">
                                    <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                            <?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_COST'), $this);?>
:
                                        </p>
                                    </td>
                                    <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                            <?php echo $this->_tpl_vars['basket']->getFDeliveryCosts(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                        </p>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        

                        
                            <!-- payment sum -->
                        <?php if ($this->_tpl_vars['basket']->getPayCostNet()): ?>
                            <tr valign="top">
                                <td style="padding: 5px; border-bottom: 2px solid #ccc;<?php if ($this->_tpl_vars['basket']->getDelCostVat()): ?>border-bottom: 1px solid #ddd;<?php endif; ?>">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                        <?php if ($this->_tpl_vars['basket']->getPaymentCosts() >= 0): ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PAYMENTCHARGEDISCOUNT1'), $this);?>
<?php else: ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PAYMENTCHARGEDISCOUNT2'), $this);?>
<?php endif; ?> <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PAYMENTCHARGEDISCOUNT3'), $this);?>

                                    </p>
                                </td>
                                <td style="padding: 5px; border-bottom: 2px solid #ccc;<?php if ($this->_tpl_vars['basket']->getDelCostVat()): ?>border-bottom: 1px solid #ddd;<?php endif; ?>" align="right">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                        <?php echo $this->_tpl_vars['basket']->getPayCostNet(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                    </p>
                                </td>
                            </tr>
                            <!-- payment sum VAT (if available) -->
                            <?php if ($this->_tpl_vars['basket']->getPayCostVat()): ?>
                                <tr valign="top">
                                    <?php if ($this->_tpl_vars['basket']->isProportionalCalculationOn()): ?>
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_PLUS_PROPORTIONAL_VAT'), $this);?>
:
                                            </p>
                                        </td>
                                    <?php else: ?>
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT1'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getPayCostVatPercent(); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT2'), $this);?>

                                            </p>
                                        </td>
                                    <?php endif; ?>
                                    <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                            <?php echo $this->_tpl_vars['basket']->getPayCostVat(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                        </p>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php elseif ($this->_tpl_vars['basket']->getFPaymentCosts()): ?>
                            <tr valign="top">
                                <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                        <?php echo smarty_function_oxmultilang(array('ident' => 'SURCHARGE'), $this);?>
:
                                    </p>
                                </td>
                                <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                        <?php echo $this->_tpl_vars['basket']->getFPaymentCosts(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                    </p>
                                </td>
                            </tr>
                        <?php endif; ?>
                        

                        
                            <?php if ($this->_tpl_vars['basket']->getTsProtectionCosts()): ?>
                                <!-- Trusted Shops -->
                                <tr valign="top">
                                    <td style="padding: 5px; border-bottom: 2px solid #ccc;<?php if ($this->_tpl_vars['basket']->getTsProtectionVat()): ?>border-bottom: 1px solid #ddd;<?php endif; ?>">
                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                            <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_TSPROTECTION'), $this);?>
:
                                        </p>
                                    </td>
                                    <td style="padding: 5px; border-bottom: 2px solid #ccc;<?php if ($this->_tpl_vars['basket']->getTsProtectionVat()): ?>border-bottom: 1px solid #ddd;<?php endif; ?>" align="right">
                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                            <?php echo $this->_tpl_vars['basket']->getTsProtectionNet(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                        </p>
                                    </td>
                                </tr>
                                <?php if ($this->_tpl_vars['basket']->getTsProtectionVat()): ?>
                                    <tr valign="top">
                                        <?php if ($this->_tpl_vars['basket']->isProportionalCalculationOn()): ?>
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_PLUS_PROPORTIONAL_VAT'), $this);?>
:
                                                </p>
                                            </td>
                                        <?php else: ?>
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT1'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getTsProtectionVatPercent(); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT2'), $this);?>

                                                </p>
                                            </td>
                                        <?php endif; ?>
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['basket']->getTsProtectionVat(); ?>
&nbsp;<?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php endif; ?>
                        

                        <?php if ($this->_tpl_vars['oViewConf']->getShowGiftWrapping()): ?>
                            
                                <!-- Gift wrapping -->
                                <?php if ($this->_tpl_vars['basket']->getWrappCostNet()): ?>
                                    <tr valign="top">
                                        <td style="padding: 5px; border-bottom: 1px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_WRAPPING_COSTS_NET'), $this);?>
:
                                            </p>
                                        </td>
                                        <td style="padding: 5px; border-bottom: 1px solid #ccc;" align="right">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['basket']->getWrappCostNet(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                    <?php if ($this->_tpl_vars['basket']->getWrappCostVat()): ?>
                                        <tr valign="top">
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_PLUS_VAT'), $this);?>
:
                                                </p>
                                            </td>
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo $this->_tpl_vars['basket']->getWrappCostVat(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                                </p>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                <?php elseif ($this->_tpl_vars['basket']->getFWrappingCosts()): ?>
                                    <tr valign="top">
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_WRAPPING_COSTS'), $this);?>
:
                                            </p>
                                        </td>
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['basket']->getFWrappingCosts(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            

                            
                                <!-- Greeting card -->
                                <?php if ($this->_tpl_vars['basket']->getGiftCardCostNet()): ?>
                                    <tr valign="top">
                                        <td style="padding: 5px; border-bottom: 1px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_GIFTCARD_COSTS_NET'), $this);?>
:
                                            </p>
                                        </td>
                                        <td style="padding: 5px; border-bottom: 1px solid #ccc;" align="right">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['basket']->getGiftCardCostNet(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                    <?php if ($this->_tpl_vars['basket']->getGiftCardCostVat()): ?>
                                    <tr>
                                        <?php if ($this->_tpl_vars['basket']->isProportionalCalculationOn()): ?>
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_PLUS_PROPORTIONAL_VAT'), $this);?>
:
                                                </p>
                                            </td>
                                        <?php else: ?>
                                            <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                    <?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT1'), $this);?>
 <?php echo $this->_tpl_vars['basket']->getGiftCardCostVatPercent(); ?>
<?php echo smarty_function_oxmultilang(array('ident' => 'SHIPPING_VAT2'), $this);?>

                                                </p>
                                            </td>
                                        <?php endif; ?>
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['basket']->getGiftCardCostVat(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                    <?php endif; ?>
                                <?php elseif ($this->_tpl_vars['basket']->getFGiftCardCosts()): ?>
                                    <tr valign="top">
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo smarty_function_oxmultilang(array('ident' => 'BASKET_TOTAL_GIFTCARD_COSTS'), $this);?>
:
                                            </p>
                                        </td>
                                        <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                                <?php echo $this->_tpl_vars['basket']->getFGiftCardCosts(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>

                                            </p>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            
                        <?php endif; ?>

                        
                            <!-- grand total price -->
                            <tr valign="top">
                                <td style="padding: 5px; border-bottom: 2px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                        <b><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_GRANDTOTAL'), $this);?>
</b>
                                    </p>
                                </td>
                                <td style="padding: 5px; border-bottom: 2px solid #ccc;" align="right">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                        <b><?php echo $this->_tpl_vars['basket']->getFPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
</b>
                                    </p>
                                </td>
                            </tr>
                        
                        </table>
                    </td>
                </tr>
            </table>

            
                <?php if ($this->_tpl_vars['order']->oxorder__oxremark->value): ?>
                    <h3 style="font-weight: bold; margin: 20px 0 7px; padding: 0; line-height: 35px; font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase; border-bottom: 4px solid #ddd;">
                        <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_OWNER_HTML_MESSAGE'), $this);?>

                    </h3>
                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                        <?php echo ((is_array($_tmp=$this->_tpl_vars['order']->oxorder__oxremark->value)) ? $this->_run_mod_handler('oxescape', true, $_tmp) : smarty_modifier_oxescape($_tmp)); ?>

                    </p>
                <?php endif; ?>
            

            
                <?php if ($this->_tpl_vars['payment']->oxuserpayments__oxpaymentsid->value != 'oxempty'): ?>
                    <h3 style="font-weight: bold; margin: 20px 0 7px; padding: 0; line-height: 35px; font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase; border-bottom: 4px solid #ddd;">
                        <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_OWNER_HTML_PAYMENTINFO'), $this);?>

                    </h3>
                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                        <b><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PAYMENTMETHOD'), $this);?>
 <?php echo $this->_tpl_vars['payment']->oxpayments__oxdesc->value; ?>
 <?php if ($this->_tpl_vars['basket']->getPaymentCosts()): ?>(<?php echo $this->_tpl_vars['basket']->getFPaymentCosts(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
)<?php endif; ?></b>
                        <br><br>
                        <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_OWNER_HTML_PAYMENTINFOOFF'), $this);?>

                    </p>
                <?php endif; ?>
            

            
                <h3 style="font-weight: bold; margin: 20px 0 7px; padding: 0; line-height: 35px; font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase; border-bottom: 4px solid #ddd;">
                    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_EMAILADDRESS'), $this);?>

                </h3>
                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                    <?php echo $this->_tpl_vars['user']->oxuser__oxusername->value; ?>

                </p>
            

            
                <!-- Address info -->
                <h3 style="font-weight: bold; margin: 20px 0 7px; padding: 0; line-height: 35px; font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase; border-bottom: 4px solid #ddd;">
                    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_ADDRESS'), $this);?>

                </h3>

                <table colspan="0" rowspan="0" border="0">
                    <tr valign="top">
                        <td style="padding-right: 30xp">
                            <h4 style="font-weight: bold; margin: 0; padding: 0 0 15px; line-height: 20px; font-size: 11px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase;">
                                <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_BILLINGADDRESS'), $this);?>

                            </h4>
                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                <?php echo $this->_tpl_vars['order']->oxorder__oxbillcompany->value; ?>
<br>
                                <?php echo ((is_array($_tmp=$this->_tpl_vars['order']->oxorder__oxbillsal->value)) ? $this->_run_mod_handler('oxmultilangsal', true, $_tmp) : smarty_modifier_oxmultilangsal($_tmp)); ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbillfname->value; ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbilllname->value; ?>
<br>
                                <?php if ($this->_tpl_vars['order']->oxorder__oxbilladdinfo->value): ?><?php echo $this->_tpl_vars['order']->oxorder__oxbilladdinfo->value; ?>
<br><?php endif; ?>
                                <?php echo $this->_tpl_vars['order']->oxorder__oxbillstreet->value; ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbillstreetnr->value; ?>
<br>
                                <?php echo $this->_tpl_vars['order']->oxorder__oxbillstateid->value; ?>

                                <?php echo $this->_tpl_vars['order']->oxorder__oxbillzip->value; ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbillcity->value; ?>
<br>
                                <?php echo $this->_tpl_vars['order']->oxorder__oxbillcountry->value; ?>
<br>
                                <?php if ($this->_tpl_vars['order']->oxorder__oxbillustid->value): ?><?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_VATIDNOMBER'), $this);?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbillustid->value; ?>
<br><?php endif; ?>
                                <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_PHONE'), $this);?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxbillfon->value; ?>
<br><br>
                            </p>
                        </td>
                        <?php if ($this->_tpl_vars['order']->oxorder__oxdellname->value): ?>
                            <td>
                                <h4 style="font-weight: bold; margin: 0; padding: 0 0 15px; line-height: 20px; font-size: 11px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase;">
                                    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_SHIPPINGADDRESS'), $this);?>

                                </h4>
                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0;">
                                    <?php echo $this->_tpl_vars['order']->oxorder__oxdelcompany->value; ?>
<br>
                                    <?php echo ((is_array($_tmp=$this->_tpl_vars['order']->oxorder__oxdelsal->value)) ? $this->_run_mod_handler('oxmultilangsal', true, $_tmp) : smarty_modifier_oxmultilangsal($_tmp)); ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxdelfname->value; ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxdellname->value; ?>
<br>
                                    <?php if ($this->_tpl_vars['order']->oxorder__oxdeladdinfo->value): ?><?php echo $this->_tpl_vars['order']->oxorder__oxdeladdinfo->value; ?>
<br><?php endif; ?>
                                    <?php echo $this->_tpl_vars['order']->oxorder__oxdelstreet->value; ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxdelstreetnr->value; ?>
<br>
                                    <?php echo $this->_tpl_vars['order']->oxorder__oxdelstateid->value; ?>

                                    <?php echo $this->_tpl_vars['order']->oxorder__oxdelzip->value; ?>
 <?php echo $this->_tpl_vars['order']->oxorder__oxdelcity->value; ?>
<br>
                                    <?php echo $this->_tpl_vars['order']->oxorder__oxdelcountry->value; ?>

                                </p>
                            </td>
                        <?php endif; ?>
                    </tr>
                </table>
            

            
                <?php if ($this->_tpl_vars['payment']->oxuserpayments__oxpaymentsid->value != 'oxempty'): ?>
                    <h3 style="font-weight: bold; margin: 20px 0 7px; padding: 0; line-height: 35px; font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-transform: uppercase; border-bottom: 4px solid #ddd;">
                    <?php echo smarty_function_oxmultilang(array('ident' => 'EMAIL_ORDER_CUST_HTML_SHIPPINGCARRIER'), $this);?>

                    </h3>
                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                        <?php echo $this->_tpl_vars['oDelSet']->oxdeliveryset__oxtitle->value; ?>

                    </p>
                <?php endif; ?>
            

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "email/html/footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
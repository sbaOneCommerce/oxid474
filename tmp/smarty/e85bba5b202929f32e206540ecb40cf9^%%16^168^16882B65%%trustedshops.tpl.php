<?php /* Smarty version 2.6.26, created on 2014-01-09 11:16:17
         compiled from page/checkout/inc/trustedshops.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxmultilang', 'page/checkout/inc/trustedshops.tpl', 4, false),)), $this); ?>

    <?php if ($this->_tpl_vars['oView']->getTSExcellenceId()): ?>
        <div id="tsBox">
            <h3 class="blockHead" id="tsProtectionHeader"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_TSPROTECTION'), $this);?>
</h3>
            <div class="etrustlogocol">
            <a href="https://www.trustedshops.com/shop/certificate.php?shop_id=<?php echo $this->_tpl_vars['oView']->getTSExcellenceId(); ?>
" target="_blank">
              <img src="<?php echo $this->_tpl_vars['oViewConf']->getImageUrl('trustedshops_m.gif'); ?>
" title="<?php echo smarty_function_oxmultilang(array('ident' => 'INC_TRUSTEDSHOPS_ITEM_IMGTITLE'), $this);?>
">
            </a>
            </div>
            <div>
            <input type="checkbox" name="bltsprotection" value="1" <?php if ($this->_tpl_vars['oView']->getCheckedTsProductId()): ?>checked<?php endif; ?>>
            <?php $this->assign('aTsProtections', $this->_tpl_vars['oView']->getTsProtections()); ?>
            <?php if (count ( $this->_tpl_vars['aTsProtections'] ) > 1): ?>
            <select name="stsprotection">
              <?php $_from = $this->_tpl_vars['aTsProtections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['oTsProduct']):
?>
                  <?php if ($this->_tpl_vars['oView']->isPaymentVatSplitted()): ?>
                     <option value="<?php echo $this->_tpl_vars['oTsProduct']->getTsId(); ?>
" <?php if ($this->_tpl_vars['oView']->getCheckedTsProductId() == $this->_tpl_vars['oTsProduct']->getTsId()): ?>SELECTED<?php endif; ?>><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_TSPROTECTIONFOR'), $this);?>
 <?php echo $this->_tpl_vars['oTsProduct']->getAmount(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
 (<?php echo $this->_tpl_vars['oTsProduct']->getFNettoPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
 <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_BASKETCONTENTS_PLUSTAX1'), $this);?>
 <?php echo $this->_tpl_vars['oTsProduct']->getFVatValue(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
 ) </option>
                  <?php else: ?>
                      <option value="<?php echo $this->_tpl_vars['oTsProduct']->getTsId(); ?>
" <?php if ($this->_tpl_vars['oView']->getCheckedTsProductId() == $this->_tpl_vars['oTsProduct']->getTsId()): ?>SELECTED<?php endif; ?>><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_TSPROTECTIONFOR'), $this);?>
 <?php echo $this->_tpl_vars['oTsProduct']->getAmount(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
 (<?php echo $this->_tpl_vars['oTsProduct']->getFPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
 <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_INCLUDEVAT'), $this);?>
) </option>
                  <?php endif; ?>
              <?php endforeach; endif; unset($_from); ?>
            </select>
            <?php else: ?>
                <?php $this->assign('oTsProduct', $this->_tpl_vars['aTsProtections'][0]); ?>
                <?php if ($this->_tpl_vars['oView']->isPaymentVatSplitted()): ?>
                <input type="hidden" name="stsprotection" value="<?php echo $this->_tpl_vars['oTsProduct']->getTsId(); ?>
"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_TSPROTECTIONFOR'), $this);?>
 <?php echo $this->_tpl_vars['oTsProduct']->getAmount(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
 (<?php echo $this->_tpl_vars['oTsProduct']->getFNettoPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
 <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_BASKETCONTENTS_PLUSTAX1'), $this);?>
 <?php echo $this->_tpl_vars['oTsProduct']->getFVatValue(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
)
                <?php else: ?>
                <input type="hidden" name="stsprotection" value="<?php echo $this->_tpl_vars['oTsProduct']->getTsId(); ?>
"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_TSPROTECTIONFOR'), $this);?>
 <?php echo $this->_tpl_vars['oTsProduct']->getAmount(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
 (<?php echo $this->_tpl_vars['oTsProduct']->getFPrice(); ?>
 <?php echo $this->_tpl_vars['currency']->sign; ?>
 <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_INCLUDEVAT'), $this);?>
)
                <?php endif; ?>
            <?php endif; ?>
              <br>
              <br>
            <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_TSPROTECTIONTEXT'), $this);?>
 <a href="http://www.trustedshops.com/shop/data_privacy.php?shop_id=<?php echo $this->_tpl_vars['oView']->getTSExcellenceId(); ?>
" target="_blank"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_TSPROTECTIONTEXT2'), $this);?>
</a>
            <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_TSPROTECTIONTEXT3'), $this);?>
 <a href="http://www.trustedshops.com/shop/protection_conditions.php?shop_id=<?php echo $this->_tpl_vars['oView']->getTSExcellenceId(); ?>
" target="_blank"><?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_TSPROTECTIONTEXT4'), $this);?>
</a> <?php echo smarty_function_oxmultilang(array('ident' => 'PAGE_CHECKOUT_PAYMENT_TSPROTECTIONTEXT5'), $this);?>

            </div>
            <div class="clear"></div>
        </div>
    <?php endif; ?>
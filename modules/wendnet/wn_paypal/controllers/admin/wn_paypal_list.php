<?php
/**
 * OXID module: wn_paypal
 *
 * @version 1.0.5
 * @author Sascha Mahnecke
 * @copyright 2012-2013 by Wendnet
 */
class wn_paypal_list extends oxAdminList{protected$_sThisTemplate='wn_paypal_list.tpl';protected$_sListClass='oxwnpaypal';public function init(){$this->_sDefSortField="oxinsert DESC";parent::init();}}
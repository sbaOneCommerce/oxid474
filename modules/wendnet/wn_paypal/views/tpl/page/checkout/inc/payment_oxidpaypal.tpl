<dl>
    <dt>
        <input id="payment_[{$sPaymentID}]" type="radio" name="paymentid" value="[{$sPaymentID}]" [{if $oView->getCheckedPaymentId() == $paymentmethod->oxpayments__oxid->value}]checked[{/if}]>
        <label for="payment_[{$sPaymentID}]"><img src="[{$oViewConf->getModuleUrl('wn_paypal')}]img/pp-logo.png" width="100" height="30" border="0" alt="Paypal-Logo" title="[{ $paymentmethod->oxpayments__oxdesc->value}]" style="vertical-align:middle">
        	[{ if $paymentmethod->dAddPaymentSum }]<b>([{ $paymentmethod->fAddPaymentSum }] [{ $currency->sign}])</b>[{/if}]</label>
    </dt>
    <dd class="[{if $oView->getCheckedPaymentId() == $paymentmethod->oxpayments__oxid->value}]activePayment[{/if}]">
        [{if $paymentmethod->oxpayments__oxlongdesc->value}]
            <div class="desc">
                [{ $paymentmethod->oxpayments__oxlongdesc->getRawValue()}]
            </div>
        [{/if}]
    </dd>
</dl>
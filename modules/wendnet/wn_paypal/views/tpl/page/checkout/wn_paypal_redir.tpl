<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<title>[{ oxmultilang ident="PAYPAL_REDIR_TITLE" }]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=[{ $oView->getCharSet() }]">
	<meta name="robots" content="noindex, nofollow">
	<link rel="shortcut icon" href="[{ $oViewConf->getImageUrl() }]favicon.ico">
	<style type="text/css">
body { font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; color:#777; margin:50px }
#page { text-align:center }
	</style>
</head>

<body[{ if $iMode }] onLoad="document.frmpp.submit()"[{/if}]>
<div id="page">
<form action="[{ $sAction }]" method="post" name="frmpp" id="frmpp">
<input type="hidden" name="cmd" value="_xclick">
[{ foreach from=$aPaypal key=sKey item=sVal }]
[{ if $sVal != '' }]
<input type="hidden" name="[{ $sKey }]" value="[{ $sVal }]">
[{/if}]
[{/foreach}]
[{ if $iMode }]
<script type="text/javascript">
document.write("<div>[{ oxmultilang ident="PAYPAL_REDIR_WAIT" }]</div>\n");
</script>

<noscript>[{/if}]
[{ oxmultilang ident="PAYPAL_REDIR_CLICK" }]
<br><br>
<input type="submit" value="[{ oxmultilang ident="PAYPAL_REDIR_BUTTON" }]">
[{ if $iMode }]</noscript>[{/if}]

</form>
</div>
[{ if !$iMode }]<hr><pre>[{ $sDebug }]</pre>[{/if}]
</body>
</html>
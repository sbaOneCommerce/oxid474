<?php
/**
 * Module: wn_paypal
 *
 * @version 1.0.4
 * @author Sascha Mahnecke
 * @copyright 2012-2013 by Wendnet
 */
$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
'charset'                                  => 'ISO-8859-15',
'mxpaypallogs'                             => 'PayPal-Log',
'tbclwn_paypal_main'                       => 'Stamm',
'WNPAYPAL_LIST_TITLE'                      => 'PayPal-Logliste',

'SHOP_MODULE_GROUP_main'                   => 'Allgemein',
'SHOP_MODULE_sWnCompany'                   => 'PayPal Firmenname',
'SHOP_MODULE_sWnLiveAccount'               => 'PayPal Live-Account (E-Mail Adresse; Shop im Produktivmodus)',
'SHOP_MODULE_sWnTestAccount'               => 'PayPal Test-Account (E-Mail Adresse; Shop nicht im Produktivmodus)',
'SHOP_MODULE_sWnHeaderLogo'                => 'PayPal Header-Logo (Standard: logo.png; nur nutzbar in SSL-Shops!)',
'SHOP_MODULE_sWnBorderColor'               => 'PayPal Rahmenfarbe (Standard: ddddff)',
'SHOP_MODULE_iWnRedirMode'                 => 'PayPal Aufruf-Modus (Standard: Sofortige Weiterleitung)',
'SHOP_MODULE_iWnRedirMode_2'               => 'Sofortige Weiterleitung',
'SHOP_MODULE_iWnRedirMode_1'               => 'Weiterleitungs-Seite',
'SHOP_MODULE_iWnRedirMode_0'               => 'Kontroll-Ausgabe',
'SHOP_MODULE_blWnAutoPayment'              => 'Beim (De)Aktivieren des Moduls automatisch die PayPal-Zahlungweise (de)aktivieren (Standard: ja)',

'GENERAL_AJAX_SORT_OXINSERT'               => 'Datum/Zeit',
'GENERAL_AJAX_SORT_OXAMOUNT'               => 'Betrag',
'GENERAL_AJAX_SORT_OXTRANSID'              => 'Transaktion',
'GENERAL_AJAX_SORT_OXORDERNR'              => 'Bestellnr.',
'GENERAL_AJAX_SORT_OXCUSTNR'               => 'Kundennr.',
'GENERAL_AJAX_SORT_OXSTATUS'               => 'PayPal-Status',
'GENERAL_AJAX_SORT_OXREASON'               => 'Begr�ndung',
'GENERAL_AJAX_SORT_OXIDSTATUS'             => 'Oxid-Status',
'GENERAL_AJAX_SORT_OXFINISHED'             => 'Return',

'GENERAL_WNPAYPAL_MODE'                    => 'PayPal-Modus',
'GENERAL_WNPAYPAL_MODE_LIVE'               => 'Live',
'GENERAL_WNPAYPAL_MODE_TEST'               => 'Sandbox',
'GENERAL_WNPAYPAL_OXMEMO'                  => 'Kunden-Memo',
'GENERAL_WNPAYPAL_OXIPN'                   => 'IPN-Parameter',
'GENERAL_WNPAYPAL_OXCART'                  => 'Warenkorb',
'GENERAL_WNPAYPAL_RETURN_PAYPAL'           => 'PayPal-Return',
'GENERAL_WNPAYPAL_RETURN_REDIR'            => 'Weiterleitung',
'GENERAL_WNPAYPAL_OXTIMESTAMP'             => 'Zul. Ge�ndert',
'GENERAL_WNPAYPAL_OXCUSTID'                => 'Kunden-ID',
'GENERAL_WNPAYPAL_OXORDERID'               => 'Order-ID',
'GENERAL_WNPAYPAL_OXSESSID'                => 'Session-ID',
'GENERAL_WNPAYPAL_OXDELADRID'              => 'Lieferadr.-ID',

'WN_PAYPAL_DB_LOAD_ERROR'                  => 'SQL-Setup-Datei konnte nicht geladen werden:',
'WN_PAYPAL_DB_SETUP_ERROR'                 => 'Fehler beim Ausf�hren des SQL-Befehls:',
'WN_PAYPAL_DB_SETUP_DONE'                  => 'Die SQL-Erweiterung wurde erfolgreich installiert!',
'WN_PAYPAL_ENABLE_PAYMENT'                 => "Die Zahlungsart 'PayPal' wurde aktiviert!",
'WN_PAYPAL_DISABLE_PAYMENT'                => "Die Zahlungsart 'PayPal' wurde deaktiviert!"
);

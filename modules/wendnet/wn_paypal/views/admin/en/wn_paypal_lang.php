<?php
/**
 * Module: wn_paypal
 *
 * @version 1.0.4
 * @author Sascha Mahnecke
 * @copyright 2012-2013 by Wendnet
 */
$sLangName  = "English";
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
'charset'                                  => 'ISO-8859-15',
'mxpaypallogs'                             => 'PayPal-Log',
'tbclwn_paypal_main'                       => 'Main',
'WNPAYPAL_LIST_TITLE'                      => 'PayPal log list',

'SHOP_MODULE_GROUP_main'                   => 'General',
'SHOP_MODULE_sWnCompany'                   => 'PayPal firm name',
'SHOP_MODULE_sWnLiveAccount'               => 'PayPal live account (E-mail address; shop in productive mode)',
'SHOP_MODULE_sWnTestAccount'               => 'PayPal test account (E-mail address; shop not in productive mode)',
'SHOP_MODULE_sWnHeaderLogo'                => 'PayPal header logo (default: logo.png; only usable with SSL-shops!)',
'SHOP_MODULE_sWnBorderColor'               => 'PayPal border color (default: ddddff)',
'SHOP_MODULE_iWnRedirMode'                 => 'PayPal calling mode (default: Instant redirection)',
'SHOP_MODULE_iWnRedirMode_2'               => 'Instant redirection',
'SHOP_MODULE_iWnRedirMode_1'               => 'Redirection page',
'SHOP_MODULE_iWnRedirMode_0'               => 'Output debug info',
'SHOP_MODULE_blWnAutoPayment'              => 'Automatically de(activate) PayPal payment method if module is (de)activated (default: yes)',

'GENERAL_AJAX_SORT_OXINSERT'               => 'Date/time',
'GENERAL_AJAX_SORT_OXAMOUNT'               => 'Amount',
'GENERAL_AJAX_SORT_OXTRANSID'              => 'Transaction',
'GENERAL_AJAX_SORT_OXORDERNR'              => 'Order no.',
'GENERAL_AJAX_SORT_OXCUSTNR'               => 'Custumer no.',
'GENERAL_AJAX_SORT_OXSTATUS'               => 'PayPal status',
'GENERAL_AJAX_SORT_OXREASON'               => 'Reason',
'GENERAL_AJAX_SORT_OXIDSTATUS'             => 'Oxid status',
'GENERAL_AJAX_SORT_OXFINISHED'             => 'Return',

'GENERAL_WNPAYPAL_MODE'                    => 'PayPal mode',
'GENERAL_WNPAYPAL_MODE_LIVE'               => 'Live',
'GENERAL_WNPAYPAL_MODE_TEST'               => 'Sandbox',
'GENERAL_WNPAYPAL_OXMEMO'                  => 'Customer memo',
'GENERAL_WNPAYPAL_OXIPN'                   => 'IPN-Parameters',
'GENERAL_WNPAYPAL_OXCART'                  => 'Basket',
'GENERAL_WNPAYPAL_RETURN_PAYPAL'           => 'PayPal return',
'GENERAL_WNPAYPAL_RETURN_REDIR'            => 'Redirection',
'GENERAL_WNPAYPAL_OXTIMESTAMP'             => 'Changed last',
'GENERAL_WNPAYPAL_OXCUSTID'                => 'Customer-ID',
'GENERAL_WNPAYPAL_OXORDERID'               => 'Order-ID',
'GENERAL_WNPAYPAL_OXSESSID'                => 'Session-ID',
'GENERAL_WNPAYPAL_OXDELADRID'              => 'Delivery addr-ID',

'WN_PAYPAL_DB_LOAD_ERROR'                  => 'SQL setup file could not be loaded:',
'WN_PAYPAL_DB_SETUP_ERROR'                 => 'Error while executing SQL statement:',
'WN_PAYPAL_DB_SETUP_DONE'                  => 'The SQL extension has been successfully installed!',
'WN_PAYPAL_ENABLE_PAYMENT'                 => "The payment method 'PayPal' has been enabled!",
'WN_PAYPAL_DISABLE_PAYMENT'                => "The payment method 'PayPal' has been disabled!"
);

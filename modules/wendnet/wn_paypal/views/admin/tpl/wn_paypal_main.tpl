[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]

<script type="text/javascript">
<!--
window.onload = function ()
{
    [{ if $updatelist == 1}]
        top.oxid.admin.updateList('[{ $oxid }]');
    [{ /if}]
}
//-->
</script>

[{ if $readonly }]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

<form name="transfer" id="transfer" action="[{ $oViewConf->getSelfLink() }]" method="post">
    [{ $oViewConf->getHiddenSid() }]
    <input type="hidden" name="oxid" value="[{ $oxid }]">
        <input type="hidden" name="cl" value="wn_paypal_main">
    <input type="hidden" name="language" value="[{ $actlang }]">
</form>

<form name="myedit" id="myedit" action="[{ $oViewConf->getSelfLink() }]" method="post">
[{ $oViewConf->getHiddenSid() }]
<input type="hidden" name="cl" value="wn_paypal_main">
<input type="hidden" name="fnc" value="">
<input type="hidden" name="oxid" value="[{ $oxid }]">
<input type="hidden" name="voxid" value="[{ $oxid }]">
<input type="hidden" name="oxparentid" value="[{ $oxparentid }]">
<input type="hidden" name="editval[oxwnpaypal__oxid]" value="[{ $oxid }]">
<input type="hidden" name="language" value="[{ $actlang }]">

<table cellspacing="0" cellpadding="0" border="0" width="98%">
<tr>
    <td valign="top" class="edittext">

        <table cellspacing="2" cellpadding="2" border="0">

        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_AJAX_SORT_OXINSERT" }]:
            </td>
            <td class="edittext"><b>[{$edit->oxwnpaypal__oxinsert->value}]</b></td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_AJAX_SORT_OXAMOUNT" }]:
            </td>
            <td class="edittext">[{ $sAmount }] [{$edit->oxwnpaypal__oxcur->value}]</td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_AJAX_SORT_OXTRANSID" }]:
            </td>
            <td class="edittext">[{$edit->oxwnpaypal__oxtransid->value}]</td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_AJAX_SORT_OXORDERNR" }]:
            </td>
            <td class="edittext">[{$edit->oxwnpaypal__oxordernr->value}]</td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_AJAX_SORT_OXCUSTNR" }]:
            </td>
            <td class="edittext">[{$edit->oxwnpaypal__oxcustnr->value}]</td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_AJAX_SORT_OXSTATUS" }]:
            </td>
            <td class="edittext"><b>[{$edit->oxwnpaypal__oxstatus->value}]</b></td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_AJAX_SORT_OXREASON" }]:
            </td>
            <td class="edittext">[{$edit->oxwnpaypal__oxreason->value}]</td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_AJAX_SORT_OXIDSTATUS" }]:
            </td>
            <td class="edittext"><b>[{$edit->oxwnpaypal__oxidstatus->value}]</b></td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_AJAX_SORT_OXFINISHED" }]:
            </td>
            [{ assign var="iFinished" value=$edit->oxwnpaypal__oxfinished->value }]
            <td class="edittext">[{ if $edit }][{ if $iFinished }][{ oxmultilang ident="GENERAL_YES" }] ([{ if $iFinished > 1 }][{ oxmultilang ident="GENERAL_WNPAYPAL_RETURN_REDIR" }][{ else }][{ oxmultilang ident="GENERAL_WNPAYPAL_RETURN_PAYPAL" }][{/if}])[{ else }][{ oxmultilang ident="GENERAL_NO" }][{/if}][{/if}]</td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_WNPAYPAL_MODE" }]:
            </td>
            <td class="edittext">[{ if $edit }][{ if $sIpn }][{ if $sTestMode }][{ oxmultilang ident="GENERAL_WNPAYPAL_MODE_TEST" }][{ else }][{ oxmultilang ident="GENERAL_WNPAYPAL_MODE_LIVE" }][{/if}][{else}]?[{/if}][{/if}]</td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_WNPAYPAL_OXTIMESTAMP" }]:
            </td>
            <td class="edittext">[{ $edit->oxwnpaypal__oxtimestamp->rawValue }]</td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_WNPAYPAL_OXORDERID" }]:
            </td>
            <td class="edittext">[{$edit->oxwnpaypal__oxorderid->value}]</td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_WNPAYPAL_OXCUSTID" }]:
            </td>
            <td class="edittext">[{$edit->oxwnpaypal__oxcustid->value}]</td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_WNPAYPAL_OXSESSID" }]:
            </td>
            <td class="edittext">[{$edit->oxwnpaypal__oxsessid->value}]</td>
        </tr>
        <tr>
            <td class="edittext">
            [{ oxmultilang ident="GENERAL_WNPAYPAL_OXDELADRID" }]:
            </td>
            <td class="edittext">[{$edit->oxwnpaypal__oxdeladrid->value}]</td>
        </tr>
        </table>
        <br>
        [{ if $sMemo }]
        <b>[{ oxmultilang ident="GENERAL_WNPAYPAL_OXMEMO" }]:</b>
        <br><br>
        <textarea class="editinput" cols="60" rows="2" readonly disabled>[{ $sMemo }]</textarea>
        <br><br>
        [{/if}]
        <b>[{ oxmultilang ident="GENERAL_WNPAYPAL_OXIPN" }]:</b>
        <br><br>
        <textarea class="editinput" cols="60" rows="5" readonly disabled>[{ $sIpn }]</textarea>

    </td>

    <!-- Anfang rechte Seite -->
    <td valign="top" class="edittext" align="left" width="50%">
		[{ if $edit && $oOrder }]
			<table width="200" border="0" cellspacing="0" cellpadding="0" nowrap>
            <tr><td class="edittext" valign="top">
				<b>[{ oxmultilang ident="GENERAL_BILLADDRESS" }]</b><br>
    	        <br>
        	    [{ if $oOrder->oxorder__oxbillcompany->value }][{ oxmultilang ident="GENERAL_COMPANY" }] [{$oOrder->oxorder__oxbillcompany->value }]<br>[{/if}]
	            [{ if $oOrder->oxorder__oxbilladdinfo->value }][{$oOrder->oxorder__oxbilladdinfo->value }]<br>[{/if}]
    	        [{$oOrder->oxorder__oxbillsal->value|oxmultilangsal}] [{$oOrder->oxorder__oxbillfname->value }] [{$oOrder->oxorder__oxbilllname->value }]<br>
        	    [{$oOrder->oxorder__oxbillstreet->value }] [{$oOrder->oxorder__oxbillstreetnr->value }]<br>
            	[{$oOrder->oxorder__oxbillstateid->value}]
	            [{$oOrder->oxorder__oxbillzip->value }] [{$oOrder->oxorder__oxbillcity->value }]<br>
    	        [{$oOrder->oxorder__oxbillcountry->value }]<br>
        	    [{if $oOrder->oxorder__oxbillcompany->value && $oOrder->oxorder__oxbillustid->value }]
                <br>
                [{ oxmultilang ident="ORDER_OVERVIEW_VATID" }]
                [{ $oOrder->oxorder__oxbillustid->value }]<br>
            [{/if}]
            <br>
            [{ oxmultilang ident="GENERAL_EMAIL" }]: <a href="mailto:[{$oOrder->oxorder__oxbillemail->value }]?subject=[{ $actshop}] - [{ oxmultilang ident="GENERAL_ORDERNUM" }] [{$oOrder->oxorder__oxordernr->value }]" class="edittext"><em>[{$oOrder->oxorder__oxbillemail->value }]</em></a><br>
            <br>
            </td>
            [{ if $oOrder->oxorder__oxdelstreet->value }]
            <td class="edittext" valign="top">
                <b>[{ oxmultilang ident="GENERAL_DELIVERYADDRESS" }]:</b><br>
                <br>
                [{ if $oOrder->oxorder__oxdelcompany->value }]Firma [{$oOrder->oxorder__oxdelcompany->value }]<br>[{/if}]
                [{ if $oOrder->oxorder__oxdeladdinfo->value }][{$oOrder->oxorder__oxdeladdinfo->value }]<br>[{/if}]
                [{$oOrder->oxorder__oxdelsal->value|oxmultilangsal }] [{$oOrder->oxorder__oxdelfname->value }] [{$oOrder->oxorder__oxdellname->value }]<br>
                [{$oOrder->oxorder__oxdelstreet->value }] [{$oOrder->oxorder__oxdelstreetnr->value }]<br>
                [{$oOrder->oxorder__oxdelstateid->value }]
                [{$oOrder->oxorder__oxdelzip->value }] [{$oOrder->oxorder__oxdelcity->value }]<br>
                [{$oOrder->oxorder__oxdelcountry->value }]<br>
                <br>
            </td>
            [{/if}]
            </tr></table>

			[{ assign var="sCurrency" value=$oOrder->oxorder__oxcurrency->value }]
            <b>[{ oxmultilang ident="GENERAL_WNPAYPAL_OXCART" }]:</b><br><br>
            <table cellspacing="0" cellpadding="0" border="0">
            [{foreach from=$orderArticles item=listitem}]
            <tr>
                <td valign="top" class="edittext">[{ $listitem->oxorderarticles__oxamount->value }] * </td>
                <td valign="top" class="edittext">&nbsp;[{ $listitem->oxorderarticles__oxartnum->value }]</td>
                <td valign="top" class="edittext">&nbsp;[{ $listitem->oxorderarticles__oxtitle->getRawValue()|oxtruncate:20:""|strip_tags }][{if $listitem->oxwrapping__oxname->value}]&nbsp;([{$listitem->oxwrapping__oxname->value}])&nbsp;[{/if}]</td>
                <td valign="top" class="edittext">&nbsp;[{ $listitem->oxorderarticles__oxselvariant->value }]</td>
                <td valign="top" class="edittext">&nbsp;&nbsp;[{ $listitem->getTotalBrutPriceFormated() }] [{ $sCurrency }]</td>
                [{ if $listitem->getPersParams() }]
                <td valign="top" class="edittext">
                    [{foreach key=sVar from=$listitem->getPersParams() item=aParam}]
                             &nbsp;&nbsp;,&nbsp;<em>[{$sVar}] : [{$aParam}]</em>
                    [{/foreach}]
                </td>
                [{/if}]
            </tr>
            [{/foreach}]
            </table>
            <br>
            <b>[{ oxmultilang ident="GENERAL_ATALL" }]:</b><br><br>
            <table border="0" cellspacing="2" cellpadding="2" id="order.info">
            <tr>
            <td class="edittext" height="15">[{ oxmultilang ident="GENERAL_IBRUTTO" }]</td>
            <td class="edittext" align="right"><b>[{ $oOrder->getFormattedTotalBrutSum() }]</b> [{ $sCurrency }]</td>
            </tr>
            <tr>
            <td class="edittext" height="15">[{ oxmultilang ident="GENERAL_DISCOUNT" }]&nbsp;&nbsp;</td>
            <td class="edittext" align="right"><b>- [{ $oOrder->getFormattedDiscount() }]</b> [{ $sCurrency }]</td>
            </tr>
            <tr>
            <td class="edittext" height="15">[{ oxmultilang ident="GENERAL_INETTO" }]</td>
            <td class="edittext" align="right"><b>[{ $oOrder->getFormattedTotalNetSum() }]</b> [{ $sCurrency }]</td>
            </tr>
            [{ foreach key=iVat from=$aProductVats item=dVatPrice }]
            <tr>
            <td class="edittext" height="15">[{ oxmultilang ident="GENERAL_IVAT" }] ([{ $iVat }]%)</td>
            <td class="edittext" align="right"><b>[{ $dVatPrice }]</b> [{ $sCurrency }]</td>
            </tr>
            [{/foreach}]
            [{ if $oOrder->oxorder__oxvoucherdiscount->value }]
            <tr>
            <td class="edittext" height="15">[{ oxmultilang ident="GENERAL_VOUCHERS" }]</td>
            <td class="edittext" align="right"><b>- [{ $oOrder->getFormattedTotalVouchers() }]</b> [{ $sCurrency }]</td>
            </tr>
            [{/if}]
            <tr>
            <td class="edittext" height="15">[{ oxmultilang ident="GENERAL_DELIVERYCOST" }]&nbsp;&nbsp;</td>
            <td class="edittext" align="right"><b>[{ $oOrder->getFormattedeliveryCost() }]</b> [{ $sCurrency }]</td>
            </tr>
            <tr>
            <td class="edittext" height="15">[{ oxmultilang ident="GENERAL_PAYCOST" }]&nbsp;&nbsp;</td>
            <td class="edittext" align="right"><b>[{ $oOrder->getFormattedPayCost() }]</b> [{ $sCurrency }]</td>
            </tr>
            [{ if $oOrder->oxorder__oxwrapcost->value }]
            <tr>
            <td class="edittext" height="15">[{ oxmultilang ident="GENERAL_CARD" }]&nbsp;[{if $oGiftCard}]([{$oGiftCard->oxwrapping__oxname->value}])[{/if}]&nbsp;</td>
            <td class="edittext" align="right"><b>[{ $oOrder->getFormattedWrapCost() }]</b> [{ $sCurrency }]</td>
            </tr>
            [{/if}]
            [{ if $oOrder->oxorder__oxtsprotectid->value }]
            <tr>
            <td class="edittext" height="15">[{ oxmultilang ident=ORDER_OVERVIEW_PROTECTION }]&nbsp;</td>
            <td class="edittext" align="right"><b>[{ $tsprotectcosts }]</b> [{ $sCurrency }]</td>
            </tr>
            [{/if}]
            <tr>
            <td class="edittext" height="25">[{ oxmultilang ident="GENERAL_SUMTOTAL" }]&nbsp;&nbsp;</td>
            <td class="edittext" align="right"><b>[{ $oOrder->getFormattedTotalOrderSum() }]</b> [{ $sCurrency }]</td>
            </tr>
            </table>

            <br>
            <table>
            <tr>
                <td class="edittext">[{ oxmultilang ident="ORDER_OVERVIEW_DELTYPE" }]: </td>
                <td class="edittext"><b>[{ $deliveryType->oxdeliveryset__oxtitle->value }]</b><br></td>
            </tr>
            </table>
		[{/if}]
    </td>
    <!-- Ende rechte Seite -->

    </tr>
</table>

</form>

[{include file="bottomnaviitem.tpl"}]
[{include file="bottomitem.tpl"}]

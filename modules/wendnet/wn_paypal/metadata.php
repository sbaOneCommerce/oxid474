<?php
/**
 * Module: wn_paypal
 *
 * @version 1.0.5
 * @author Sascha Mahnecke
 * @copyright 2012-2013 by Wendnet
 */

/**
 * Metadata version
 */
$sMetadataVersion = '1.1';
 
/**
 * Module information
 */
$aModule = array(
    'id'           => 'wn_paypal',
    'title'        => 'Wendnet-PayPal',
    'description'  => array(
        'de' => 'Modul zur Einbindung von Zahlungen per PayPal.',
        'en' => 'Module to integrate PayPal payments.'
    ),
    'thumbnail'    => 'logo.gif',
    'version'      => '1.0.5',
    'author'       => 'Sascha Mahnecke',
    'email'        => 'wendnet@gmx.de',
    'extend'       => array(
        'order'         => 'wendnet/wn_paypal/controllers/pp_order',
        'oxshopcontrol' => 'wendnet/wn_paypal/core/pp_oxshopcontrol'
    ),
    'files' => array(
        'oxwnpaypal'      => 'wendnet/wn_paypal/core/oxwnpaypal.php',
        'wn_paypal'       => 'wendnet/wn_paypal/controllers/wn_paypal.php',
        'wn_paypal_list'  => 'wendnet/wn_paypal/controllers/admin/wn_paypal_list.php',
        'wn_paypal_log'   => 'wendnet/wn_paypal/controllers/admin/wn_paypal_log.php',
        'wn_paypal_main'  => 'wendnet/wn_paypal/controllers/admin/wn_paypal_main.php'
    ),
    'templates' => array(
        'page/checkout/inc/payment_oxidpaypal.tpl' => 'wendnet/wn_paypal/views/tpl/page/checkout/inc/payment_oxidpaypal.tpl',
        'page/checkout/wn_paypal_redir.tpl'        => 'wendnet/wn_paypal/views/tpl/page/checkout/wn_paypal_redir.tpl',
        'wn_paypal_list.tpl'     => 'wendnet/wn_paypal/views/admin/tpl/wn_paypal_list.tpl',
        'wn_paypal_log.tpl'      => 'wendnet/wn_paypal/views/admin/tpl/wn_paypal_log.tpl',
        'wn_paypal_main.tpl'     => 'wendnet/wn_paypal/views/admin/tpl/wn_paypal_main.tpl'
    ),
    'settings' => array(
        array( 'group' => 'main', 'name' => 'sWnCompany', 'type' => 'str', 'value' => 'Test GmbH & Co. KG', 'position' => 1 ),
        array( 'group' => 'main', 'name' => 'sWnLiveAccount', 'type' => 'str', 'value' => '', 'position' => 2 ),
        array( 'group' => 'main', 'name' => 'sWnTestAccount', 'type' => 'str', 'value' => '', 'position' => 3 ),
        array( 'group' => 'main', 'name' => 'sWnHeaderLogo', 'type' => 'str', 'value' => 'logo.png', 'position' => 4 ),
        array( 'group' => 'main', 'name' => 'sWnBorderColor', 'type' => 'str', 'value' => 'ddddff', 'position' => 5 ),
        array( 'group' => 'main', 'name' => 'iWnRedirMode', 'type' => 'select', 'value' => '2', 'constrains' => '2|1|0', 'position' => 6 ),
        array( 'group' => 'main', 'name' => 'blWnAutoPayment', 'type' => 'bool', 'value' => 'true', 'position' => 7 )
    ),
    'blocks' => array(
        array('template' => 'page/checkout/payment.tpl', 'block' => 'select_payment', 'file' => 'views/blocks/select_payment.tpl'),
        array('template' => 'page/checkout/order.tpl', 'block' => 'checkout_order_details', 'file' => 'views/blocks/checkout_order_details.tpl')
    ),
    'events' => array(
        'onActivate'   => 'oxwnpaypal::onActivate',
        'onDeactivate' => 'oxwnpaypal::onDeactivate'
    )
);

?>
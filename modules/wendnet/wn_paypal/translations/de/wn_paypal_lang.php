<?php
/**
 * Module: wn_paypal
 *
 * @version 1.0.5
 * @author Sascha Mahnecke
 * @copyright 2012-2013 by Wendnet
 */
$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
'charset'                                  => 'ISO-8859-15',
'PAYPAL_REDIR_TITLE'                       => 'PayPal-Weiterleitung',
'PAYPAL_REDIR_WAIT'                        => 'Bitte einen Augenblick Geduld, Sie werden weitergeleitet zu PayPal...',
'PAYPAL_REDIR_CLICK'                       => 'Klicken Sie hier, um Paypal aufzurufen und Ihre Bezahlung abzuschlie�en',
'PAYPAL_REDIR_BUTTON'                      => 'Weiter �',
'PAYPAL_CANCEL_BUTTON'                     => 'Zur�ck zum Online-Shop (Bestellung abschlie�en)'
);

<?php
/**
 * Module: wn_paypal
 *
 * @version 1.0.5
 * @author Sascha Mahnecke
 * @copyright 2012-2013 by Wendnet
 */
$sLangName  = "English";
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
'charset'                                  => 'ISO-8859-15',
'PAYPAL_REDIR_TITLE'                       => 'PayPal redirection',
'PAYPAL_REDIR_WAIT'                        => 'Please be patient, you will be redirected to PayPal...',
'PAYPAL_REDIR_CLICK'                       => 'Please click here to open PayPal and finish your payment',
'PAYPAL_REDIR_BUTTON'                      => 'Continue �',
'PAYPAL_CANCEL_BUTTON'                     => 'Back to online shop (finishing order process)'
);

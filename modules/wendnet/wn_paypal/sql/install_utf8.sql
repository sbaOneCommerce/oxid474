CREATE TABLE IF NOT EXISTS `oxwnpaypal` (
  `OXID` char(32) NOT NULL DEFAULT '',
  `OXORDERNR` int(11) unsigned NOT NULL DEFAULT '0',
  `OXORDERID` char(32) NOT NULL DEFAULT '',
  `OXCUSTNR` int(11) NOT NULL DEFAULT '0',
  `OXCUSTID` char(32) NOT NULL DEFAULT '',
  `OXCART` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `OXAMOUNT` double NOT NULL DEFAULT '0.00',
  `OXCUR` varchar(8) NOT NULL DEFAULT '',
  `OXSESSID` varchar(64) NOT NULL DEFAULT '',
  `OXTRANSID` varchar(32) NOT NULL DEFAULT '',
  `OXSTATUS` varchar(64) NOT NULL DEFAULT '',
  `OXIDSTATUS` varchar(64) NOT NULL DEFAULT '',
  `OXREASON` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `OXIPN` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `OXFINISHED` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OXINSERT` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `OXTIMESTAMP` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY (`OXID`),
  KEY `oxsessid` (`OXSESSID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

INSERT IGNORE INTO `oxpayments` (`OXID`, `OXACTIVE`, `OXDESC`, `OXADDSUM`, `OXADDSUMTYPE`, `OXADDSUMRULES`, `OXFROMBONI`, `OXFROMAMOUNT`, `OXTOAMOUNT`, `OXVALDESC`, `OXCHECKED`, `OXDESC_1`, `OXVALDESC_1`, `OXDESC_2`, `OXVALDESC_2`, `OXDESC_3`, `OXVALDESC_3`, `OXLONGDESC`, `OXLONGDESC_1`, `OXLONGDESC_2`, `OXLONGDESC_3`, `OXSORT`) VALUES 
('oxidpaypal', 1, 'PayPal', 0, 'abs', 15, 0, 0, 1000000, '', 0, 'PayPal', '', '', '', '', '', 'Sie werden bei Abschluß der Bestellung automatisch zu PayPal weitergeleitet.', 'You will be automatically redirected to PayPal while finishing the order process.', '', '', 0);

UPDATE `oxpayments` SET `OXACTIVE`=1 WHERE `OXID`='oxidpaypal';

INSERT IGNORE INTO `oxobject2group` (`OXID`, `OXSHOPID`, `OXOBJECTID`, `OXGROUPSID`) VALUES
('wnpaypal.8aadfdc570f77e9c1df9c66', 'oxbaseshop', 'oxidpaypal', 'oxidadmin'),
('wnpaypal.2355267ab70c7abfd707ea1', 'oxbaseshop', 'oxidpaypal', 'oxiddealer'),
('wnpaypal.3a8b0864810f952a9b131b3', 'oxbaseshop', 'oxidpaypal', 'oxidforeigncustomer'),
('wnpaypal.c5018cd254666b01a114fcc', 'oxbaseshop', 'oxidpaypal', 'oxidgoodcust'),
('wnpaypal.0ba321fae39e03e588f14c4', 'oxbaseshop', 'oxidpaypal', 'oxidmiddlecust'),
('wnpaypal.0fafd44ae4d0aafa36cac4e', 'oxbaseshop', 'oxidpaypal', 'oxidnewcustomer'),
('wnpaypal.ad5cbc091d030abc027acbf', 'oxbaseshop', 'oxidpaypal', 'oxidnewsletter'),
('wnpaypal.bfc2c11cb6d1ba22e0eca37', 'oxbaseshop', 'oxidpaypal', 'oxidpowershopper'),
('wnpaypal.d227e42c8015478e6bcfbe0', 'oxbaseshop', 'oxidpaypal', 'oxidpricea'),
('wnpaypal.44c9ab77ee203e9ac9a020c', 'oxbaseshop', 'oxidpaypal', 'oxidpriceb'),
('wnpaypal.31d00a6bcf68e37515db157', 'oxbaseshop', 'oxidpaypal', 'oxidpricec'),
('wnpaypal.22f49efd572e626e06c4e53', 'oxbaseshop', 'oxidpaypal', 'oxidsmallcust');

INSERT IGNORE INTO `oxobject2payment` (`OXID`, `OXPAYMENTID`, `OXOBJECTID`, `OXTYPE`) VALUES
('wnpaypal.819717134ab64887fc84c9d', 'oxidpaypal', 'oxidstandard', 'oxdelset');

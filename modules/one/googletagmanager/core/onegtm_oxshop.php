<?php

class onegtm_oxshop extends onegtm_oxshop_parent
{
    public function getJsonData($type,$data,$total=null)
    {

        switch($type){
            case 'alist':
                echo $this->_getOneCategoryData($data);
                break;
            case 'purchase':
                echo $this->_getOneOrderData($data);
                break;
            case 'cart':
                echo $this->_getOneCartData($data,$total);
                break;
            case 'product':
                echo $this->_getOneProductData($data);
                break;
            case 'start':
                echo $this->_getOneStartData();
                break;
            case 'siteview':
                echo $this->_getOneSiteviewData();
                break;
            default:
                break;
        }

    }

    protected function _getOneCategoryData($data)
    {
        if($data)
        {
            $params = array();
            $params['pagetype'] = 'category';
            $params['prodid'] = array();
            $params['pname'] = array();
            $params['pvalue'] = array();


            foreach($data as $oArticle)
            {
                $params['prodid'][] = $oArticle->oxarticles__oxartnum->value;
                $params['pname'][] = $oArticle->oxarticles__oxtitle->value;
                $params['pvalue'][] = $oArticle->getFPrice();
            }

            return json_encode($params);
        }
    }

    protected function _getOneOrderData($data)
    {
        if($data)
        {
            $params = array();
            $params['pagetype'] = 'purchase';
            $params['value'] = $data->oxorder__oxtotalordersum->value;


            return json_encode($params);
        }
    }

    protected function _getOneCartData($data,$total)
    {

        if($data)
        {
            $params = array();
            $params['pagetype'] = 'cart';
            $params['prodid'] = array();
            $params['pname'] = array();
            $params['pvalue'] = array();


            foreach($data as $oArticle)
            {
                $params['prodid'][] = $oArticle->oxarticles__oxartnum->value;
                $params['pname'][] = $oArticle->oxarticles__oxtitle->value;
                $params['pvalue'][] = $oArticle->getFPrice();
            }

            $params['value'] = $total;
            return json_encode($params);
        }
    }

    protected function _getOneSiteviewData()
    {
            $params = array();
            $params['pagetype'] = 'siteview';

            return json_encode($params);
    }

    protected function _getOneProductData($oArticle)
    {

        if($oArticle)
        {
            $params = array();
            $params['pagetype'] = 'product';

            $params['prodid'][] = $oArticle->oxarticles__oxartnum->value;
            $params['pname'][] = $oArticle->oxarticles__oxtitle->value;
            $params['pvalue'][] = $oArticle->getFPrice();

            return json_encode($params);
        }
    }

    protected function _getOneStartData()
    {
            $params = array();
            $params['pagetype'] = 'start';

            return json_encode($params);
    }

}

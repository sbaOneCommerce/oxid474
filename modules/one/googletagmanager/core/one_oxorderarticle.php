<?php
/**
 * This Software is the property of One Commerce GmbH
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * key is a violation of the license agreement and will be
 * prosecuted by civil and criminal law.
 *
 * Inhaber: Christian Paavo Spieker
 * Alle Rechte vorbehalten
 *
 * @author Pavel Mihaylov <support@onecommerce.de>
 * @copyright (C) 2013, One Commerce GmbH
 * @see http://www.onecommerce.de
 */
 
class One_OxOrderArticle extends One_OxOrderArticle_parent
{
    
    public function getCategoryName($blWithRoot=false)
    {
        $oArticle = oxNew('oxarticle');
        $oArticle->load($this->getProductId());
        
        $sCatName='NoCat';
        $oCat = $oArticle->getCategory();
       
        if($oCat)
        {
            $oRootCat = $oCat->getParentCategory();
        
            $sCatName = ($blWithRoot?($oRootCat->oxcategories__oxtitle->value?$oRootCat->oxcategories__oxtitle->value."/":""):"").$oCat->oxcategories__oxtitle->value;
        }
        
        
        return $sCatName;
        
    }
}

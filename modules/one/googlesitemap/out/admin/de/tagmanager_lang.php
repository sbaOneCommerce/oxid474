<?php


$sLangName  = "Deutsch";

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(

'charset'                                            => 'UTF-8',
'SHOP_MODULE_GROUP_main'                             => 'Grundeinstellungen',
'SHOP_MODULE_sGtmId'                           => 'Google Tag Manager ID',
'SHOP_MODULE_blGaActive'                       => 'Google Analytics aktivieren',
'SHOP_MODULE_blGoogleCommerceActive'           => 'Google Commerce aktivieren',
'SHOP_MODULE_blGoogleConversionActive'         => 'Googel Conversiontracking aktivieren',
'SHOP_MODULE_aRemarketingSegments'             => 'Remarketing Segments',

);


[{assign var="config" value=$oViewConf->getConfig()}]
<script>
    [{literal}]
        dataLayer = [{
    [{/literal}]
                    'googleConversionActive':[{if $config->getConfigParam("blGoogleConversionActive")}]1[{else}]0[{/if}],
                    'googleCommerceActive':[{if $config->getConfigParam("blGoogleCommerceActive")}]1[{else}]0[{/if}],
                    'gaActive':[{if $config->getConfigParam("blGaActive")}]1[{else}]0[{/if}]
    [{literal}]
         }];
    [{/literal}]
</script>

<script>
    dataLayer.push({
        'pageCategory' : '[{$oViewConf->getActiveClassName()}]',
        'visitorExistingCustomer' : '[{if $oxcmp_user}]1[{else}]0[{/if}]'
    }); 
</script>

[{if $oViewConf->getActiveClassName() == 'register'}]
    <script>
        dataLayer.push({
            'registerState' : '[{$oView->getRegistrationStatus()}]'
        }); 
    </script>
[{/if}]

[{if $oViewConf->getActiveClassName() == 'search'}]
    [{assign var="currency" value=$oView->getActCurrency()}]
    <script>
        dataLayer.push({
            'searchQuery':'[{$oView->getSearchParamForHtml()}]',
            'siteSearchResults': [{$oView->getArticleCount()}]
        });
    
        dataLayer.push({ 
            'searchResults':  [
                [{foreach from=$oView->getArticleList() name=search item=_product}]
                    {
                    "sku": "[{ $_product->oxarticles__oxartnum->value }]",
                    "price": [{ $_product->getFPrice()|strip_tags }],
                    "currency": "[{ $currency->name}]"
                
                    }[{if !$smarty.foreach.search.last}],[{/if}]
                [{/foreach}]
            ]
        });
    </script>
[{/if}]

[{if $oViewConf->getActiveClassName() == 'alist'}]
    [{if !$actCategory}]
        [{assign var="actCategory" value=$oView->getActiveCategory()}]
    [{/if}]
    <script>
        dataLayer.push({
            'categoryTitle' : '[{$actCategory->oxcategories__oxtitle->value}]'
        }); 
    </script>
[{/if}]

[{if $oViewConf->getActiveClassName() == 'newsletter'}]
    <script>
        dataLayer.push({
           'newsletterState':[{$oView->getNewsletterStatus()}]
        });
    </script>
[{/if}]

[{if $oxcmp_user}]
    <script>
        dataLayer.push({
            'visitorType':'[{$oxcmp_user->getUserValue()}]',
            'visitorLifetimeValue':[{$oxcmp_user->getUserLifetimeValue()}]
        });
    </script>
[{/if}]

[{if $oViewConf->getActiveClassName() == 'thankyou'}]
    [{if !$order}]
        [{assign var="order" value=$oView->getOrder()}]
    [{/if}]
    
    [{if $order}]
    
        [{if $config->getConfigParam("blGoogleConversionActive")}]
        [{* Conversion values *}]
        <script>
            dataLayer.push({
                'conversionValue':[{$order->oxorder__oxtotalordersum->value}],
                'conversionId':'[{$order->oxorder__oxordernr->value}]',
                'conversionType':'[{$order->getConversionType()}]',
                'conversionAttributes':'[{$order->getConversionAttributes()}]',
                'conversionDate':'[{ $order->oxorder__oxorderdate->value }]'
            });
        </script>
        [{/if}]
    
        [{if $config->getConfigParam("blGoogleCommerceActive")}]
        <script>
            [{* Transaction values *}]
        
            dataLayer.push({
                'transactionTotal':[{$order->oxorder__oxtotalordersum->value}],
                'transactionId':'[{$order->oxorder__oxordernr->value}]',
                'transactionCurrency':'[{$order->oxorder__oxcurrency->value}]',
                'transactionDate':'[{ $order->oxorder__oxorderdate->value }]',
                'transactionShipping':[{ $order->oxorder__oxdeliverycost->value }],
                'transactionType':'[{$order->getConversionType()}]',
                'transactionTax':[{math equation=x - y x=$order->oxorder__oxtotalordersum->value  y=$order->oxorder__oxtotalnetsum->value format="%.2f"}],
                'transactionPaymentType':'[{ $order->oxorder__oxpaymenttype->value }]',
                'transactionShippingMethod':'[{$order->getDeliveryMethod()}]',
                'transactionPromoCode':'[{$order->getPromoCode()}]',
                'transactionAffiliation':'[{$order->getAffiliation()}]',
                'orderNetValue': [{ $order->oxorder__oxtotalnetsum->value }],
                'neworder':1
            });
        </script>
        <script>
            dataLayer.push({
            'transactionProducts': [
            [{foreach from=$order->getOrderArticles() item = oOrderArticle name=transProds}] 
                { 
                    name: '[{ $oOrderArticle->oxorderarticles__oxtitle->value }]',
                    sku: '[{ $oOrderArticle->oxorderarticles__oxartnum->value }]',
                    category: '[{$oOrderArticle->getCategoryName(true)}]',
                    price: [{ $oOrderArticle->oxorderarticles__oxprice->value }], 
                    quantity: [{ $oOrderArticle->oxorderarticles__oxamount->value }]
                }[{if !$smarty.foreach.transProds.last}],[{/if}]
            [{/foreach}]
            ]
            });
        
        </script>
        [{/if}]
    [{/if}]
[{/if}]
    
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=[{$config->getConfigParam('sGtmId')}]"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','[{$config->getConfigParam("sGtmId")}]');</script>
<!-- End Google Tag Manager -->
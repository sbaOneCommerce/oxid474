<?php

/**
 * This Software is the property of One Commerce GmbH
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * key is a violation of the license agreement and will be
 * prosecuted by civil and criminal law.
 *
 * Inhaber: Christian Paavo Spieker
 * Alle Rechte vorbehalten
 *
 * @author Pavel Mihaylov <support@onecommerce.de>
 * @copyright (C) 2013, One Commerce GmbH
 * @see http://www.onecommerce.de
 */

class one_oxuser extends one_oxuser_parent {

    protected $_aRemarketingSegments = array(0,49,99,149,299,499,999,2000,9999999);

    public function getUserValue()
    {
        if ( $this->getOrderCount() <= 1) {
            return oxLang::getInstance()->translateString( "new customer");
        } else {
            return oxLang::getInstance()->translateString( "customer");
        }
        
    }
    
    public function getUserLifetimeValue()
    {
        $sSum = 0;
        foreach($this->getOrders() as $oOrder)
        {
            $sSum += $oOrder->oxorder__oxtotalordersum->value;
        }
        
        $aSegments = $this->getConfig()->getConfigParam('aRemarketingSegments');
        if(!$aSegments)
            $aSegments = $this->_aRemarketingSegments;
        
        foreach($aSegments as $key => $val)
        {
            if($sSum <= $val)
                return $key;
        }
        
        return 0;
        
    }

  

}
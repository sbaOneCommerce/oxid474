<?php

/**
 * This Software is the property of One Commerce GmbH
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * key is a violation of the license agreement and will be
 * prosecuted by civil and criminal law.
 *
 * Inhaber: Christian Paavo Spieker
 * Alle Rechte vorbehalten
 *
 * @author Pavel Mihaylov <support@onecommerce.de>
 * @copyright (C) 2013, One Commerce GmbH
 * @see http://www.onecommerce.de
 */

 /**
  * Change this functions to get the values you need
  */
 
class One_Oxorder extends One_Oxorder_parent {

    /**
     *  return affilaition id
     */
    public function getAffiliation()
    {
        return;
    }
    
    /**
     *  return promo code
     */
    public function getPromoCode()
    {
        return;
    }
    

    /**
     *  return delivery method
     */
    public function getDeliveryMethod()
    {
        return;
    }
    
    /**
     *  return conversion type
     */
    public function getConversionType()
    {
        return 'order';
    }
    
    /**
     *  return conversion type
     */
    public function getConversionAttributes()
    {
        return ;
    }
    
    
    

}
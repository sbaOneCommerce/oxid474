<?php
/**
 * This Software is property of One Commerce GmbH
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * key is a violation of the license agreement and will be
 * prosecuted by civil and criminal law.
 *
 * Inhaber: Christian Paavo Spieker
 * Alle Rechte vorbehalten
 *
 * @author Pavel Mihaylov <support@onecommerce.de>
 * @copyright (C) 2012, One Commerce GmbH
 * @see http://www.onecommerce.de
 */

/**
 * Metadata version
 */
$sMetadataVersion = '1.1';

/**
 * Module information
 */
$aModule = array(
    'id'           => 'googletagmanager',
    'title'        => 'One Google Tag Manager',
    'description'  => 'Integration of the Google Tag Manager',
    'thumbnail'    => 'onecombox.jpg',
    'version'      => '1.0',
    'author'       => 'One Commerce GmbH',
    'email'         => 'shopsupport@onecommerce.de',
    'url'           => 'www.onecommerce.de',
    'extend'       => array(
        'oxuser' => 'one/googletagmanager/core/one_oxuser',
        'oxorderarticle' => 'one/googletagmanager/core/one_oxorderarticle',
        'oxorder' => 'one/googletagmanager/core/one_oxorder',
       
    ),
    'settings' => array(
            array('group' => 'main', 'name' => 'sGtmId',                                  'type' => 'str', 'value' => '', 'position' => '0'),
            array('group' => 'main', 'name' => 'blGaActive',                              'type' => 'bool', 'value' => 'true', 'position' => '0'),
            array('group' => 'main', 'name' => 'blGoogleCommerceActive',                  'type' => 'bool', 'value' => 'true', 'position' => '10'),
            array('group' => 'main', 'name' => 'blGoogleConversionActive',                'type' => 'bool', 'value' => 'false', 'position' => '20'),
            array('group' => 'main', 'name' =>  'aRemarketingSegments',                   'type' => 'aarr', 'position' => '30','value' => array( 1 => 0, 2 => 49, 3 => 99, 4 => 149, 5 => 299, 6 => 499, 7 => 999, 8 => 2000, 9 => 9999999 ))
        
    ),
    'blocks' => array(
        array('template' => 'layout/base.tpl', 'block'=>'after_body_start', 'file'=>'blocks/tagmanager.tpl'),
    )
    
);

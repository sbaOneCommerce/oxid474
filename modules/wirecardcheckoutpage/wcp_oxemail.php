<?php

class wcp_oxemail extends wcp_oxemail_parent
{
    /**
     * two wcp payments for order template
     *
     * @var string
     */
    protected $_sWCPDoublePaymentForOrder = "email/html/wcpDoublePaymentForOrder.tpl";

    /**
     * Send payment notification mail template for customer
     *
     * @var string
     */
    protected $_sWCPPaidTemplateCustomer = "email/html/wcpPaid_cust.tpl";

    /**
     * Send payment notification plain mail template for customer
     *
     * @var string
     */
    protected $_sWCPPaidTemplatePlainCustomer = "email/plain/wcpPaid_cust.tpl";

    /**
     * Send payment notification mail template for shop owner
     * 
     * @var string
     */
    protected $_sWCPPaidTemplateOwner = "email/html/wcpPaid_owner.tpl";
    
    /**
     * Send payment notification payment mail template for shop owner
     */
     protected $_sWCPPaidTemplatePlainOwner = "email/plain/wcpPaid_owner.tpl";
    /**
     * Sends two payments for order notification email to shop owner.
     *
     * @param string $sOrderNumber  
     * @param string $sSubject        user defined subject [optional]
     *
     * @return bool
     */
    public function sendWCPDoublePaymentMail($sOrderNumber, $sWCPOrderNumber1, $sWCPOrderNumber2, $sSubject = null )
    {
        $myConfig = $this->getConfig();
        $blSend = false;

        $oShop = $this->_getShop();

        //set mail params (from, fromName, smtp... )
        $this->_setMailParams( $oShop );
        /**
         * @var oxLang
         */
        $oLang = oxLang::getInstance();

        $oSmarty = $this->_getSmarty();
        
        $oSmarty->assign( "charset", $oLang->translateString("charset"));
        $oSmarty->assign( "shop", $oShop );
        $oSmarty->assign( "oViewConf", $oShop );
        $oSmarty->assign( "oView", $myConfig->getActiveView() );
        $oSmarty->assign( "orderNumber", $sOrderNumber );
        $oSmarty->assign( "wcpOrderNumber1", $sWCPOrderNumber1 );
        $oSmarty->assign( "wcpOrderNumber2", $sWCPOrderNumber2);

        $oOutputProcessor = oxNew( "oxoutput" );
        $aNewSmartyArray = $oOutputProcessor->processViewArray( $oSmarty->get_template_vars(), "oxemail" );

        foreach ( $aNewSmartyArray as $key => $val ) {
            $oSmarty->assign( $key, $val );
        }

        $this->setRecipient( $oShop->oxshops__oxowneremail->value, $oShop->oxshops__oxname->getRawValue() );
        $this->setFrom( $oShop->oxshops__oxowneremail->value, $oShop->oxshops__oxname->getRawValue() );
        $this->setBody( $oSmarty->fetch( $this->getConfig()->getTemplatePath( $this->_sWCPDoublePaymentForOrder, false ) ) );
        $this->setAltBody( "" );
        $this->setSubject( ( $sSubject !== null ) ? $sSubject : $oLang->translateString( 'WCP_EMAIL_DOUBLE_PAYMENT_HEADLINE' ) );

        $blSend = $this->send();

        return $blSend;
    }

/**
     * Sets mailer additional settings and sends "WCPPaid" mail to user.
     * Returns true on success.
     *
     * @param oxOrder $oOrder   order object
     * @param string  $sSubject user defined subject [optional]
     *
     * @return bool
     */
    public function sendWCPPaidMail( $oOrder, $sSubject = null )
    {
        $userMail = $this->_sendWCPPaidMailToUser($oOrder, $sSubject);

        if($this->getConfig()->getConfigParam('WCP_PAID_MAIL_TO_OWNER') == 'yes')
        {
            $ownerMail = $this->_sendWCPPaidMailToOwner($oOrder, $sSubject);
        }
        else
        {
            //no mail needed
            $ownerMail = true;
        }
        
        if($userMail && $ownerMail)
        {
            return true;
        }
        else 
        {
            return false;
        }

    }

    protected function _sendWCPPaidMailToOwner($oOrder, $sSubject = null)
    {
        $myConfig = $this->getConfig();
        $oShop = $this->_getShop();

        // cleanup
        $this->_clearMailer();

        $oLang = oxLang::getInstance();
        $iOrderLang = $oLang->getObjectTplLanguage();

        // if running shop language is different from admin lang. set in config
        // we have to load shop in config language
        if ( $oShop->getLanguage() != $iOrderLang ) {
            $oShop = $this->_getShop( $iOrderLang );
        }

        $this->setSmtp( $oShop );
        $oUser = $oOrder->getOrderUser();

        $oSmarty = $this->_getSmarty();

        $oUser = $oOrder->getOrderUser();

        //create messages
        $oSmarty->assign( "charset", $oLang->translateString("charset"));
        $oSmarty->assign( "shop", $oShop );
        $oSmarty->assign( "oViewConf", $oShop );
        $oSmarty->assign( "oView", $myConfig->getActiveView() );
        $oSmarty->assign( "order", $oOrder );
        $oSmarty->assign( "currency", $myConfig->getActShopCurrencyObject() );
        $oSmarty->assign( "user", $oUser);

        //deprecated var
        $oSmarty->assign( "isreview", true);
        $oSmarty->assign( "reviewuserhash", $oUser->getReviewUserHash($oOrder->oxorder__oxuserid->value) );

        $oOutputProcessor = oxNew( "oxoutput" );
        $aNewSmartyArray = $oOutputProcessor->processViewArray( $oSmarty->get_template_vars(), "oxemail" );

        foreach ( $aNewSmartyArray as $key => $val ) {
            $oSmarty->assign( $key, $val );
        }

        $this->setBody( $oSmarty->fetch( $this->_sWCPPaidTemplateOwner ) );
        $this->setAltBody( $oSmarty->fetch( $this->_sWCPPaidTemplatePlainOwner ) );

        // send confirmation to shop owner
        $sFullName = $oUser->oxuser__oxfname->getRawValue() . " " . $oUser->oxuser__oxlname->getRawValue();
        $this->setFrom( $oUser->oxuser__oxusername->value, $sFullName );

        //$this->setRecipient( $oOrder->oxorder__oxbillemail->value, $sFullName );
        $this->setRecipient( $oShop->oxshops__oxowneremail->value, $oShop->oxshops__oxname->getRawValue() );
        $this->setReplyTo( $oShop->oxshops__oxorderemail->value, $oShop->oxshops__oxname->getRawValue() );

        return $this->send();
    }
    
    protected function _sendWCPPaidMailToUser($oOrder, $sSubject = null)
    {
        $myConfig = $this->getConfig();
        // cleanup
        $this->_clearMailer();

        $iOrderLang = (int) ( isset( $oOrder->oxorder__oxlang->value ) ? $oOrder->oxorder__oxlang->value : 0 );

        // shop info
        $oShop = $this->_getShop( $iOrderLang );

        //set mail params (from, fromName, smtp)
        $this->_setMailParams( $oShop );

        //create messages
        $oLang = oxLang::getInstance();
        $oSmarty = $this->_getSmarty();

        $this->setSmtp( $oShop );
        $oUser = $oOrder->getOrderUser();

        //create messages
        $oSmarty->assign( "charset", $oLang->translateString("charset"));
        $oSmarty->assign( "shop", $oShop );
        $oSmarty->assign( "oViewConf", $oShop );
        $oSmarty->assign( "oView", $myConfig->getActiveView() );
        $oSmarty->assign( "order", $oOrder );
        $oSmarty->assign( "currency", $myConfig->getActShopCurrencyObject() );
        $oSmarty->assign( "user", $oUser);

        //deprecated var
        $oSmarty->assign( "isreview", true);
        $oUser = oxNew( 'oxuser' );
        $oSmarty->assign( "reviewuserhash", $oUser->getReviewUserHash($oOrder->oxorder__oxuserid->value) );

        $oOutputProcessor = oxNew( "oxoutput" );
        $aNewSmartyArray = $oOutputProcessor->processViewArray( $oSmarty->get_template_vars(), "oxemail" );

        foreach ( $aNewSmartyArray as $key => $val ) {
            $oSmarty->assign( $key, $val );
        }

        //V send email in order language
        $iOldTplLang = $oLang->getTplLanguage();
        $iOldBaseLang = $oLang->getTplLanguage();
        $oLang->setTplLanguage( $iOrderLang );
        $oLang->setBaseLanguage( $iOrderLang );

        $subject = $oLang->translateString('EMAIL_WCP_PAID_SUBJECT');

        $oSmarty->security_settings['INCLUDE_ANY'] = true;
        // force non admin to get correct paths (tpl, img)
        $this->setBody( $oSmarty->fetch( $this->_sWCPPaidTemplateCustomer ) );
        $this->setAltBody( $oSmarty->fetch( $this->_sWCPPaidTemplatePlainCustomer ) );
        $oLang->setTplLanguage( $iOldTplLang );
        $oLang->setBaseLanguage( $iOldBaseLang );

        //Sets subject to email
        $this->setSubject( ( $sSubject !== null ) ? $sSubject : $subject );

        $sFullName = $oOrder->oxorder__oxbillfname->getRawValue() . " " . $oOrder->oxorder__oxbilllname->getRawValue();

        $this->setFrom( $oShop->oxshops__oxorderemail->value, $oShop->oxshops__oxname->getRawValue() );
        $this->setRecipient( $oOrder->oxorder__oxbillemail->value, $sFullName );
        $this->setReplyTo( $oShop->oxshops__oxorderemail->value, $oShop->oxshops__oxname->getRawValue() );

        return $this->send();
    }

    public function getLanguageInstance()
    {
        $oLang = oxLang::getInstance();
        return $oLang;
    }
}

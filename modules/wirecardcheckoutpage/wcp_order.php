<?php
/**
    Shop System Plugins - Terms of use

    This terms of use regulates warranty and liability between Wirecard
    Central Eastern Europe (subsequently referred to as WDCEE) and it's
    contractual partners (subsequently referred to as customer or customers)
    which are related to the use of plugins provided by WDCEE.

    The Plugin is provided by WDCEE free of charge for it's customers and
    must be used for the purpose of WDCEE's payment platform integration
    only. It explicitly is not part of the general contract between WDCEE
    and it's customer. The plugin has successfully been tested under
    specific circumstances which are defined as the shopsystem's standard
    configuration (vendor's delivery state). The Customer is responsible for
    testing the plugin's functionality before putting it into production
    enviroment.
    The customer uses the plugin at own risk. WDCEE does not guarantee it's
    full functionality neither does WDCEE assume liability for any
    disadvantage related to the use of this plugin. By installing the plugin
    into the shopsystem the customer agrees to the terms of use. Please do
	not use this plugin if you do not agree to the terms of use!
*/

class wcp_order extends wcp_order_parent
{

    public function init()
    {
        $oOrder = oxNew('oxorder');
        $oOrder->load(oxSession::getVar('sess_challenge'));
        if($oOrder && wdceepayment::isValidWCPPayment($oOrder->oxorder__oxpaymenttype->value) && $oOrder->wcpCheckOrderExists())
        {
            //oxSession::deleteVar( 'sess_challenge' );
            oxSession::deleteVar('wcpPaymentState');
        }

        return parent::init();
    }

    /**
     * Hookpoint. check if it's an wcp Order.
     * @param $iSuccess - the order success state.
     * @return String - checkoutUrl or parent return.
     * @see order
     */
    protected function _getNextStep($iSuccess)
    {
        $oPayment = $this->getPayment();
        if($oPayment && wdceepayment::isValidWCPPayment($oPayment->oxpayments__oxid->value))
        {
            if(is_numeric($iSuccess) && $iSuccess == 1)
            {
                $oSession = $this->getSession();
                $oSession->setVar('wcpBasket', serialize($oSession->getBasket()));
                $oSession->delBasket();
                if($this->getConfig()->getConfigParam('WCP_USE_IFRAME') == 'yes')
                {
                    return 'wdceepayment?fnc=checkoutIFrame';
                }
                else
                {
                    return 'wdceepayment?fnc=checkoutForm';
                }
            }
        }
        return parent::_getNextStep($iSuccess);
    }
}
<?php
/**
    Shop System Plugins - Terms of use

    This terms of use regulates warranty and liability between Wirecard
    Central Eastern Europe (subsequently referred to as WDCEE) and it's
    contractual partners (subsequently referred to as customer or customers)
    which are related to the use of plugins provided by WDCEE.

    The Plugin is provided by WDCEE free of charge for it's customers and
    must be used for the purpose of WDCEE's payment platform integration
    only. It explicitly is not part of the general contract between WDCEE
    and it's customer. The plugin has successfully been tested under
    specific circumstances which are defined as the shopsystem's standard
    configuration (vendor's delivery state). The Customer is responsible for
    testing the plugin's functionality before putting it into production
    enviroment.
    The customer uses the plugin at own risk. WDCEE does not guarantee it's
    full functionality neither does WDCEE assume liability for any
    disadvantage related to the use of this plugin. By installing the plugin
    into the shopsystem the customer agrees to the terms of use. Please do
    not use this plugin if you do not agree to the terms of use!
*/

/**
 * Metadata version
 */
$sMetadataVersion = '1.0';

/**
 * Module information
 */

$aModule = array (
        'id' => 'wirecardcheckoutpage',
        'title' => 'Wirecard Checkout Page',
        'description' => 'Wirecard Checkout Page Payment Module for the Oxid eShop',
        'thumbnail' => 'picture.jpg',
        'version' => '2.4.0',
        'author' => 'Wirecard CEE',
        'email' => 'support@wirecard.at',
        'url' => 'http://www.wirecard.at',
        'extend' => array (
                'order' => 'wirecardcheckoutpage/wcp_order',
                'oxorder' => 'wirecardcheckoutpage/wcp_oxorder',
                'oxemail' => 'wirecardcheckoutpage/wcp_oxemail',
                'oxpaymentlist' => 'wirecardcheckoutpage/wcp_oxpaymentlist'
        ),
        'files' => array (
                'out/admin/de/wcp_lang.php' => 'wirecardcheckoutpage/out/admin/de/wcp_lang.php',
                'out/admin/en/wcp_lang.php' => 'wirecardcheckoutpage/out/admin/en/wcp_lang.php',
                'out/lang/de/wcp_lang.php' => 'wirecardcheckoutpage/out/lang/de/wcp_lang.php',
                'out/lang/en/wcp_lang.php' => 'wirecardcheckoutpage/out/lang/en/wcp_lang.php'
        ),
        'templates' => array (
                'wcp_settings.tpl' => 'wirecardcheckoutpage/out/admin/tpl/wcp_settings.tpl',
                'wcp_formparams.tpl' => 'wirecardcheckoutpage/out/admin/tpl/wcp_formparams.tpl',
                'wcp_list.tpl' => 'wirecardcheckoutpage/out/admin/tpl/wcp_list.tpl',
                'wcp_parameters.tpl' => 'wirecardcheckoutpage/out/admin/tpl/wcp_parameters.tpl',
                'wcp_shopsettings.tpl' => 'wirecardcheckoutpage/out/admin/tpl/wcp_shopsettings.tpl',
                'wcp_termsofuse.tpl' => 'wirecardcheckoutpage/out/admin/tpl/wcp_termsofuse.tpl',
                'page/checkout/wcp_checkout_iframe.tpl' => 'wirecardcheckoutpage/out/tpl/page/checkout/wcp_checkout_iframe.tpl',
                'page/checkout/wcp_checkout_page.tpl' => 'wirecardcheckoutpage/out/tpl/page/checkout/wcp_checkout_page.tpl',
                'page/checkout/wcp_return_iframe.tpl' => 'wirecardcheckoutpage/out/tpl/page/checkout/wcp_return_iframe.tpl'
        )
);
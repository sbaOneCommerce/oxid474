<?php
/**
    Shop System Plugins - Terms of use

    This terms of use regulates warranty and liability between Wirecard
    Central Eastern Europe (subsequently referred to as WDCEE) and it's
    contractual partners (subsequently referred to as customer or customers)
    which are related to the use of plugins provided by WDCEE.

    The Plugin is provided by WDCEE free of charge for it's customers and
    must be used for the purpose of WDCEE's payment platform integration
    only. It explicitly is not part of the general contract between WDCEE
    and it's customer. The plugin has successfully been tested under
    specific circumstances which are defined as the shopsystem's standard
    configuration (vendor's delivery state). The Customer is responsible for
    testing the plugin's functionality before putting it into production
    enviroment.
    The customer uses the plugin at own risk. WDCEE does not guarantee it's
    full functionality neither does WDCEE assume liability for any
    disadvantage related to the use of this plugin. By installing the plugin
    into the shopsystem the customer agrees to the terms of use. Please do
    not use this plugin if you do not agree to the terms of use!
*/

$sLangName  = "English";
$iLangNr    = 1;
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
//Wirecard CEE Text START
'charset'                                         => 'ISO-8859-15',
'wcp_payment_page_redirect'                      => 'Please wait while your are redirected to our Payment Service Provider.',
'wcp_return_redirect'                            => 'Please wait, your payment is being processed...',
'wcp_payment_pending'                            => 'Payment verification is pending.',
'wcp_payment_canceled'                           => 'You have canceled the payment process.',
'wcp_payment_failure'                            => 'An error occured duiring the payment process.',
'EMAIL_WCP_PAID_SUBJECT'                    => 'Order has been paid.',
'EMAIL_WCP_PAID_PAID'                       => 'Payment has been processed successfully.',
'EMAIL_WCP_PAID_ORDERNUMBER'                => 'Order No.:',
'EMAIL_WCP_PAID_QUANTITY'                   => 'Quantity',
'EMAIL_WCP_PAID_PRODUCT'                    => 'Product',
'EMAIL_WCP_PAID_PRODUCTRATING'              => 'Product Rating',
'EMAIL_WCP_PAID_ARTNUMBER'                  => 'Art.No.:',
'EMAIL_WCP_PAID_REVIEW'                     => 'review',
'EMAIL_WCP_PAID_YUORTEAM1'                  => 'Your',
'EMAIL_WCP_PAID_YUORTEAM2'                  => 'Team',
);
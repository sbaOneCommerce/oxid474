[{include file="headitem.tpl" }]
[{* TODO: get get rid of the tables. prio: -2*}]
<form name="transfer" id="transfer" action="[{ $oViewConf->getSelfLink() }]" method="post">
    [{include file="wcp_formparams.tpl" cl="wcp_parameters" lstrt=$lstrt actedit=$actedit oxid=$oxid fnc="" language=$actlang editlanguage=$actlang delshopid="" updatenav=""}]
</form>

<table cellspacing="0" cellpadding="0" border="0" width="99%" height="100%">
    <tr>
        <td width="100%" align="center" valign="top" bgcolor="#E7EAED" style="border : 1px #000000; border-style : none none solid none;">
            <table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
                <tr>
                    <td></td>
                    <td colspan="2"><h2>[{ oxmultilang ident="wcp_config_title" }]</h2></td>
                </tr>
                    <td width="15"></td>
                    <td valign="top" class="edittext">
                        <form name="myedit" id="myedit" action="[{ $shop->selflink }]" method="post">
                            [{include file="wcp_formparams.tpl" cl="wcp_parameters" lstrt=$lstrt actedit=$actedit oxid=$oxid fnc="save" language=$actlang editlanguage=$actlang delshopid="" updatenav=""}]
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><h3>[{ oxmultilang ident="wcp_parameter_settings_title" }]</h3></td>
                                </tr>
                                [{ if $wcp_saved}]
                                <tr>
                                    <td colspan="2" class="edittext" style="color: #4F8A10; background-color: #DFF2BF; border: 1px solid; font-weight: bold; padding: 4px;">[{ oxmultilang ident="wcp_save_message" }]</td>
                                <tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                [{/if}]
                                <!-- Customer-ID -->
                                <tr>
                                    <td valign="top" class="edittext" width="10%">CUSTOMER ID:</td>
                                    <td valign="top" class="edittext">
                                        <input type=text class="editinput" style="width:60%" name=confstrs[WCP_CUSTOMER_ID] value="[{$confstrs.WCP_CUSTOMER_ID}]">
                                        <p>
                                            [{ oxmultilang ident="wcp_customer_id_desc" }]
                                        </p>
                                    </td>
                                </tr>	
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <tr>
                                    <td valign="top" class="edittext">SHOPID:</td>
                                    <td valign="top" class="edittext">
                                        <input type=text class="editinput" style="width:60%" name=confstrs[WCP_SHOP_ID] value="[{$confstrs.WCP_SHOP_ID}]">
                                        <p>
                                            [{ oxmultilang ident="wcp_shop_id_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- Secret-Key -->
                                <tr>
                                    <td valign="top" class="edittext">SECRET KEY</td>
                                    <td valign="top" class="edittext">
                                        <input type=text class="editinput" style="width:60%" name=confstrs[WCP_SECRET] value="[{$confstrs.WCP_SECRET}]">
                                        <p>
                                            [{ oxmultilang ident="wcp_secret_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- Image Url -->
                                <tr>
                                    <td valign="top" class="edittext">IMAGEURL</td>
                                    <td valign="top" class="edittext">
                                        <input type=text class="editinput" style="width:60%" name=confstrs[WCP_IMAGE_URL] value="[{$confstrs.WCP_IMAGE_URL}]">
                                        <p>
                                            [{ oxmultilang ident="wcp_image_url_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- Service Url -->
                                <tr>
                                    <td valign="top" class="edittext">SERVICEURL</td>
                                    <td valign="top" class="edittext">
                                        <input type=text class="editinput" style="width:60%" name=confstrs[WCP_SERVICE_URL] value="[{$confstrs.WCP_SERVICE_URL}]">
                                        <p>
                                            [{ oxmultilang ident="wcp_service_url_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- Display Text -->
                                <tr>
                                    <td valign="top" class="edittext">DISPLAYTEXT</td>
                                    <td valign="top" class="edittext">
                                        <input type=text class="editinput" style="width:60%" name=confstrs[WCP_DISPLAY_TEXT] value="[{$confstrs.WCP_DISPLAY_TEXT}]">
                                        <p>
                                            [{ oxmultilang ident="wcp_display_text_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- max retries -->
                                <tr>
                                    <td valign="top" class="edittext">MAX RETRIES</td>
                                    <td valign="top" class="edittext">
                                        <input type=text class="editinput" style="width:60%" name=confstrs[WCP_MAX_RETRIES] value="[{$confstrs.WCP_MAX_RETRIES}]">
                                        <p>
                                            [{ oxmultilang ident="wcp_max_retries_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- Autodeposit -->
                                <tr>
                                    <td valign="top" class="edittext">AUTO-DEPOSIT</td>
                                    <td valign="top" class="edittext">
                                        <select name=confstrs[WCP_AUTO_DEPOSIT]>
                                          [{if $confstrs.WCP_AUTO_DEPOSIT == yes}] 
                                          <option value="yes" selected="selected">[{ oxmultilang ident="GENERAL_YES" }]</option>
                                          <option value="no">[{ oxmultilang ident="GENERAL_NO" }]</option>
                                          [{else}]
                                          <option value="yes">[{ oxmultilang ident="GENERAL_YES" }]</option>
                                          <option value="no" selected="selected">[{ oxmultilang ident="GENERAL_NO" }]</option>                                      
                                          [{/if}]
                                        </select>
                                        <p>
                                            [{ oxmultilang ident="wcp_auto_deposit_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                                <!-- background color -->
                                <tr>
                                    <td valign="top" class="edittext">BACKGROUND COLOR</td>
                                    <td valign="top" class="edittext">
                                        <input type=text class="editinput" style="width:60%" name=confstrs[WCP_BACKGROUND_COLOR] value="[{$confstrs.WCP_BACKGROUND_COLOR}]">
                                        <p>
                                            [{ oxmultilang ident="wcp_background_color_desc" }]
                                        </p>
                                    </td>
                                </tr>
                                <!-- save -->
                                <tr>
                                    <td class="edittext"></td>
                                    <td class="edittext"><br>
                                        <input type="submit" class="confinput" name="save" value="[{ oxmultilang ident="GENERAL_SAVE" }]" onClick="Javascript:document.myedit.fnc.value='save'">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top" class="edittext"><hr></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                    <td valign="top" class="edittext" align="left"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

[{include file="bottomnaviitem.tpl"}]

[{include file="bottomitem.tpl"}]
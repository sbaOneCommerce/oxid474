[{include file="headitem.tpl" }]
<!-- Terms of use -->
<div style="background-color: #E7EAED; padding: 18px;">
    <h2>[{ oxmultilang ident="wcp_config_title" }]</h2>
    <h3>[{ oxmultilang ident="wcp_terms_of_use_title" }]</h3>
    [{ oxmultilang ident="wcp_terms_of_use_desc" }]
</div>
<form name="transfer" id="transfer" action="[{ $oViewConf->getSelfLink() }]" method="post">
    [{ $oViewConf->getHiddenSid() }]
    <input type="hidden" name="oxid" value="[{ $oxid }]">
    <input type="hidden" name="cl" value="wcp_list">
    <input type="hidden" name="fnc" value="">
    <input type="hidden" name="actshop" value="[{$oViewConf->getActiveShopId()}]">
    <input type="hidden" name="updatenav" value="">
    <input type="hidden" name="editlanguage" value="[{ $editlanguage }]">
</form>

[{include file="bottomnaviitem.tpl"}]

[{include file="bottomitem.tpl"}]
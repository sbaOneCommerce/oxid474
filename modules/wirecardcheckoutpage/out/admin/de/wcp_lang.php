<?php
/**
    Shop System Plugins - Terms of use

    This terms of use regulates warranty and liability between Wirecard
    Central Eastern Europe (subsequently referred to as WDCEE) and it's
    contractual partners (subsequently referred to as customer or customers)
    which are related to the use of plugins provided by WDCEE.

    The Plugin is provided by WDCEE free of charge for it's customers and
    must be used for the purpose of WDCEE's payment platform integration
    only. It explicitly is not part of the general contract between WDCEE
    and it's customer. The plugin has successfully been tested under
    specific circumstances which are defined as the shopsystem's standard
    configuration (vendor's delivery state). The Customer is responsible for
    testing the plugin's functionality before putting it into production
    enviroment.
    The customer uses the plugin at own risk. WDCEE does not guarantee it's
    full functionality neither does WDCEE assume liability for any
    disadvantage related to the use of this plugin. By installing the plugin
    into the shopsystem the customer agrees to the terms of use. Please do
    not use this plugin if you do not agree to the terms of use!
*/

$sLangName  = "Deutsch";
$iLangNr    = 1;
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
'charset'                                         => 'ISO-8859-15',
'wcp_config_title'                               => 'Wirecard CEE Konfiguration',
'wcp_parameter_settings_title'                   => 'Parameter Einstellungen',
'wcp_settings'                                   => 'Wirecard Checkout Page Einstellungen',
'wcp_save_message'                               => 'Einstellungen wurden erfolgreich gespeichert',
'wcp_do_not_move'                                => 'Nicht verschieben',
'wcp_customer_id_desc'                           => 'Ihre Kundennummer (D2#####)',
'wcp_shop_id_desc'                               => 'Kennung der Shopseite, wenn mehrere Lizenzen unter einer Kundennummer verwendet werden.',
'wcp_secret_desc'                                => 'Preshared Key zur Signierung der &uuml;bertragenen Parameter',
'wcp_image_url_desc'                             => 'Link zu Ihrem Logo, das auf den Seiten von Wirecard Checkout Page eingebunden wird.',
'wcp_service_url_desc'                           => 'URL Ihrer Service-Seite',
'wcp_display_text_desc'                          => 'Text, der dem Konsumenten zu den Bestelldaten angezeigt wird.',
'wcp_customer_statement_desc'                    => 'Text der auf der Abrechnung des Kunden erscheinen soll',
'wcp_max_retries_desc'                           => 'Anzahl der maximalen Zahlungsversuche.',
'wcp_auto_deposit_desc'                          => 'Aktiviert automatisches Abbuchen der Zahlung.',
'wcp_background_color_desc'                      => 'Hintergrundfarbe die von Wirecard Checkout Page verwendet werden soll im Hex Format ohne voranstehende # (z.B. 0beef0)',
'wcp_plugin_settings_title'                      => 'Plugin Einstellungen',
'wcp_use_iframe_title'                           => 'IFrame verwenden',
'wcp_use_iframe_desc'                            => 'Anzeigen der Wirecard Checkout Page Paymentpage entweder in einem IFrame(Ja) oder als Seite(Nein)',
'wcp_use_iframe_note'                            => 'Hinweis: Ihr Design darf nicht dem XHTML Strict Standard entsprechen. In XHTML Strict sind IFrames nicht enthalten.',
'wcp_send_additional_data_title'                 => 'Konsumenten-Daten mitsenden',
'wcp_send_additional_data_desc'                  => 'Mitsenden von Konsumenten-Adressdaten zur Adressverifizierung.',
'wcp_log_confirmations_title'                    => 'Confirmations loggen',
'wcp_log_confirmations_desc'                     => 'Mitloggen von confirmUrl aufrufen f&uuml;r debug Zwecke (log/wirecardCheckoutPageConfirm.log)',
'wcp_paid_mail_to_owner_title'                   => 'Bezahlbest&auml;tigung an Shopbetreiber senden?',
'wcp_paid_mail_to_owner_desc'                    => 'Mit dieser Option k&ouml;nnen sie w&auml;hlen ob zus&auml;tzlich zur Bestellbest&auml;tigung eine Bezahlbest&auml;tigung an die Bestellmail gesendet wird.',
'wcp_checkout_folder_title'                      => 'Checkout Ordner',
'wcp_checkout_folder_desc'                       => 'Ordner in dem neue Bestellungen abgelegt werden bevor der Bezahlvorgang abgeschlossen wird.',
'wcp_success_folder_title'                       => 'Ordner f&uuml;r erfolgreiche Zahlungen',
'wcp_success_folder_desc'                        => 'Ordner in dem erfolgreich Bezahlte Bestellungen abgelegt werden.',
'wcp_pending_folder_title'                       => 'Ordner f&uuml;r ausstehende Zahlungen',
'wcp_pending_folder_desc'                        => 'Ordner in dem Bestellungen abgelegt werden deren Bezahlung ausst&auml;ndig ist.',
'wcp_cancel_folder_title'                        => 'Ordner f&uuml;r abgebrochene Zahlungen',
'wcp_cancel_folder_desc'                         => 'Ordner in dem Bestellungen abgelegt werden deren Bezahlung abgebrochen wurde.',
'wcp_failure_folder_title'                       => 'Ordner f&uuml;r fehlerhafte Zahlungen.',
'wcp_failure_folder_desc'                        => 'Ordner in dem Bestellungen abgelegt werden deren Bezahlung einen Fehler zur&uuml;ckgeliefert hat (z.B. Zeit&uuml;berschreitung der Zahlung)',
'wcp_terms_of_use_title'                         => 'Nutzungsbedingungen',
'wcp_terms_of_use_desc'                          =>
'<p>Diese Vereinbarung regelt die Gew&auml;hreistung und Haftung zwischen<br />
Wirecard Central Eastern Europe (nachfolgend kurz WDCEE) und seinen<br />
Vertragspartnern (nachfolgend kurz Kunde oder Kunden) betreffend<br />
Verwendung von WDCEE bereitgestellten Plugins.
</p>
<p>Das Plugin wird kostenlos von WDCEE f&uuml;r Kunden zur Verf&uuml;gung gestellt<br />
und darf ausschlie&szlig;lich f&uuml;r die Anbindung an die WDCEE Payment Plattform<br />
verwendet werden. Das Plugin z&auml;hlt ausdr&uuml;cklich nicht zum &uuml;blichen<br />
Leistungsumfang der WDCEE betreffend Zahlungabwicklung. Das Plugin wurde<br />
erfolgreich unter bestimmten Umst&auml;nden getestet die als<br />
Standardkonfiguration des Shopsystems (Hersteller Auslieferungszustand<br />
des Shopsystems) definiert sind. Der Kunde ist verantwortlich, die<br />
einwandfreie Funktion des Plugins sicherzustellen, bevor es produktiv<br />
verwendet wird.
</p>
<p>Der Kunde verwendet das Plugin auf eigenes Risiko. WDCEE &uuml;bernimmt keine<br />
Garantie f&uuml;r die einwandfreie Funktionalit&auml;t des Plugins. Des weitern<br />
wird von WDCEE keinerlei Haftung f&uuml;r Sch&auml;den &uuml;bernommen, die dem Kunden<br />
in Verbindung mit dem Einsatz des Plugins entstehen. Mit der<br />
Installation des Plugins stimmt der Kunde diesen Nutzungsbedingungen zu.<br />
Bitte verwenden Sie das Plugin nicht wenn Sie mit dieser Vereinbarung<br />
nicht einverstanden sind!
</p>'
);


/*

[{ oxmultilang ident="GENERAL_YOUWANTTODELETE" }]


*/

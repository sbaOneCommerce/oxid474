<?php

class wcp_oxpaymentlist extends wcp_oxpaymentlist_parent
{

    public function getPaymentList( $sShipSetId, $dPrice, $oUser = null )
    {
        $paymentList = parent::getPaymentList( $sShipSetId, $dPrice, $oUser );

        if(array_key_exists('oxWCPInvoice', $paymentList) || array_key_exists('oxWCPInstallment', $paymentList))
        {
            $oBasket = $this->getSession()->getBasket();
            $oOrder = oxNew('oxorder');
            if(array_key_exists('oxWCPInvoice', $paymentList))
            {
                if(!$this->_isWCPInvoiceAvailable($oUser, $oBasket, $oOrder))
                {
                    unset($paymentList['oxWCPInvoice']);
                }
            }
            if(array_key_exists('oxWCPInstallment', $paymentList))
            {
                if(!$this->_isWCPInstallmentAvailable($oUser, $oBasket, $oOrder))
                {
                    unset($paymentList['oxWCPInstallment']);
                }
            }
        }
        $this->_aArray = $paymentList;
        return $this->_aArray;
    }

    /**
     * check if paymentType invoice is available
     * @param oxUser $oUser
     * @return boolean
     */
    protected function _isWCPInvoiceAvailable($oUser, $oBasket, $oOrder)
    {
        if(!($oUser || $oBasket || $oOrder))
        {
            return false;
        }
        if(!$this->_wcpValidateCustomerAge($oUser))
        {
            return false;
        }
        if(!$this->_wcpValidateAddresses($oUser, $oOrder))
        {
            return false;
        }
        if(!$this->_wcpValidateCurrency($oBasket))
        {
            return false;
        }
        
        return true;
    }

    /**
     * check if paymentType installment is available
     * @param oxUser $oUser
     * @return boolean
     */
    protected function _isWCPInstallmentAvailable($oUser, $oBasket, $oOrder)
    {
        if(!($oUser || $oBasket || $oOrder))
        {
            return false;
        }
        if(!$this->_wcpValidateCustomerAge($oUser))
        {
            return false;
        }
        if(!$this->_wcpValidateAddresses($oUser, $oOrder))
        {
            return false;
        }
        if(!$this->_wcpValidateCurrency($oBasket))
        {
            return false;
        }
        return true;
    }

    /**
     * check if user is older than the given age
     * @param oxUser $oUser
     * @param integer $iMinAge
     * @return boolean
     */
    protected function _wcpValidateCustomerAge($oUser, $iMinAge = 18)
    {
        $dob = $oUser->oxuser__oxbirthdate->value;
        if($dob && $dob != '0000-00-00')
        {
            $iAgeChecker = $iMinAge--;
            $dobObject = new DateTime($dob);
            $currentYear = date('Y');
            $currentMonth = date('m');
            $currentDay = date('d');
            $ageCheckDate = ($currentYear - $iAgeChecker) . '-' . $currentMonth . '-' . $currentDay;
            $ageCheckObject = new DateTime($ageCheckDate);
            if($ageCheckObject < $dobObject)
            {
                //customer is younger than given age. PaymentType not available
                return false;
            }
        }
        else
        {
            return false;
        }
        return true;
    }

    protected function _wcpValidateAddresses($oUser, $oOrder)
    {
        //if delivery Address is not set it's the same as billing
        $oDelAddress = $oOrder->getDelAddressInfo();
        if($oDelAddress)
        {
            if($oDelAddress->oxaddress__oxcompany->value != $oUser->oxuser__oxcompany->value ||
               $oDelAddress->oxaddress__oxfname->value != $oUser->oxuser__oxfname->value ||
               $oDelAddress->oxaddress__oxlname->value != $oUser->oxuser__oxlname->value ||
               $oDelAddress->oxaddress__oxstreet->value != $oUser->oxuser__oxstreet->value ||
               $oDelAddress->oxaddress__oxstreetnr->value != $oUser->oxuser__oxstreetnr->value ||
               $oDelAddress->oxaddress__oxaddinfo->value != $oUser->oxuser__oxaddinfo->value ||
               $oDelAddress->oxaddress__oxcity->value != $oUser->oxuser__oxcity->value ||
               $oDelAddress->oxaddress__oxcountry->value != $oUser->oxuser__oxcountry->value ||
               $oDelAddress->oxaddress__oxstateid->value != $oUser->oxuser__oxstateid->value ||
               $oDelAddress->oxaddress__oxzip->value != $oUser->oxuser__oxzip->value ||
               $oDelAddress->oxaddress__oxfon->value != $oUser->oxuser__oxfon->value ||
               $oDelAddress->oxaddress__oxfax->value != $oUser->oxuser__oxfax->value ||
               $oDelAddress->oxaddress__oxsal->value != $oUser->oxuser__oxsal->value
            )
            {
                return false;
            }
        }
        return true;
    }

    /**
     * check if basket currency is an allowed currency
     * @param oxBasket $oBasket
     * @param Array $aAllowedCurrencies
     * @return boolean
     */
    protected function _wcpValidateCurrency($oBasket, $aAllowedCurrencies = Array('EUR'))
    {
        $currency = $oBasket->getBasketCurrency();
        if(!in_array($currency->name, $aAllowedCurrencies))
        {
            return false;
        }
        return true;
    }
}
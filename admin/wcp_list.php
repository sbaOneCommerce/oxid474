<?php
/**
    Shop System Plugins - Terms of use

    This terms of use regulates warranty and liability between Wirecard
    Central Eastern Europe (subsequently referred to as WDCEE) and it's
    contractual partners (subsequently referred to as customer or customers)
    which are related to the use of plugins provided by WDCEE.

    The Plugin is provided by WDCEE free of charge for it's customers and
    must be used for the purpose of WDCEE's payment platform integration
    only. It explicitly is not part of the general contract between WDCEE
    and it's customer. The plugin has successfully been tested under
    specific circumstances which are defined as the shopsystem's standard
    configuration (vendor's delivery state). The Customer is responsible for
    testing the plugin's functionality before putting it into production
    enviroment.
    The customer uses the plugin at own risk. WDCEE does not guarantee it's
    full functionality neither does WDCEE assume liability for any
    disadvantage related to the use of this plugin. By installing the plugin
    into the shopsystem the customer agrees to the terms of use. Please do
	not use this plugin if you do not agree to the terms of use!
*/

/**
 * Admin WCP List list manager.
 * Performs collection and managing (such as filtering or deleting) function.
 * Admin Menu: Main Menu -> Core Settings.
 * @package admin
 */
class WCP_List extends oxAdminList
{
    protected $_sThisTemplate = 'wcp_list.tpl';
    
    /**
     * Forces main frame update is set TRUE
     *
     * @var bool
     */
    protected $_blUpdateMain = false;

    /**
     * Default SQL sorting parameter (default null).
     *
     * @var string
     */
    protected $_sDefSortField = 'oxname';

    /**
     * Name of chosen object class (default null).
     *
     * @var string
     */
    protected $_sListClass = 'oxshop';

    /**
     * Navigation frame reload marker
     *
     * @var bool
     */
    protected $_blUpdateNav = null;

    /**
     * Executes parent method parent::render() and returns name of template
     * file "wcp_list.tpl".
     *
     * @return string
     */
    public function render()
    {
        $myConfig = $this->getConfig();

        parent::render();

        $soxId = $this->_aViewData["oxid"] = $this->getEditObjectId();
        if ( $soxId != '-1' && isset( $soxId ) ) {
            // load object
            $oShop = oxNew( 'oxshop' );
            if ( !$oShop->load( $soxId ) ) {
                $soxId = $myConfig->getBaseShopId();
                $oShop->load( $soxId );
            }
            $this->_aViewData['editshop'] = $oShop;
        }

        // default page number 1
        $this->_aViewData['default_edit'] = 'wcp_termsofuse';
        $this->_aViewData['updatemain']   = $this->_blUpdateMain;

        if ( $this->_aViewData['updatenav'] ) {
            //skipping requirements checking when reloading nav frame
            oxSession::setVar( "navReload", true );
        }

        //making sure we really change shops on low level
        if ( $soxId && $soxId != '-1' ) {
            $myConfig->setShopId( $soxId );
            oxSession::setVar( 'currentadminshop', $soxId );
        }

        return $this->_sThisTemplate;
    }

    public function getEditObjectId()
    {
        if(method_exists(get_parent_class($this), 'getEditObjectId'))
        {
            return parent::getEditObjectId();
        }
        else
        {
            return null;
        }
    }
    
    public function getListFilter()
    {
        if(method_exists(get_parent_class($this), 'getListFilter'))
        {
            return parent::getListFilter();
        }
        else
        {
            return null;
        }
    }
    
    public function getListSorting()
    {
        if(method_exists(get_parent_class($this), 'getListSorting'))
        {
            return parent::getListSorting();
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Sets SQL WHERE condition. Returns array of conditions.
     *
     * @return array
     */
    public function buildWhere()
    {
        // we override this to add our shop if we are not malladmin
        $this->_aWhere = parent::buildWhere();
        if ( !oxSession::getVar( 'malladmin' ) ) {
            // we only allow to see our shop
            $this->_aWhere[ getViewName( "oxshops" ) . ".oxid" ] = oxSession::getVar( "actshop" );
        }

        return $this->_aWhere;
    }

}

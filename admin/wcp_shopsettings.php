<?php
/**
    Shop System Plugins - Terms of use

    This terms of use regulates warranty and liability between Wirecard
    Central Eastern Europe (subsequently referred to as WDCEE) and it's
    contractual partners (subsequently referred to as customer or customers)
    which are related to the use of plugins provided by WDCEE.

    The Plugin is provided by WDCEE free of charge for it's customers and
    must be used for the purpose of WDCEE's payment platform integration
    only. It explicitly is not part of the general contract between WDCEE
    and it's customer. The plugin has successfully been tested under
    specific circumstances which are defined as the shopsystem's standard
    configuration (vendor's delivery state). The Customer is responsible for
    testing the plugin's functionality before putting it into production
    enviroment.
    The customer uses the plugin at own risk. WDCEE does not guarantee it's
    full functionality neither does WDCEE assume liability for any
    disadvantage related to the use of this plugin. By installing the plugin
    into the shopsystem the customer agrees to the terms of use. Please do
	not use this plugin if you do not agree to the terms of use!
*/

class WCP_ShopSettings extends Shop_Config
{
    /**
     * Current class template name.
     * @var string
     */
    protected $_sThisTemplate = 'wcp_shopsettings.tpl';

    /**
    * Creates shop object, passes shop data to Smarty engine and returns name of
    * template file "ox_wcp.tpl".
    * @return string
    */
    public function render()
    {
        parent::render();
        $this->_aViewData['afolder'] = $this->getConfig()->getConfigParam('aOrderfolder');
        $this->_aViewData['sHelpURL'] = '';
        return $this->_sThisTemplate;
    }
    
    public function save()
    {
        $this->_aViewData['wcp_saved'] = true;
        return parent::save();
    }
    
    public function getListSorting()
    {
        if(method_exists(get_parent_class($this), 'getListSorting'))
        {
            return parent::getListSorting();
        }
        else
        {
            return null;
        }
    }
    
}